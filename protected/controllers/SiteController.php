<?php

class SiteController extends Controller
{

	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionAbout()
	{
		$this->render('about');
	}
	public function actionRobokassa($id)
	{
		if(user()->isGuest){
			$this->redirect('/users/login');
		}else{
			$this->renderPartial('Robokassa',array('id'=>$id));
		}

	}
	public function actionSuccess()
	{
		if(Yii::app()->request->getParam('Shp_pmId'))
			$_GET['payment_id']=$_GET['Shp_pmId'];

		$payment_id = (int) Yii::app()->request->getParam('payment_id');
		$model      = StorePaymentMethod::model()->findByPk($payment_id);

		if(!$model)
			throw new CHttpException(404, 'Ошибка');

		$system = $model->getPaymentSystemClass();
		if($system instanceof BasePaymentSystem)
		{
			$response=$system->processPaymentRequest($model);

			if($response instanceof Order && $response->paid==1)
				$this->redirect('/');
			else
				throw new CHttpException(404, Yii::t('OrdersModule.core', 'Возникла ошибка при обработке запроса. <br> {err}', array('{err}'=>$response)));
		}
	}
	public function actionFail()
	{
		if(Yii::app()->request->getParam('Shp_pmId'))
			$_GET['payment_id']=$_GET['Shp_pmId'];

		$payment_id = (int) Yii::app()->request->getParam('payment_id');
		$model      = StorePaymentMethod::model()->findByPk($payment_id);

		if(!$model)
			throw new CHttpException(404, 'Ошибка');

		$system = $model->getPaymentSystemClass();
		if($system instanceof BasePaymentSystem)
		{
			$response=$system->processPaymentRequest($model);

			if($response instanceof Order)
				$this->redirect($this->createUrl('/cart/cart/view', array('key'=>$response->key)));
			else
				throw new CHttpException(404, Yii::t('OrdersModule.core', 'Возникла ошибка при обработке запроса. <br> {err}', array('{err}'=>$response)));
		}
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', array('error'=>$error));
		}
	}
}
