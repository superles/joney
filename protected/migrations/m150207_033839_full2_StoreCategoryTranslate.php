<?php

class m150207_033839_full2_StoreCategoryTranslate extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreCategoryTranslate", array(
    "id"=>"int(10) unsigned NOT NULL AUTO_INCREMENT",
    "object_id"=>"int(11)",
    "language_id"=>"int(11)",
    "name"=>"varchar(255)",
    "meta_title"=>"varchar(255)",
    "meta_keywords"=>"varchar(255)",
    "meta_description"=>"varchar(255)",
    "description"=>"text",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreCategoryTranslate does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}