<?php

class m150207_033839_full2_grid_view_filter extends CDbMigration
{
	public function up()
	{
	$this->createTable("grid_view_filter", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "user_id"=>"int(11)",
    "grid_id"=>"varchar(100)",
    "name"=>"varchar(100)",
    "data"=>"text",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_grid_view_filter does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}