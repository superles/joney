<?php

class m150207_033839_full2_Page extends CDbMigration
{
	public function up()
	{
	$this->createTable("Page", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "user_id"=>"int(11)",
    "category_id"=>"int(11)",
    "url"=>"varchar(255)",
    "tags"=>"text",
    "created"=>"datetime",
    "updated"=>"datetime",
    "publish_date"=>"datetime",
    "status"=>"varchar(255)",
    "layout"=>"varchar(2555)",
    "view"=>"varchar(255)",
    "image_id"=>"int(11)",
    "gallery_id"=>"int(11)",
    "views_count"=>"int(11)",
    "vote_id"=>"int(11)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_Page does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}