<?php

class m150207_033841_full2_gallery_photoFks extends CDbMigration
{
	public function up()
	{
	$this->addForeignKey("gallery_photo_ibfk_1", "gallery_photo", "gallery_id", "gallery", "id");

	}

	public function down()
	{
		echo "m150207_033841_full2_gallery_photoFks does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}