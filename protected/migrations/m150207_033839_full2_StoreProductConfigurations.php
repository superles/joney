<?php

class m150207_033839_full2_StoreProductConfigurations extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreProductConfigurations", array(
    "product_id"=>"int(11) NOT NULL",
    "configurable_id"=>"int(11) NOT NULL",
"PRIMARY KEY (product_id,configurable_id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreProductConfigurations does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}