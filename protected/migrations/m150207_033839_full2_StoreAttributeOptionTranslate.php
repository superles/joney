<?php

class m150207_033839_full2_StoreAttributeOptionTranslate extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreAttributeOptionTranslate", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "language_id"=>"int(11)",
    "object_id"=>"int(11)",
    "value"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreAttributeOptionTranslate does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}