<?php

class m150207_033841_full2_poll_choiceFks extends CDbMigration
{
	public function up()
	{
	$this->addForeignKey("choice_poll", "poll_choice", "poll_id", "poll", "id");

	}

	public function down()
	{
		echo "m150207_033841_full2_poll_choiceFks does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}