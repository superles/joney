<?php

class m150207_033839_full2_StoreAttribute extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreAttribute", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "name"=>"varchar(255)",
    "type"=>"tinyint(4)",
    "display_on_front"=>"tinyint(1) DEFAULT '1'",
    "use_in_filter"=>"tinyint(1)",
    "use_in_variants"=>"tinyint(1)",
    "use_in_compare"=>"tinyint(1)",
    "select_many"=>"tinyint(1)",
    "position"=>"int(11)",
    "required"=>"tinyint(1)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreAttribute does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}