<?php

class m150207_033839_full2_PageCategory extends CDbMigration
{
	public function up()
	{
	$this->createTable("PageCategory", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "parent_id"=>"int(11)",
    "url"=>"varchar(255)",
    "full_url"=>"text",
    "layout"=>"varchar(255)",
    "view"=>"varchar(255)",
    "created"=>"datetime",
    "updated"=>"datetime",
    "page_size"=>"smallint(6)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_PageCategory does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}