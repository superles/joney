<?php

class m150207_033839_full2_gallery_photo extends CDbMigration
{
	public function up()
	{
	$this->createTable("gallery_photo", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "gallery_id"=>"int(11) NOT NULL",
    "rank"=>"int(11) NOT NULL",
    "name"=>"varchar(512) NOT NULL",
    "description"=>"text",
    "file_name"=>"varchar(128) NOT NULL",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_gallery_photo does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}