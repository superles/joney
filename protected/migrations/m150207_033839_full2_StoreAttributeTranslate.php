<?php

class m150207_033839_full2_StoreAttributeTranslate extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreAttributeTranslate", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "object_id"=>"int(11)",
    "language_id"=>"int(11)",
    "title"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreAttributeTranslate does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}