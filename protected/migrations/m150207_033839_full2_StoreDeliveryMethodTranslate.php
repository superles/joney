<?php

class m150207_033839_full2_StoreDeliveryMethodTranslate extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreDeliveryMethodTranslate", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "object_id"=>"int(11)",
    "language_id"=>"int(11)",
    "name"=>"varchar(255)",
    "description"=>"text",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreDeliveryMethodTranslate does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}