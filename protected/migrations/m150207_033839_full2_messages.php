<?php

class m150207_033839_full2_messages extends CDbMigration
{
	public function up()
	{
	$this->createTable("messages", array(
    "id"=>"int(11) NOT NULL",
    "sender_id"=>"int(11) NOT NULL",
    "receiver_id"=>"int(11) NOT NULL",
    "subject"=>"varchar(256) NOT NULL",
    "body"=>"text",
    "is_read"=>"enum('0','1') NOT NULL DEFAULT '0'",
    "deleted_by"=>"enum('sender','receiver')",
    "created_at"=>"datetime NOT NULL",
), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_messages does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}