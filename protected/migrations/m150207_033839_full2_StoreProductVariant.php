<?php

class m150207_033839_full2_StoreProductVariant extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreProductVariant", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "attribute_id"=>"int(11)",
    "option_id"=>"int(11)",
    "product_id"=>"int(11)",
    "price"=>"float(10,2) DEFAULT '0.00'",
    "price_type"=>"tinyint(1)",
    "sku"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreProductVariant does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}