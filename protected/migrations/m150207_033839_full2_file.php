<?php

class m150207_033839_full2_file extends CDbMigration
{
	public function up()
	{
	$this->createTable("file", array(
    "id"=>"int(10) unsigned NOT NULL AUTO_INCREMENT",
    "name"=>"varchar(255) NOT NULL",
    "extension"=>"varchar(255) NOT NULL",
    "path"=>"varchar(255)",
    "filename"=>"varchar(255) NOT NULL",
    "mimeType"=>"varchar(255) NOT NULL",
    "byteSize"=>"int(10) unsigned NOT NULL",
    "createdAt"=>"datetime NOT NULL",
    "hash"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_file does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}