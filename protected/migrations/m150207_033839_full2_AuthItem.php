<?php

class m150207_033839_full2_AuthItem extends CDbMigration
{
	public function up()
	{
	$this->createTable("AuthItem", array(
    "name"=>"varchar(64) NOT NULL",
    "type"=>"int(11) NOT NULL",
    "description"=>"text",
    "bizrule"=>"text",
    "data"=>"text",
"PRIMARY KEY (name)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_AuthItem does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}