<?php

class m150207_033839_full2_poll_vote extends CDbMigration
{
	public function up()
	{
	$this->createTable("poll_vote", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "choice_id"=>"int(11) unsigned NOT NULL",
    "poll_id"=>"int(11) unsigned NOT NULL",
    "user_id"=>"int(11) NOT NULL",
    "ip_address"=>"varchar(16) NOT NULL",
    "timestamp"=>"int(11) unsigned NOT NULL DEFAULT '0'",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_poll_vote does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}