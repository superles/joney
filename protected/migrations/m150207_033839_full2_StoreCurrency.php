<?php

class m150207_033839_full2_StoreCurrency extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreCurrency", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "name"=>"varchar(255)",
    "iso"=>"varchar(10)",
    "symbol"=>"varchar(10)",
    "rate"=>"float(10,3)",
    "main"=>"tinyint(1)",
    "default"=>"tinyint(1)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreCurrency does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}