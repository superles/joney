<?php

class m150207_033839_full2_StoreWishlist extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreWishlist", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "key"=>"varchar(10)",
    "user_id"=>"int(11)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreWishlist does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}