<?php

class m150207_033839_full2_poll extends CDbMigration
{
	public function up()
	{
	$this->createTable("poll", array(
    "id"=>"int(11) unsigned NOT NULL AUTO_INCREMENT",
    "title"=>"varchar(255) NOT NULL",
    "description"=>"longtext",
    "status"=>"tinyint(1) NOT NULL DEFAULT '1'",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_poll does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}