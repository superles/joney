<?php

class m150207_033839_full2_Rights extends CDbMigration
{
	public function up()
	{
	$this->createTable("Rights", array(
    "itemname"=>"varchar(64) NOT NULL",
    "type"=>"int(11)",
    "weight"=>"int(11)",
"PRIMARY KEY (itemname)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_Rights does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}