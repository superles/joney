<?php

class m150207_033839_full2_OrderProduct extends CDbMigration
{
	public function up()
	{
	$this->createTable("OrderProduct", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "order_id"=>"int(11) NOT NULL",
    "product_id"=>"int(11) NOT NULL",
    "configurable_id"=>"int(11)",
    "name"=>"text",
    "configurable_name"=>"text",
    "configurable_data"=>"text",
    "variants"=>"text",
    "quantity"=>"smallint(6)",
    "sku"=>"varchar(255)",
    "price"=>"float(10,2)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_OrderProduct does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}