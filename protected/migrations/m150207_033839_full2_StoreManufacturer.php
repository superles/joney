<?php

class m150207_033839_full2_StoreManufacturer extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreManufacturer", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "url"=>"varchar(255)",
    "layout"=>"varchar(255)",
    "view"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreManufacturer does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}