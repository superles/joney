<?php

class m150207_033839_full2_StorePaymentMethod extends CDbMigration
{
	public function up()
	{
	$this->createTable("StorePaymentMethod", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "currency_id"=>"int(11)",
    "active"=>"tinyint(1)",
    "payment_system"=>"varchar(100)",
    "position"=>"smallint(6)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StorePaymentMethod does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}