<?php

class m150207_033839_full2_PageTag extends CDbMigration
{
	public function up()
	{
	$this->createTable("PageTag", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "name"=>"varchar(128) NOT NULL",
    "frequency"=>"int(11) DEFAULT '1'",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_PageTag does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}