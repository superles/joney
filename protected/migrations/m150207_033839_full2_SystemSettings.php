<?php

class m150207_033839_full2_SystemSettings extends CDbMigration
{
	public function up()
	{
	$this->createTable("SystemSettings", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "category"=>"varchar(255)",
    "key"=>"varchar(255)",
    "value"=>"text",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_SystemSettings does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}