<?php

class m150207_033839_full2_StoreCategory extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreCategory", array(
    "id"=>"int(10) unsigned NOT NULL AUTO_INCREMENT",
    "lft"=>"int(10) unsigned",
    "rgt"=>"int(10) unsigned",
    "level"=>"smallint(5) unsigned",
    "url"=>"varchar(255)",
    "full_path"=>"varchar(255)",
    "layout"=>"varchar(255)",
    "view"=>"varchar(255)",
    "description"=>"text",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreCategory does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}