<?php

class m150207_033839_full2_AuthAssignment extends CDbMigration
{
	public function up()
	{
	$this->createTable("AuthAssignment", array(
    "itemname"=>"varchar(64) NOT NULL",
    "userid"=>"varchar(64) NOT NULL",
    "bizrule"=>"text",
    "data"=>"text",
"PRIMARY KEY (itemname,userid)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_AuthAssignment does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}