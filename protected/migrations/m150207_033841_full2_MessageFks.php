<?php

class m150207_033841_full2_MessageFks extends CDbMigration
{
	public function up()
	{
	$this->addForeignKey("Message_ibfk_1", "Message", "id", "SourceMessage", "id");

	}

	public function down()
	{
		echo "m150207_033841_full2_MessageFks does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}