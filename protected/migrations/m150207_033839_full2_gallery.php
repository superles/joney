<?php

class m150207_033839_full2_gallery extends CDbMigration
{
	public function up()
	{
	$this->createTable("gallery", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "versions_data"=>"text NOT NULL",
    "name"=>"tinyint(1) NOT NULL DEFAULT '1'",
    "description"=>"tinyint(1) NOT NULL DEFAULT '1'",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_gallery does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}