<?php

class m150207_033839_full2_Message extends CDbMigration
{
	public function up()
	{
	$this->createTable("Message", array(
    "id"=>"int(11) NOT NULL",
    "language"=>"varchar(16) NOT NULL",
    "translation"=>"text",
"PRIMARY KEY (id,language)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_Message does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}