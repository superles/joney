<?php

class m150207_033839_full2_OrderHistory extends CDbMigration
{
	public function up()
	{
	$this->createTable("OrderHistory", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "order_id"=>"int(11)",
    "user_id"=>"int(11)",
    "username"=>"varchar(255)",
    "handler"=>"varchar(255)",
    "data_before"=>"text",
    "data_after"=>"text",
    "created"=>"datetime",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_OrderHistory does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}