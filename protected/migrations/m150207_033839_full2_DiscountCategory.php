<?php

class m150207_033839_full2_DiscountCategory extends CDbMigration
{
	public function up()
	{
	$this->createTable("DiscountCategory", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "discount_id"=>"int(11)",
    "category_id"=>"int(11)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_DiscountCategory does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}