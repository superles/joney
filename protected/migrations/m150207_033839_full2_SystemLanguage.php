<?php

class m150207_033839_full2_SystemLanguage extends CDbMigration
{
	public function up()
	{
	$this->createTable("SystemLanguage", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "name"=>"varchar(100)",
    "code"=>"varchar(25)",
    "locale"=>"varchar(100)",
    "default"=>"tinyint(1)",
    "flag_name"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_SystemLanguage does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}