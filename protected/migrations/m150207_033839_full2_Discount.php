<?php

class m150207_033839_full2_Discount extends CDbMigration
{
	public function up()
	{
	$this->createTable("Discount", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "name"=>"varchar(255)",
    "active"=>"tinyint(1)",
    "sum"=>"varchar(10)",
    "start_date"=>"datetime",
    "end_date"=>"datetime",
    "roles"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_Discount does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}