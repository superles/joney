<?php

class m150207_033839_full2_poll_choice extends CDbMigration
{
	public function up()
	{
	$this->createTable("poll_choice", array(
    "id"=>"int(11) unsigned NOT NULL AUTO_INCREMENT",
    "poll_id"=>"int(11) unsigned NOT NULL",
    "label"=>"varchar(255) NOT NULL",
    "votes"=>"int(11) unsigned NOT NULL DEFAULT '0'",
    "weight"=>"int(11) NOT NULL",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_poll_choice does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}