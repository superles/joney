<?php

class m150207_033839_full2_StoreProductImage extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreProductImage", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "product_id"=>"int(11)",
    "name"=>"varchar(255)",
    "is_main"=>"tinyint(1)",
    "uploaded_by"=>"int(11)",
    "date_uploaded"=>"datetime",
    "title"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreProductImage does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}