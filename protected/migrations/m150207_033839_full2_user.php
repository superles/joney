<?php

class m150207_033839_full2_user extends CDbMigration
{
	public function up()
	{
	$this->createTable("user", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "username"=>"varchar(255)",
    "password"=>"varchar(255)",
    "email"=>"varchar(255)",
    "created_at"=>"datetime",
    "last_login"=>"datetime",
    "login_ip"=>"varchar(255)",
    "recovery_key"=>"varchar(20)",
    "recovery_password"=>"varchar(100)",
    "discount"=>"varchar(255)",
    "banned"=>"tinyint(1)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_user does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}