<?php

class m150207_033839_full2_StoreProductAttributeEAV extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreProductAttributeEAV", array(
    "entity"=>"int(11) unsigned NOT NULL",
    "attribute"=>"varchar(250)",
    "value"=>"text",
), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreProductAttributeEAV does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}