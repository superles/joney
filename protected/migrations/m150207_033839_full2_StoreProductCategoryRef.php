<?php

class m150207_033839_full2_StoreProductCategoryRef extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreProductCategoryRef", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "product"=>"int(11)",
    "category"=>"int(11)",
    "is_main"=>"tinyint(1)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreProductCategoryRef does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}