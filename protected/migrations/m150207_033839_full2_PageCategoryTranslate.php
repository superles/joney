<?php

class m150207_033839_full2_PageCategoryTranslate extends CDbMigration
{
	public function up()
	{
	$this->createTable("PageCategoryTranslate", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "object_id"=>"int(11) NOT NULL",
    "language_id"=>"int(11) NOT NULL",
    "name"=>"varchar(255)",
    "description"=>"text",
    "meta_title"=>"varchar(255)",
    "meta_description"=>"varchar(255)",
    "meta_keywords"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_PageCategoryTranslate does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}