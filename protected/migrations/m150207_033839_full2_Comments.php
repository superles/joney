<?php

class m150207_033839_full2_Comments extends CDbMigration
{
	public function up()
	{
	$this->createTable("Comments", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "user_id"=>"int(11)",
    "class_name"=>"varchar(100)",
    "object_pk"=>"int(11)",
    "status"=>"tinyint(4)",
    "email"=>"varchar(255)",
    "name"=>"varchar(50)",
    "text"=>"text",
    "created"=>"datetime",
    "updated"=>"datetime",
    "ip_address"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_Comments does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}