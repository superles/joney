<?php

class m150207_033839_full2_StoreTypeAttribute extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreTypeAttribute", array(
    "type_id"=>"int(11) NOT NULL",
    "attribute_id"=>"int(11) NOT NULL",
"PRIMARY KEY (type_id,attribute_id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreTypeAttribute does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}