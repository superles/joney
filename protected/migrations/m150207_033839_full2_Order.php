<?php

class m150207_033839_full2_Order extends CDbMigration
{
	public function up()
	{
	$this->createTable("Order", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "user_id"=>"int(11)",
    "secret_key"=>"varchar(10)",
    "delivery_id"=>"int(11)",
    "delivery_price"=>"float(10,2)",
    "total_price"=>"float(10,2)",
    "status_id"=>"int(11)",
    "paid"=>"tinyint(1)",
    "user_name"=>"varchar(100)",
    "user_email"=>"varchar(100)",
    "user_address"=>"varchar(255)",
    "user_phone"=>"varchar(30)",
    "user_comment"=>"varchar(500)",
    "ip_address"=>"varchar(50)",
    "created"=>"datetime",
    "updated"=>"datetime",
    "discount"=>"varchar(255)",
    "admin_comment"=>"text",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_Order does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}