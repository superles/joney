<?php

class m150207_033839_full2_AuthItemChild extends CDbMigration
{
	public function up()
	{
	$this->createTable("AuthItemChild", array(
    "parent"=>"varchar(64) NOT NULL",
    "child"=>"varchar(64) NOT NULL",
"PRIMARY KEY (parent,child)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_AuthItemChild does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}