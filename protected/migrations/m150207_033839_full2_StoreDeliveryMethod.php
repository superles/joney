<?php

class m150207_033839_full2_StoreDeliveryMethod extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreDeliveryMethod", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "price"=>"float(10,2) DEFAULT '0.00'",
    "free_from"=>"float(10,2) DEFAULT '0.00'",
    "position"=>"smallint(6)",
    "active"=>"tinyint(1)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreDeliveryMethod does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}