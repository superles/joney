<?php

class m150207_033839_full2_DiscountManufacturer extends CDbMigration
{
	public function up()
	{
	$this->createTable("DiscountManufacturer", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "discount_id"=>"int(11)",
    "manufacturer_id"=>"int(11)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_DiscountManufacturer does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}