<?php

class m150207_033839_full2_StoreProduct extends CDbMigration
{
	public function up()
	{
	$this->createTable("StoreProduct", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "manufacturer_id"=>"int(11)",
    "type_id"=>"int(11)",
    "use_configurations"=>"tinyint(1) NOT NULL",
    "url"=>"varchar(255) NOT NULL",
    "price"=>"float(10,2)",
    "max_price"=>"float(10,2) NOT NULL DEFAULT '0.00'",
    "is_active"=>"tinyint(1)",
    "layout"=>"varchar(255)",
    "view"=>"varchar(255)",
    "sku"=>"varchar(255)",
    "quantity"=>"int(11)",
    "availability"=>"tinyint(2) DEFAULT '1'",
    "auto_decrease_quantity"=>"tinyint(2) DEFAULT '1'",
    "views_count"=>"int(11)",
    "created"=>"datetime",
    "updated"=>"datetime",
    "added_to_cart_count"=>"int(11)",
    "votes"=>"int(11)",
    "rating"=>"int(11)",
    "discount"=>"varchar(255)",
    "video"=>"text",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_StoreProduct does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}