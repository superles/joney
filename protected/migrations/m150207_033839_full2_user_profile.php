<?php

class m150207_033839_full2_user_profile extends CDbMigration
{
	public function up()
	{
	$this->createTable("user_profile", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "user_id"=>"int(11)",
    "full_name"=>"varchar(255)",
    "phone"=>"varchar(20)",
    "delivery_address"=>"varchar(255)",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_user_profile does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}