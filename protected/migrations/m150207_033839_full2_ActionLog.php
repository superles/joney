<?php

class m150207_033839_full2_ActionLog extends CDbMigration
{
	public function up()
	{
	$this->createTable("ActionLog", array(
    "id"=>"int(11) NOT NULL AUTO_INCREMENT",
    "username"=>"varchar(255)",
    "event"=>"tinyint(255)",
    "model_name"=>"varchar(50)",
    "model_title"=>"text",
    "datetime"=>"datetime",
"PRIMARY KEY (id)"), " DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");


	}

	public function down()
	{
		echo "m150207_033839_full2_ActionLog does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}