<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends RController
{
	public $user;
	public $profile;





	/**
	 * @return array
	 */
	public function actions()
	{
		return array(

			'downloadAttachment' => 'ext.attach.DownloadAttachmentAction',
			'deleteAttachment' => 'ext.attach.DeleteAttachmentAction',
			'editAttachment' => 'ext.attach.EditAttachmentAction',
			'captcha' => array(
				'class' => 'CCaptchaAction',
			),
		);
	}

	/**
	 * @var Project
	 */
	public $model;
	/**
	 * @var CActiveDataProvider
	 */
	public $dp;
	/**
	 * @var FeedbackForm
	 */
	public $form;
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();

	/**
	 * @var string
	 */
	public $pageKeywords;

	/**
	 * @var string
	 */
	public $pageDescription;

	/**
	 * @var string
	 */
	private $_pageTitle;

	/**
	 * Set layout and view
	 * @param mixed $model
	 * @param string $view Default view name
	 * @return string
	 */
	protected function setDesign($model, $view)
	{
		// Set layout
		if ($model->layout)
			$this->layout = $model->layout;

		// Use custom page view
		if ($model->view)
			$view = $model->view;

		return $view;
	}

	/**
	 * @param $message
	 */
	public  function addFlashMessage($message)
	{
		$currentMessages = Yii::app()->user->getFlash('messages');

		if (!is_array($currentMessages))
			$currentMessages = array();

		Yii::app()->user->setFlash('messages', CMap::mergeArray($currentMessages, array($message)));
	}

	public function setPageTitle($title)
	{
		$this->_pageTitle=$title;
	}


	public function getPageTitle()
	{
		$title=Yii::app()->settings->get('core', 'siteName');
		if(!empty($this->_pageTitle))
			$title=$this->_pageTitle.=' / '.$title;
		return $title;
	}

	public function beforeAction($action){
		if(parent::beforeAction($action)){

			if(request()->getPost('FeedbackForm',false)){


				$this->form=new FeedbackForm();


				$this->form->attributes=request()->getPost('FeedbackForm',false);
				if($this->form->validate()){
					$this->form->sendMessage();
					$this->form->unsetAttributes();
					if(request()->isAjaxRequest){
						ob_end_flush();

						echo 'Спасибо. Ваше сообщение отправлено.';
						exit;
					}else{
						$this->addFlashMessage('Спасибо. Ваше сообщение отправлено.');
					}



				}

			}else{
				$this->form=new FeedbackForm();
			}


			/*
                        $this->user = new User('register');
                        $this->profile = new UserProfile;

                        if(Yii::app()->request->isPostRequest && isset($_POST['User'], $_POST['UserProfile']))
                        {
                            $this->user->attributes = $_POST['User'];
                            $this->profile->attributes = $_POST['UserProfile'];

                            $valid = $this->user->validate();
                            $valid = $this->profile->validate() && $valid;

                            if($valid)
                            {
                                $this->user->save();
                                $this->profile->save();
                                $this->profile->setUser($this->user);

                                if($this->user->isMaster&&isset($_POST['UserSpecializations'])){
                                    foreach ($_POST['UserSpecializations']['category_id'] as $s){
                                        $spec=new UserSpecializations();
                                        $spec->user_id=$this->user->id;
                                        $spec->category_id=$s;
                                        $spec->save();
                                    }


                                }

                                // Add user to authenticated group
                                Yii::app()->authManager->assign('Authenticated', $this->user->id);
                                switch ($this->user->utype) {
                                    case 'customer':
                                        app()->authManager->assign('Customer', $this->user->id);
                                        break;
                                    case 'master':
                                        app()->authManager->assign('Master', $this->user->id);
                                        break;

                                }
                                $this->addFlashMessage(Yii::t('UsersModule.core', 'Спасибо за регистрацию на нашем сайте.'));

                                // Authenticate user
                                $identity = new UserIdentity($this->user->username, $_POST['User']['password']);
                                if($identity->authenticate())
                                {
                                    Yii::app()->user->login($identity, Yii::app()->user->rememberTime);

                                }
                            }
                        }*/

			return true;
		}else{
			return false;
		}
	}
}