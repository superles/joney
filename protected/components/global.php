<?php
/**
 * global.php file.
 * Global shorthand functions for commonly used Yii methods.
 * @author Christoffer Niska <christoffer.niska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2013-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

defined('DS') or define('DS', DIRECTORY_SEPARATOR);
if(is_file(dirname(__FILE__)."/../vendor/autoload.php")){
    require dirname(__FILE__)."/../vendor/autoload.php";
}

require 'simple_html_dom.php';
/**
 * Returns the application instance.
 * @return SWebApplication
 */
function app()
{
    return Yii::app();
}

function cache($name,$value=null,$dp=null){
    if($value===null){
        return app()->cache->get($name);
    }else{
        return app()->cache->set($name,$value,0,$dp);
    }

}

function cookies($name,$value=null){
    if(empty($value)){
        return app()->request->cookies->itemAt($name)?app()->request->cookies->itemAt($name)->value:false;
    }else{

        $cookie=new CHttpCookie($name,$value);
        app()->request->cookies->add($name,$cookie);
    }

}

/**
 * Returns the application parameter with the given name.
 * @param $name
 * @return mixed
 */
function param($name)
{
    return isset(Yii::app()->params[$name]) ? Yii::app()->params[$name] : null;
}

/**
 * Returns the client script instance.
 * @return CClientScript
 */
function clientScript()
{
    return Yii::app()->getClientScript();
}

/**
 * Returns the main database connection.
 * @return CDbConnection
 */
function db()
{
    return Yii::app()->getDb();
}

/**
 * Returns the formatter instance.
 * @return Formatter
 */
function format()
{
    return Yii::app()->getComponent('format');
}

/**
 * Returns the date formatter instance.
 * @return CDateFormatter
 */
function dateFormatter()
{
    return Yii::app()->getDateFormatter();
}

/**
 * Returns the date formatter instance.
 * @return CDateFormatter
 */
function numberFormatter()
{
    return Yii::app()->getNumberFormatter();
}

/**
 * Returns the request instance.
 * @return CHttpRequest
 */
function request()
{
    return Yii::app()->getRequest();
}

/**
 * Returns the session instance.
 * @return CHttpSession
 */
function session()
{
    return Yii::app()->getSession();
}

/**
 * Returns the web user instance for the logged in user.
 * @return BaseUser
 */
function user()
{
    return Yii::app()->getUser();
}

/**
 * Translates the given string using Yii::t().
 * @param $category
 * @param $message
 * @param array $params
 * @param string $source
 * @param string $language
 * @return string
 */
function t($category, $message, $params = array(), $source = null, $language = null)
{
    return Yii::t($category, $message, $params, $source, $language);
}

function tt($txt){

    return Yii::t('PagesModule.site',$txt);
}

/**
 * Returns the base URL for the given URL.
 * @param string $url
 * @return string
 */
function baseUrl($url = '')
{
    static $baseUrl;
    if (!isset($baseUrl)) {
        $baseUrl = Yii::app()->request->baseUrl;
    }
    $url=$baseUrl . '/' . ltrim($url, '/');
    return $url;
}
function basePath($url = '')
{
    static $basePath;
    if (!isset($basePath)) {
        $basePath = Yii::getPathOfAlias('webroot');
    }
    return $basePath . ltrim($url);
}

/**
 * Registers the given CSS file.
 * @param $url
 * @param string $media
 */
function css($url, $media = '')
{
    Yii::app()->clientScript->registerCssFile(baseUrl($url), $media);
}

/**
 * Registers the given JavaScript file.
 * @param $url
 * @param null $position
 */
function js($url, $position = null)
{
    Yii::app()->clientScript->registerScriptFile(baseUrl($url), $position);
}

/**
 * Escapes the given string using CHtml::encode().
 * @param $text
 * @return string
 */
function e($text)
{
    return CHtml::encode($text);
}

/**
 * Returns the escaped value of a model attribute.
 * @param $model
 * @param $attribute
 * @param null $defaultValue
 * @return string
 */
function v($model, $attribute, $defaultValue = null)
{
    return CHtml::encode(CHtml::value($model, $attribute, $defaultValue));
}

/**
 * Purifies the given HTML.
 * @param $text
 * @return string
 */
function purify($text)
{
    static $purifier;
    if (!isset($purifier)) {
        $purifier = new CHtmlPurifier;
    }
    return $purifier->purify($text);
}

/**
 * Returns the given markdown text as purified HTML.
 * @param $text
 * @return string
 */
function markdown($text)
{
    static $parser;
    if (!isset($parser)) {
        $parser = new MarkdownParser;
    }
    return $parser->safeTransform($text);
}

/**
 * Creates an image tag using CHtml::image().
 * @param $src
 * @param string $alt
 * @param array $htmlOptions
 * @return string
 */
function img($src, $alt = '', $htmlOptions = array())
{
    return CHtml::image(baseUrl($src), $alt, $htmlOptions);
}

/**
 * Creates a link to the given url using CHtml::link().
 * @param $text
 * @param string $url
 * @param array $htmlOptions
 * @return string
 */
function l($text, $url = '#', $htmlOptions = array())
{
    return CHtml::link($text, $url, $htmlOptions);
}

/**
 * Creates a relative URL using CUrlManager::createUrl().
 * @param $route
 * @param array $params
 * @param string $ampersand
 * @return mixed
 */
function url($route, $params = array(), $ampersand = '&')
{
    return Yii::app()->urlManager->createUrl($route, $params, $ampersand);
}

/**
 * Encodes the given object using json_encode().
 * @param mixed $value
 * @param integer $options
 * @return string
 */
function jsonEncode($value, $options = 0)
{
    return json_encode($value, $options);
}

/**
 * Decodes the given JSON string using json_decode().
 * @param $string
 * @param boolean $assoc
 * @param integer $depth
 * @param integer $options
 * @return mixed
 */
function jsonDecode($string, $assoc = true, $depth = 512, $options = 0)
{
    return json_decode($string, $assoc, $depth, $options);
}

/**
 * Returns the current time as a MySQL date.
 * @param integer $timestamp the timestamp.
 * @return string the date.
 */
function sqlDate($timestamp = null)
{
    if ($timestamp === null) {
        $timestamp = time();
    }
    return date('Y-m-d', $timestamp);
}

/**
 * Returns the current time as a MySQL date time.
 * @param integer $timestamp the timestamp.
 * @return string the date time.
 */
function sqlDateTime($timestamp = null)
{
    if ($timestamp === null) {
        $timestamp = time();
    }
    return date('Y-m-d H:i:s', $timestamp);
}

/**
 * Dumps the given variable using CVarDumper::dumpAsString().
 * @param mixed $var
 * @param int $depth
 * @param bool $highlight
 */
function dumpAsString($var, $depth = 10, $highlight = true)
{
    echo CVarDumper::dumpAsString($var, $depth, $highlight);
}

function strTruncate($string, $limit, $break=".", $pad="...")
{
    // return with no change if string is shorter than $limit
    if(mb_strlen($string,'UTF-8') <= $limit) return $string;

    // is $break present between $limit and the end of the string?
    if(false !== ($breakpoint = mb_strpos($string, $break, $limit,'UTF-8'))) {
        if($breakpoint < mb_strlen($string) - 1) {
            $string = mb_substr($string, 0, $breakpoint,'UTF-8') . $pad;
        }
    }

    return $string;
}
function strip_tags_smart(
    /*string*/ $s,
               array $allowable_tags = null,
    /*boolean*/ $is_format_spaces = true,
               array $pair_tags = array('script', 'style', 'map', 'iframe', 'frameset', 'object', 'applet', 'comment', 'button', 'textarea', 'select'),
               array $para_tags = array('p', 'td', 'th', 'li', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'div', 'form', 'title', 'pre')
)
{
    //return strip_tags($s);
    static $_callback_type  = false;
    static $_allowable_tags = array();
    static $_para_tags      = array();
    #regular expression for tag attributes
    #correct processes dirty and broken HTML in a singlebyte or multibyte UTF-8 charset!
    static $re_attrs_fast_safe =  '(?![a-zA-Z\d])  #statement, which follows after a tag
                                   #correct attributes
                                   (?>
                                       [^>"\']+
                                     | (?<=[\=\x20\r\n\t]|\xc2\xa0) "[^"]*"
                                     | (?<=[\=\x20\r\n\t]|\xc2\xa0) \'[^\']*\'
                                   )*
                                   #incorrect attributes
                                   [^>]*+';

    if (is_array($s))
    {
        if ($_callback_type === 'strip_tags')
        {
            $tag = strtolower($s[1]);
            if ($_allowable_tags)
            {
                #tag with attributes
                if (array_key_exists($tag, $_allowable_tags)) return $s[0];

                #tag without attributes
                if (array_key_exists('<' . $tag . '>', $_allowable_tags))
                {
                    if (substr($s[0], 0, 2) === '</') return '</' . $tag . '>';
                    if (substr($s[0], -2) === '/>')   return '<' . $tag . ' />';
                    return '<' . $tag . '>';
                }
            }
            if ($tag === 'br') return "\r\n";
            if ($_para_tags && array_key_exists($tag, $_para_tags)) return "\r\n\r\n";
            return '';
        }
        trigger_error('Unknown callback type "' . $_callback_type . '"!', E_USER_ERROR);
    }

    if (($pos = strpos($s, '<')) === false || strpos($s, '>', $pos) === false)  #speed improve
    {
        #tags are not found
        return $s;
    }

    $length = strlen($s);

    #unpaired tags (opening, closing, !DOCTYPE, MS Word namespace)
    $re_tags = '~  <[/!]?+
                   (
                       [a-zA-Z][a-zA-Z\d]*+
                       (?>:[a-zA-Z][a-zA-Z\d]*+)?
                   ) #1
                   ' . $re_attrs_fast_safe . '
                   >
                ~sxSX';

    $patterns = array(
        '/<([\?\%]) .*? \\1>/sxSX',     #встроенный PHP, Perl, ASP код
        '/<\!\[CDATA\[ .*? \]\]>/sxSX', #блоки CDATA
        #'/<\!\[  [\x20\r\n\t]* [a-zA-Z] .*?  \]>/sxSX',  #:DEPRECATED: MS Word таги типа <![if! vml]>...<![endif]>

        '/<\!--.*?-->/sSX', #комментарии

        #MS Word таги типа "<![if! vml]>...<![endif]>",
        #условное выполнение кода для IE типа "<!--[if expression]> HTML <![endif]-->"
        #условное выполнение кода для IE типа "<![if expression]> HTML <![endif]>"
        #см. http://www.tigir.com/comments.htm
        '/ <\! (?:--)?+
               \[
               (?> [^\]"\']+ | "[^"]*" | \'[^\']*\' )*
               \]
               (?:--)?+
           >
         /sxSX',
    );
    if ($pair_tags)
    {
        #парные таги вместе с содержимым:
        foreach ($pair_tags as $k => $v) $pair_tags[$k] = preg_quote($v, '/');
        $patterns[] = '/ <((?i:' . implode('|', $pair_tags) . '))' . $re_attrs_fast_safe . '(?<!\/)>
                         .*?
                         <\/(?i:\\1)' . $re_attrs_fast_safe . '>
                       /sxSX';
    }
    #d($patterns);

    $i = 0; #защита от зацикливания
    $max = 99;
    while ($i < $max)
    {
        $s2 = preg_replace($patterns, '', $s);
        if (preg_last_error() !== PREG_NO_ERROR)
        {
            $i = 999;
            break;
        }

        if ($i == 0)
        {
            $is_html = ($s2 != $s || preg_match($re_tags, $s2));
            if (preg_last_error() !== PREG_NO_ERROR)
            {
                $i = 999;
                break;
            }
            if ($is_html)
            {
                if ($is_format_spaces)
                {
                    /*
                    В библиотеке PCRE для PHP \s - это любой пробельный символ, а именно класс символов [\x09\x0a\x0c\x0d\x20\xa0] или, по другому, [\t\n\f\r \xa0]
                    Если \s используется с модификатором /u, то \s трактуется как [\x09\x0a\x0c\x0d\x20]
                    Браузер не делает различия между пробельными символами, друг за другом подряд идущие символы воспринимаются как один
                    */
                    #$s2 = str_replace(array("\r", "\n", "\t"), ' ', $s2);
                    #$s2 = strtr($s2, "\x09\x0a\x0c\x0d", '    ');
                    $s2 = preg_replace('/  [\x09\x0a\x0c\x0d]++
                                         | <((?i:pre|textarea))' . $re_attrs_fast_safe . '(?<!\/)>
                                           .+?
                                           <\/(?i:\\1)' . $re_attrs_fast_safe . '>
                                           \K
                                        /sxSX', ' ', $s2);
                    if (preg_last_error() !== PREG_NO_ERROR)
                    {
                        $i = 999;
                        break;
                    }
                }

                #массив тагов, которые не будут вырезаны
                if ($allowable_tags) $_allowable_tags = array_flip($allowable_tags);

                #парные таги, которые будут восприниматься как параграфы
                if ($para_tags) $_para_tags = array_flip($para_tags);
            }
        }#if

        #tags processing
        if ($is_html)
        {
            $_callback_type = 'strip_tags';
            $s2 = preg_replace_callback($re_tags, __FUNCTION__, $s2);
            $_callback_type = false;
            if (preg_last_error() !== PREG_NO_ERROR)
            {
                $i = 999;
                break;
            }
        }

        if ($s === $s2) break;
        $s = $s2; $i++;
    }#while
    if ($i >= $max) $s = strip_tags($s); #too many cycles for replace...

    if ($is_format_spaces && strlen($s) !== $length)
    {
        #remove a duplicate spaces
        $s = preg_replace('/\x20\x20++/sSX', ' ', trim($s));
        #remove a spaces before and after new lines
        $s = str_replace(array("\r\n\x20", "\x20\r\n"), "\r\n", $s);
        #replace 3 and more new lines to 2 new lines
        $s = preg_replace('/[\r\n]{3,}+/sSX', "\r\n\r\n", $s);
    }
    return $s;
}
/**
 * @return Iwi
 */
function iwi($file){
    if(!app()->hasComponent('iwi')){
        app()->setComponent('iwi',array(
            'class' => 'application.extensions.iwi.IwiComponent',
            // GD or ImageMagick
            'driver' => 'GD',
            // ImageMagick setup path
        ));

    }

    if(!is_file($file)){
        $file=webrooturl_to_path('/styles/img/noimage.png');
    }

    return app()->iwi->load($file);
}

function datalist($model,$text,$is_tree=false,$disable_roots=false){
    /**
     * @var $model CActiveRecord
     */
    if($is_tree){
        foreach ($model->roots()->findAll() as $r){
            /**
             * @var Category $r
             */
            if($disable_roots){
                $data[]=$r->getAttributes(array($text,'id','level'));
            }else{
                $data[]=$r->getAttributes(array($text,'id','level'));
            }
            foreach($r->children()->findAll() as $child){
                $data[]=$child->getAttributes(array($text,'id','root','level'));
            }
        }
        return $data;
    }else{
        return CHtml::listData($model->model()->findAll(),'id',$text);
    }

}

/**
 * @return CFile
 */
function getfile($path){
    return app()->file->set($path);
}

function helper_image($path,$default=false,$exist=true,$model=false){

    if(!$default){
        $default=webroot('/style/images/art/b1.jpg');
    }

    if(!isset($path)||$path==''){
        $path=$default;
    }
    $file=getfile($path);



    if($file->getExists()){
        $e=substr($file->getExtension(),0,3);
        $d=$file->getDirname();
        $realpath=$d.'/'.$file->getFilename().'.'.$e;
        if($realpath!=$file->getRealPath()){
            $file->copy($realpath);
        }

            if($model){
                return $file;
            }else{
                return $realpath;
            }

        }else{
            return $default;
        }


}

/**
 * @param $path
 * @return string
 */
function webroot($path){
    return Yii::getPathOfAlias('webroot').$path;
}
function webrooturl_to_path($path){
    return webroot($path);
}

function webrooturl($path){
    return str_replace(Yii::app()->createAbsoluteUrl('/'),'',$path);
}
function webrootpathurl($path){
    return str_replace(Yii::getPathOfAlias('webroot'),'',$path);
}

/**
 * @return RDbAuthManager
 */
function authManager(){
    return Yii::app()->getAuthManager();
}

/**
 * @return User
 */
function userModel($id=false){
    if(!$id){
        return user()->getModel();
    }else{
        return user()->model($id);
    }
}

function month_ru($mon){
    $mons = array(1 => "Янв", 2 => "Фев", 3 => "Мар", 4 => "Апр", 5 => "Май", 6 => "Июнь", 7 => "Июль", 8 => "Авг", 9 => "Сен", 10 => "Окт", 11 => "Ноя", 12 => "Дек");
    return $mons[intval($mon)];
}

function html(){
    return new TbHtml;
}

function def($value,$default=false){
    if(!isset($value)||empty($value)){
        return $default;

    }else{
        return $value;
    }
}



function set_thumb($file, $photos_dir='/files',$thumbs_dir='/files', $reswidth=167,$resheight=200, $quality=100) {
    //check if thumb exists
    if (!file_exists($thumbs_dir."/".$file)) {
        //get image info
        list($width, $height, $type, $attr) = getimagesize($photos_dir."/".$file);

        //set dimensions
        if(($reswidth-$width)> ($resheight-$height)) {
            $width_t=$reswidth;
            //respect the ratio
            $ratio=$height/$width;
            $height_t=round($ratio*$resheight);
            //set the offset
            $off_y=ceil(($width_t-$height_t)/2);
            $off_x=0;
        } elseif(($reswidth-$width) < ($resheight-$height)) {
            $height_t=$resheight;
            $ratio=$width/$height;
            $width_t=round($ratio*$reswidth);
            $off_x=ceil(($height_t-$width_t)/2);
            $off_y=0;
        }
        else {
            $width_t=$height_t=$reswidth;
            $off_x=$off_y=0;
        }

        if(preg_match('/\.png$/',$file)){
            $thumb=imagecreatefrompng($photos_dir."/".$file);
        }else{
            $thumb=imagecreatefromjpeg($photos_dir."/".$file);
        }

        $thumb_p = imagecreatetruecolor($reswidth, $resheight);
        //default background is black
        $bg = imagecolorallocate ( $thumb_p, 255, 255, 255 );
        imagefill ( $thumb_p, 0, 0, $bg );
        imagecopyresampled($thumb_p, $thumb, $off_x, $off_y, 0, 0, $width_t, $height_t, $width, $height);
        imagejpeg($thumb_p,$thumbs_dir."/".$file,$quality);
    }
}