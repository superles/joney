<?php
return 	array(
    'connectionString'=>'mysql:host=localhost;dbname=joney',
    'username'=>'root',
    'password'=>'1',
    'enableProfiling'       => YII_DEBUG, // Disable in production
    'enableParamLogging'    => YII_DEBUG, // Disable in production
    'emulatePrepare'        => true,
    'schemaCachingDuration' => YII_DEBUG ? 0 : 3600,
    'charset'               => 'utf8',
    'tablePrefix'           =>''
);
?>
