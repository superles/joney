<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder')
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'language'=>'ru',
	'sourceLanguage'=>'xx',
	// pre-loading components
	'preload'=>array('log','translate'),
	'aliases' => array(




		'bootstrap' => 'ext.bootstrap',
		'translate'=>'application.modules.translate'



	),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.modules.translate.TranslateModule',
		'application.modules.pages.models.*',
		'application.modules.pages.components.*',
		'application.components.*',
		'application.components.payment.*',
		'application.components.validators.*',
		'application.modules.core.models.*',
		'application.modules.orders.models.*',
		'application.modules.users.models.User',
		// Rights module
		'application.modules.rights.*',
		'application.modules.rights.components.*',



		'application.modules.feedback.models.*',


		'application.modules.poll.models.*',
		'application.modules.poll.components.*',


		'bootstrap.helpers.*',
		'bootstrap.form.*',
		'bootstrap.behaviors.*',
		'bootstrap.widgets.*',

		'ext.file.*',

		'ext.gallerymanager.*',
		'ext.gallerymanager.models.*',


	),

	'modules'=>array(

		'action_logger',
		'admin'=>array(),
		'rights'=>array(
			'layout'=>'application.modules.admin.views.layouts.main',
			'cssFile'=>false,
			'debug'=>YII_DEBUG,
		),
		'core',

		'poll' => array(
			// Force users to vote before seeing results
			'forceVote' => TRUE,
			// Restrict anonymous votes by IP address,
			// otherwise it's tied only to user_id
			'ipRestrict' => TRUE,
			// Allow guests to cancel their votes
			// if ipRestrict is enabled
			'allowGuestCancel' => FALSE,
		),

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'1',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array(
				'192.168.1.13',
				'127.0.0.1',
				'::1'
			),
			'generatorPaths'=>array(
				'bootstrap.gii',   // псевдоним пути
			),
		),
	),
	'controllerMap'=>array(
		'gallery'=>array(
			'class'=>'ext.gallerymanager.GalleryController'
		)
	),
	// application components
	'components'=>array(
		'messages'=>array(
			'class'=>'SMessageSource',
			'onMissingTranslation' => array('TranslateModule', 'missingTranslation'),
		),
		'translate'=>array(//if you name your component something else change TranslateModule
			'class'=>'application.modules.translate.components.MPTranslate',
			//any avaliable options here
			'acceptedLanguages'=>array(
				'ru'=>'Русский',
				'en'=>'English',

			),
		),
		'settings'=>array(
			'class'=>'application.components.SSystemSettings'
		),
		'mobile'=>array(
			'class'=>'ext.mobile.MobileDetect'
		),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class'=>'BaseUser',
			'loginUrl'=>'/users/login'
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'class'=>'SUrlManager',
			'showScriptName'=>false,
			'useStrictParsing'=>true,
			'rules'=>array(
				'/'=>'store/index/index',
				'admin/auth'=>'admin/auth',
				'admin/auth/logout'=>'admin/auth/logout',
				'admin/<module:\w+>'=>'<module>/admin/default',
				'admin/<module:\w+>/<controller:\w+>'=>'<module>/admin/<controller>',
			//	'admin/<module:\w+>/<controller:\w+>/<id:\d+>'=>'<module>/admin/<controller>/update',
				'admin/<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/admin/<controller>/<action>',
				'admin/<module:\w+>/<controller:\w+>/<action:\w+>/*'=>'<module>/admin/<controller>/<action>',

				'filemanager/connector' => 'admin/fileManager/index',

				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

				'admin'=>'admin/default/index',
				'rights'=>'rights/assignment/view',
				'rights/<controller:\w+>/<id:\d+>'=>'rights/<controller>/view',
				'rights/<controller:\w+>/<action:\w+>/<id:\d+>'=>'rights/<controller>/<action>',
				'rights/<controller:\w+>/<action:\w+>'=>'rights/<controller>/<action>',
				'gii'=>'gii',
				'gii/<controller:\w+>'=>'gii/<controller>',
				'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
			),
		),
		'db'=>require("db.php"),
		'request'=>array(
			'class'=>'SHttpRequest',
			'enableCsrfValidation'=>true,
			'enableCookieValidation'=>true,
			'noCsrfValidationRoutes'=>array(
				'/processPayment',
				'/site/success',
				'/site/fail',
				'/admin/pages/default/imageUpload',
				'/admin/pages/default/imageList',
				'/admin/pages/default/fileUpload',
'/admin/pages/default/update',
				'/accounting1c/default/',
				'/filemanager/connector',
				'/admin/pages/default/editAttachment'
			)
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'authManager'=>array(
			'class'=>'RDbAuthManager',
			'connectionID'=>'db',
		),
		'cache'=>array(
			'class'=>'CDummyCache',
		),
		'languageManager'=>array(
			'class'=>'SLanguageManager'
		),
		'fixture'=>array(
			'class'=>'system.test.CDbFixtureManager',
		),
		'cart'=>array(
			'class'=>'ext.cart.SCart',
		),
		'currency'=>array(
			'class'=>'store.components.SCurrencyManager'
		),
		'mail'=>array(
			'class'=>'ext.mailer.EMailer',
			'CharSet'=>'UTF-8',
		),

		'log'=>YII_DEBUG===true ? require('logging.php') : null,
		'iwi' => array(
			'class' => 'application.extensions.iwi.IwiComponent',
			// GD or ImageMagick
			'driver' => 'GD',
			// ImageMagick setup path
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),
		'file'=>array(
			'class' => 'ext.file.CFile'
		),
		'fileManager' => array(
			'class' => 'ext.yii-filemanager.components.FileManager',

		),
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		'adminPageSize'=>30,
	),
);
