<?php

error_reporting(0);

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
            'db'=>array(
                'connectionString'=>'mysql:host=localhost;dbname=cms',
                'username'=>'root',
                'password'=>'Gfhjkm6804231',
                'enableProfiling'       => YII_DEBUG, // Disable in production
                'enableParamLogging'    => YII_DEBUG, // Disable in production
                'emulatePrepare'        => true,
                'schemaCachingDuration' => YII_DEBUG ? 0 : 3600,
                'charset'               => 'utf8',
            ),
		),
	)
);
