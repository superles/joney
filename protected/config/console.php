<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'cms',
	'components'=>array(
        'db'=>array(
            'connectionString'=>'mysql:host=localhost;dbname=cms',
            'username'=>'root',
            'password'=>'Gfhjkm6804231',
            'enableProfiling'       => YII_DEBUG, // Disable in production
            'enableParamLogging'    => YII_DEBUG, // Disable in production
            'emulatePrepare'        => true,
            'schemaCachingDuration' => YII_DEBUG ? 0 : 3600,
            'charset'               => 'utf8',
        ),
	),
    'commandMap' => array(


        'deft' => array(
            'class' => 'application.commands.DeftmigrationsCommand',

        ),

    )
);