<?php
/**
 * Behavior for adding gallery to any model.
 *
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 */
Yii::import('ext.gallerymanager.models.Gallery');
Yii::import('ext.gallerymanager.models.GalleryPhoto');
class GalleryBehavior extends CActiveRecordBehavior
{
    /** @var string Model attribute name to store created gallery id */
    public $idAttribute;
    /**
     * @var array Settings for image auto-generation
     * @example
     *  array(
     *       'small' => array(
     *              'resize' => array(200, null),
     *       ),
     *      'medium' => array(
     *              'resize' => array(800, null),
     *      )
     *  );
     */


    public $versions;
    /** @var boolean does images in gallery need names */
    public $name;
    /** @var boolean does images in gallery need descriptions */
    public $description;

    private $photos;
    private $_photos;

    private $_gallery;




    public function afterFind($event){




        if(cache("gallery_".$this->getOwner()->{$this->idAttribute})){
            $this->_photos = cache("gallery_".$this->getOwner()->{$this->idAttribute});
        }else{
            foreach($this->getPhotos() as $photo){
                $this->_photos[$photo->cat?:'default'][]=$photo;
            }
            cache("gallery_".$this->getOwner()->{$this->idAttribute},$this->_photos);
        }

        parent::afterFind($event);

    }

    /** Will create new gallery after save if no associated gallery exists */
    public function beforeSave($event)
    {
        parent::beforeSave($event);
        if ($event->isValid) {
            if (empty($this->getOwner()->{$this->idAttribute})) {
                $gallery = new Gallery();
                $gallery->name = $this->name;
                $gallery->description = $this->description;

                $gallery->versions = $this->versions;
                $gallery->save();

                $this->getOwner()->{$this->idAttribute} = $gallery->id;
            }
        }
    }

    /** Will remove associated Gallery before object removal */
    public function beforeDelete($event)
    {
        $gallery = $this->getGallery();
        if ($gallery !== null) {
            $gallery->delete();
        }
        parent::beforeDelete($event);
    }

    /** Method for changing gallery configuration and regeneration of images versions */
    public function changeConfig()
    {
        $gallery = $this->getGallery();
        if ($gallery == null) return;

        if ($gallery->versions_data != serialize($this->versions)) {
            foreach ($gallery->galleryPhotos as $photo) {
                $photo->removeImages();
            }

            $gallery->name = $this->name;
            $gallery->description = $this->description;

            $gallery->versions = $this->versions;
            $gallery->save();

            foreach ($gallery->galleryPhotos as $photo) {
                $photo->updateImages();
            }
        }
    }

    public function setGallery($gallery)
    {
        if (empty($this->_gallery)) {
            $this->_gallery = $gallery;
        }

    }

    /** @return Gallery Returns gallery associated with model */
    public function getGallery()
    {
        if (empty($this->_gallery)) {
            $this->_gallery = Gallery::model()->findByPk($this->getOwner()->{$this->idAttribute});
        }

        if(!$this->_gallery) {
            $this->_gallery=new Gallery();
            $this->_gallery->setVersions(unserialize('a:2:{s:8:"projectr";a:1:{s:8:"adaptive";a:2:{i:0;i:100;i:1;i:100;}}s:4:"main";a:1:{s:8:"adaptive";a:2:{i:0;i:270;i:1;i:220;}}}'));
            $this->_gallery->save();
            $this->getOwner()->{$this->idAttribute}=$this->_gallery->getAttribute('id');
        }
        return $this->_gallery;
    }

    /** @return GalleryPhoto[] Photos from associated gallery */
    public function getPhotos($cat=false)
    {
        $criteria = new CDbCriteria();
        if(!$cat){
            $criteria->condition = 'gallery_id = :gallery_id';
        }else{
            $criteria->condition = 'gallery_id = :gallery_id and cat="'.$cat.'"';
        }

        $criteria->params[':gallery_id'] = $this->getOwner()->{$this->idAttribute};
        $criteria->order = '`rank` asc';
        //if(!YII_DEBUG&&cache(json_encode($criteria->toArray()))){
        $models=GalleryPhoto::model()->findAll($criteria);

        return $models;
    }

    /**
     * @param bool $cat
     * @return GalleryPhoto[]
     */
    public function getGalleryPhotos($cat=false){
        $photos=false;
        if(!empty($this->_photos)){
            if(!$cat){
                $photos=array();
                foreach ($this->_photos as $cat=>$photos_cat) {
                    foreach ($photos_cat as $photo) {
                        $photos[]=$photo;
                    }

                }



            }elseif(!empty($this->_photos[$cat])){
                $photos=array();

                foreach ($this->_photos[$cat] as $photo) {
                    $photos[]=$photo;
                }



            }
        }
        return $photos;
    }
}
