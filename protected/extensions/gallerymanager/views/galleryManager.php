<?php
/**
 * @var $this GalleryManager
 * @var $model GalleryPhoto
 *
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 */
app()->bootstrap->registerModal();
Yii::import('ext.ckeditor.*');
$editor=new CKEditorWidget();

$editor->registerClientScript();
?>

<?php echo CHtml::openTag('div', $this->htmlOptions); ?>
    <!-- Gallery Toolbar -->
<div class="panel panel-default">
  <div class="panel-heading">
     <span class="btn btn-success fileinput-button">
            <i class="fa fa-plus fa fa-white"></i>
         <?php echo Yii::t('galleryManager.main', 'Добавить…');?>
         <input type="file" name="image" class="afile form-control" accept="image/*" multiple="multiple"/>
        </span>
  </div>
  <div class="panel-body">
      <!-- Gallery Photos -->
      <div class="sorter">
          <div class="images"></div>
          <br style="clear: both;"/>
      </div>
  </div>
  <div class="panel-footer">
      <div class="btn-toolbar gform">


          <div class="pull-left lh28">

                     <?php echo Yii::t('galleryManager.main', 'Выбрать все');?>
              <span class="ml10 ">
       <input type="checkbox" style="margin: 0;" class="select_all"/>
      </span>
              </div>
          <div class="pull-left ml30">
   <span class="pull-left">

              <span class="btn disabled edit_selected"><i class="fa fa-pencil"></i> <?php echo Yii::t('galleryManager.main', 'Редактировать');?></span>
         </span>
               <span class="pull-left">
              <span class="btn disabled remove_selected"><i class="fa fa-times"></i> <?php echo Yii::t('galleryManager.main', 'Удалить');?></span>
                    </span>
          </div>
      </div>
  </div>
</div>




    <!-- Modal window to edit photo information -->
    <div class="modal hide editor-modal">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">×</a>

            <h3><?php echo Yii::t('galleryManager.main', 'Редактировать')?></h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">

            <div class="form ">

            </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
<div class="pull-right">
                    <a href="#" class="btn btn-primary save-changes">
                        <?php echo Yii::t('galleryManager.main', 'Сохранить')?>
                    </a>
                    <a href="#" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('galleryManager.main', 'Закрыть')?></a>
</div>
                </div>
            </div>
        </div>
        <div class="modal-footer">

        </div>
    </div>
    <div class="overlay">
        <div class="overlay-bg">&nbsp;</div>
        <div class="drop-hint">
            <span class="drop-hint-info"><?php echo Yii::t('galleryManager.main', 'Переместить сюда…')?></span>
        </div>
    </div>
    <div class="progress-overlay">
        <div class="overlay-bg">&nbsp;</div>
        <!-- Upload Progress Modal-->
        <div class="modal progress-modal">
            <div class="modal-header">
                <h3><?php echo Yii::t('galleryManager.main', 'Загрузка изображений…')?></h3>
            </div>
            <div class="modal-body">
                <div class="progress progress-striped active">
                    <div class="bar upload-progress"></div>
                </div>
            </div>
        </div>
    </div>
<?php echo CHtml::closeTag('div'); ?>