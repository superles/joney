<?php
/**
 * The following variables are available in this template:
 * - $this: the BootstrapCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
<?php echo "?>\n"; ?>

<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('main','$label')=>array('index'),
	Yii::t('main','Create'),
);\n";
?>

$this->menu=array(
	array('label'=>Yii::t('main','List <?php echo $this->modelClass; ?>'), 'url'=>array('index')),
	array('label'=>Yii::t('main','Manage <?php echo $this->modelClass; ?>'), 'url'=>array('admin')),
);
?>

<?php echo "<?php echo "?>Yii::t('main','Create <?php echo $this->modelClass; ?>')<?php echo "?>"?>

<?php echo "<?php \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>