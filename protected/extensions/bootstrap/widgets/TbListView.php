<?php
/**
 * TbListView class file.
 * @author Christoffer Niska <christoffer.niska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2013-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 */

Yii::import('zii.widgets.CListView');

/**
 * Bootstrap Zii list view.
 */
class TbListView extends CListView
{
    public $disableJS=false;
    /**
     * @var string the CSS class name for the pager container. Defaults to 'list-pager'.
     */
    public $pagerCssClass = 'pagination-block';
    /**
     * @var string the CSS class name for the summary container. Defaults to 'list-summary'.
     */
    public $summaryCssClass = 'list-summary';
    /**
     * @var string the CSS class name for the items container. Defaults to 'list-items'.
     */
    public $itemsCssClass = 'list-items';
    /**
     * @var array the configuration for the pager.
     * Defaults to <code>array('class'=>'\TbPager')</code>.
     */
    public $pager = array('class' => '\TbPager');
    /**
     * @var string the URL of the CSS file used by this detail view.
     * Defaults to false, meaning that no CSS will be included.
     */
    public $cssFile = false;
    /**
     * @var string the template to be used to control the layout of various sections in the view.
     */
    public $template = "<div class='col-xs-12'>
    <div class='row'>
        {items}
    </div>
</div>\n<div class=\"row\"><div class=\"col-md-6\">{pager}</div><div class=\"col-md-6\">{summary}</div></div>";

    /**
     * Renders the empty message when there is no data.
     */
    public function renderEmptyText()
    {
        $emptyText = $this->emptyText === null ? Yii::t('zii', 'No results found.') : $this->emptyText;
        echo TbHtml::tag('div', array('class' => 'empty', 'span' => 12), $emptyText);
    }

    /**
     * Registers necessary client scripts.
     */
    public function registerClientScript()
    {
        $id=$this->getId();

        if($this->ajaxUpdate===false)
            $ajaxUpdate=array();
        else
            $ajaxUpdate=array_unique(preg_split('/\s*,\s*/',$this->ajaxUpdate.','.$id,-1,PREG_SPLIT_NO_EMPTY));
        $options=array(
            'ajaxUpdate'=>$ajaxUpdate,
            'ajaxVar'=>$this->ajaxVar,
            'pagerClass'=>$this->pagerCssClass,
            'loadingClass'=>$this->loadingCssClass,
            'sorterClass'=>$this->sorterCssClass,
            'enableHistory'=>$this->enableHistory
        );
        if($this->ajaxUrl!==null)
            $options['url']=CHtml::normalizeUrl($this->ajaxUrl);
        if($this->ajaxType!==null)
            $options['ajaxType']=strtoupper($this->ajaxType);
        if($this->updateSelector!==null)
            $options['updateSelector']=$this->updateSelector;
        foreach(array('beforeAjaxUpdate', 'afterAjaxUpdate', 'ajaxUpdateError') as $event)
        {
            if($this->$event!==null)
            {
                if($this->$event instanceof CJavaScriptExpression)
                    $options[$event]=$this->$event;
                else
                    $options[$event]=new CJavaScriptExpression($this->$event);
            }
        }

        $options=CJavaScript::encode($options);
        $cs=Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerCoreScript('bbq');
        if($this->enableHistory)
            $cs->registerCoreScript('history');
        $cs->registerScriptFile($this->baseScriptUrl.'/jquery.yiilistview.js',CClientScript::POS_END);
        $cs->registerScript(__CLASS__.'#'.$id,"jQuery('#$id').yiiListView($options);");
    }


}
