<?php
/**
 * Simple Yii CKEditor widget
 * @property string $configJS
 * @property string $assetsPath
 * @property string $assetsUrl
 * @author Yuriy Firs <firs.yura@gmail.com>
 * @version 0.1.1
 */
class CKEditorWidget extends CInputWidget {




    /**
     * Assets package ID.
     */
    const PACKAGE_ID = 'ckeditor-widget';

    /**
     * @var array Default config
     */
    public function  getConfig(){

        return array(

            'extraPlugins' => user()->isAdmin()?'imagepaste,sourcedialog,codemirror,filebrowser,youtube,slideshow,lineheight,stylesheetparser,autocorrect,save':'imagepaste,sourcedialog,codemirror,youtube,slideshow,lineheight,stylesheetparser,autocorrect',

            'basePath'=>$this->getAssetsUrl(),

            //'startupMode' => 'source',

            'allowedContent' => true,

            'colorButton_colors'=>'000,800000,8B4513,2F4F4F,008080,000080,4B0082,696969,B22222,A52A2A,DAA520,006400,40E0D0,0000CD,800080,808080,F00,FF8C00,FFD700,008000,0FF,00F,EE82EE,A9A9A9,FFA07A,FFA500,FFFF00,00FF00,AFEEEE,ADD8E6,DDA0DD,D3D3D3,FFF0F5,FAEBD7,FFFFE0,F0FFF0,F0FFFF,F0F8FF,E6E6FA,FFF,B29B70,B3B3B3',

//        'removeFormatTags'=>'',
//
//        'fillEmptyBlocks'=>false,
//
//        'useComputedState'=>false,
            'enterMode'=>'CKEDITOR.ENTER_BR',

            'language'=>'ru',

            'browserContextMenuOnCtrl'=>true,

            'contentsCss'=>'/style/css/all.css',

            'bodyPrepend'=>'<section class=""><div class="container">',

            'bodyAppend'=>'</section></div>',
            'bodyClass'=>'container'


        );
    }

    public $package = array();

    public $_id;

    public $inline=false;

    public $auto=false;

    public $theme;


    public $helper='helper.js';
    /**
     * Init widget.
     */
    public function init()
    {


        $app = Yii::app();
        $identifiers=$this->resolveNameID();

        if ($this->name===null)
            $this->name=$identifiers[0];
        if ($this->_id===null)
            $this->_id=$identifiers[1];

        $this->package = array(
            'baseUrl' => $this->assetsUrl,
            'js' => array(
                'ckeditor.js',
            )
        );

        $this->registerClientScript();
    }

    /**
     * Register CSS and Script.
     */
    protected function registerClientScript()
    {
        /**
         * @var CmsNode $model
         */
        if(!$this->inline){
            if(!$this->auto){
                clientScript()->registerScriptFile($this->getAssetsUrl().'/ckeditor.js',CClientScript::POS_HEAD)->registerScriptFile($this->getAssetsUrl().'/helper.js',CClientScript::POS_HEAD)->registerScript(<<<JS

	$('#{$this->id}_help').click(function(e){
	    e.preventDefault();
	    $(this).hide();

        var id='{$this->id}';
        CKEDITOR.replace(id);

    });
JS
                    ,CClientScript::POS_READY)->registerScript(
                    $this->id,$this->getConfigJS()."

    ",
                    CClientScript::POS_END
                );
            }else{
                clientScript()->registerScriptFile($this->getAssetsUrl().'/ckeditor.js',CClientScript::POS_HEAD)->registerScriptFile($this->getAssetsUrl().'/helper.js',CClientScript::POS_HEAD)->registerScript($this->id,<<<JS

	    $('#{$this->id}_help').hide();



        var id='{$this->id}';
        if(CKEDITOR.instances[id]) {
  CKEDITOR.remove(CKEDITOR.instances[id]);
}
        CKEDITOR.replace(id);
        CKEDITOR.instances[id].on("instanceReady", function()
{
//set keyup event
this.document.on("keyup", CK_jQ);

//and paste event
this.document.on("paste", CK_jQ);
});

function CK_jQ()
{
 CKEDITOR.instances[id].updateElement();
}


JS
                    ,CClientScript::POS_READY)->registerScript(
                    $this->id,$this->getConfigJS()."

    ",
                    CClientScript::POS_END
                );
            }
        }else{

            if($this->auto) {

                clientScript()->registerScriptFile($this->getAssetsUrl() . '/ckeditor.js', CClientScript::POS_HEAD)->registerScriptFile($this->getAssetsUrl() . '/helper.js', CClientScript::POS_HEAD)->registerScriptFile($this->getAssetsUrl() . '/adapters/jquery.js', CClientScript::POS_HEAD)->registerScript(
                    $this->id, $this->getConfigJS() . "
  CKEDITOR.disableAutoInline = true;
                CKEDITOR.inline( '" . $this->_id . "',{


                });",
                    CClientScript::POS_END
                );
            }else{
                clientScript()->registerScriptFile($this->getAssetsUrl() . '/ckeditor.js', CClientScript::POS_HEAD)->registerScriptFile($this->getAssetsUrl() . '/helper.js', CClientScript::POS_HEAD)->registerScriptFile($this->getAssetsUrl() . '/adapters/jquery.js', CClientScript::POS_HEAD)->registerScript(
                    $this->id, $this->getConfigJS() . "
  CKEDITOR.disableAutoInline = true;

               	$('#{$this->id}_help').click(function(e){
	    e.preventDefault();
	    $(this).hide();

        var id='{$this->id}';
        CKEDITOR.instances[id].on('instanceReady', function()
{
//set keyup event
this.document.on('keyup', CK_jQ);

//and paste event
this.document.on('paste', CK_jQ);
});

function CK_jQ()
{
 CKEDITOR.instances[id].updateElement();
}
        CKEDITOR.inline(id);

    });
               ",
                    CClientScript::POS_END
                );
            }
        }
    }

    /**
     * Get the assets path.
     * @return string
     */
    public function getAssetsPath()
    {
        return __DIR__ . '/assets';
    }

    /**
     * Print activeTextArea
     */
    public function run()
    {
        $_SESSION['KCFINDER'] = array(
            'disabled' => false
        );
        // add class ckeditor
        if (array_key_exists('class', $this->htmlOptions) && strpos($this->htmlOptions['class'], 'ckeditor') === false) {
            // $this->htmlOptions['class'] .= ' ckeditor';
        } elseif (!array_key_exists('class', $this->htmlOptions))
            // $this->htmlOptions['class'] = 'ckeditor';
            echo "<div class='form-group'>";
        if(user()->isAdmin()){
            if($this->inline){
                echo "<div id='".CHtml::activeId($this->model,$this->attribute)."' name='".CHtml::activeName($this->model,$this->attribute)."' >{$this->model->{$this->attribute}}</div>";
                echo '<div class="hint "><a href="#" onclick="return setupCKEditor(\''.CHtml::activeId($this->model,$this->attribute).'\', this,false,true);"><span class="glyphicon glyphicon-pencil fs20"></span></a></div></div>';
            }else{
                echo TbHtml::activeTextArea($this->model,$this->attribute,$this->htmlOptions);
                echo '<div class="hint "><a href="#" onclick="return setupCKEditor(\''.CHtml::activeId($this->model,$this->attribute).'\', this,true,false);">РЕДАКТОР</a></div></div>';
            }



        }elseif($this->model){
            echo TbHtml::label(!empty($this->htmlOptions['label'])?$this->htmlOptions['label']:$this->model->getAttributeLabel($this->attribute),TbHtml::activeId($this->model,$this->attribute),array('style'=>'display:block'));
            echo TbHtml::activeTextArea($this->model,$this->attribute,$this->htmlOptions);
            echo '<div class="hint "><a href="#" onclick="return setupCKEditor(\''.CHtml::activeId($this->model,$this->attribute).'\', this);">РЕДАКТОР</a></div></div>';
        }elseif($this->name){
            $this->htmlOptions['id']=$this->id;
            echo TbHtml::textAreaControlGroup($this->name,$this->value,$this->htmlOptions);
            echo '<div class="hint "><a href="#" id="'.$this->id.'_help" onclick="return setupCKEditor(\''.CHtml::activeId($this->model,$this->attribute).'\', this);">РЕДАКТОР</a></div></div>';
        }

    }

    /**
     * Publish assets and return url.
     * @return string
     */
    public function getAssetsUrl()
    {
        return app()->assetManager->publish($this->getAssetsPath(),false,-1,YII_DEBUG);
    }

    /**
     * Convert config array to config string
     * @return string
     */
    public function getConfigJS()
    {
        $return = '';

        if(user()->isAdmin()){
            $kcfind=array(
                'filebrowserBrowseUrl'=>$this->getAssetsUrl().'/kcfinder/browse.php?opener=ckeditor&type=files',

                'filebrowserImageBrowseUrl'=>$this->getAssetsUrl().'/kcfinder/browse.php?opener=ckeditor&type=images',
                'filebrowserFlashBrowseUrl'=>$this->getAssetsUrl().'/kcfinder/browse.php?opener=ckeditor&type=flash',
                'filebrowserUploadUrl'=>$this->getAssetsUrl().'/kcfinder/upload.php?opener=ckeditor&type=files',
                'filebrowserImageUploadUrl'=>$this->getAssetsUrl().'/kcfinder/upload.php?opener=ckeditor&type=images',
                'filebrowserFlashUploadUrl'=>$this->getAssetsUrl().'/kcfinder/upload.php?opener=ckeditor&type=flash',
            );
        }else{
            $kcfind=array(
//         'filebrowserBrowseUrl'=>$this->getAssetsUrl().'/kcfinder/browse.php?opener=ckeditor&type=files',
//
//        'filebrowserImageBrowseUrl'=>$this->getAssetsUrl().'/kcfinder/browse.php?opener=ckeditor&type=images',
//        'filebrowserFlashBrowseUrl'=>$this->getAssetsUrl().'/kcfinder/browse.php?opener=ckeditor&type=flash',
//        'filebrowserUploadUrl'=>$this->getAssetsUrl().'/kcfinder/upload.php?opener=ckeditor&type=files',
                'filebrowserImageUploadUrl'=>$this->getAssetsUrl().'/kcfinder/upload.php?opener=ckeditor&type=images',
//        'filebrowserFlashUploadUrl'=>$this->getAssetsUrl().'/kcfinder/upload.php?opener=ckeditor&type=flash',
            );
        }

        $config=CMap::mergeArray($this->getConfig(),$kcfind);
        foreach ($config as $key=>$value) {
            $return .= "config.".$key." = ".json_encode($value)."; ";
        }
        return 'CKEDITOR.editorConfig = function( config ) {
        config.font_names = "baskerville/baskerville;helvetica/helveticaneuecyrlight;bebas/bebas_neue_cyrillicregular;" + config.font_names;
        '.$return.'};';
    }
}
