
	$(document).ready(function() {
		$(".SidebarTabsContent .tab").hide(); //Hide all content
		$(".SidebarTabsContent .tab:first").show(); //Show first tab content
		$(".SidebarTabsControl a:first").addClass('active');
	});

	function SidebarShowTab(id, el)
	{
		$(".SidebarTabsContent .tab").hide();
		$(".SidebarTabsControl a").removeClass('active');
		$("#" + id).show();
		$(el).addClass('active');
		if ($(el).text() == "Адрес") {
		    if (map == null && $('#map_canvas')) {
			initialize();
		    }
		    google.maps.event.trigger(map, 'resize');
		}
	}
