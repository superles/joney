<?php

/**
 * This is the model class for table "file".
 *
 * The followings are the available columns in table 'file':
 * @property integer $id
 * @property integer $class_id
 * @property string $classname
 * @property string $description
 * @property string $title
 * @property string $filename
 */
class AttachmentFile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Files';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('class_id, classname, filename', 'required'),
			array('class_id', 'numerical', 'integerOnly'=>true),
			array('classname, filename,category,title', 'length', 'max'=>255),
            array('description','safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, class_id, classname, filename,category,description,title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'class_id' => 'Class',
			'classname' => 'Classname',
			'filename' => 'Filename',
            'category' => 'category',
            'description' => 'Описание',
            'title' => 'Заголовок',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('class_id',$this->class_id);
		$criteria->compare('classname',$this->classname,true);
		$criteria->compare('filename',$this->filename,true);
        $criteria->compare('category',$this->filename,true);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AttachmentFile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getUrl()
    {
        $model=new $this->classname;
        $model=$model->findByPk($this->class_id);
        /**
         * @var Gift $model
         */



        return app()->assetManager->publish($this->filename,false,-1,YII_DEBUG);


    }
}
