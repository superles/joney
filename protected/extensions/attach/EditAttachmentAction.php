<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.10.14
 * Time: 5:51
 * @property Controller $owner
 * @property AttachmentBehavior $file
 */
Yii::import('ext.attach.*');
class EditAttachmentAction extends CAction{

    public function run($id){

        $file=AttachmentFile::model()->findByPk($id);

        if(app()->request->isAjaxRequest&&$file&&is_file($file->filename)){
            if(request()->getPost('title',false)||request()->getPost('title',false)){
            $file->title=request()->getPost('title',false);
            $file->description=request()->getPost('description',false);
            $file->save();
                echo json_encode(array('status'=>$id));
            }else{
                echo json_encode($file->attributes);
            }

        }else{
            echo json_encode(array('status'=>0));
        }

    }

}