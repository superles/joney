<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.10.14
 * Time: 5:51
 * @property Controller $owner
 * @property AttachmentBehavior $file
 */
Yii::import('ext.attach.*');
class DownloadAttachmentAction extends CAction{

    public function run($id){


        $file=AttachmentFile::model()->findByPk($id);
        $m=new $file->classname;
        /**
         * @var Page $obj
         * @var StoreProduct $product
         */
        $obj=$m->findByPk($file->class_id);
        $product=$obj->childrens[0];
        $ids=array_map(function(Order $el){ return $el->id; },Order::model()->findAllByAttributes(array('user_id'=>user()->id)));
        $criteria=new CDbCriteria();
        $criteria->compare('t.order_id',$ids);
        $criteria->compare('t.product_id',$product->id);

        if($oproduct=OrderProduct::model()->find($criteria)){
            $order=$oproduct->order;
            if(!$order||$order->paid!=1){
                $this->controller->accessDenied('Недостаточно прав для скачивания');
            }
        }else{
            $this->controller->accessDenied('Недостаточно прав для скачивания');
        }

        app()->file->set($file->filename)->download();



    }
}