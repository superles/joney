<?php
Yii::import('ext.attach.*');
/**
 * AttachmentBehavior class file.
 *
 * @author Greg Molnar
 * @link https://github.com/gregmolnar/yii-attachment-behavior/
 * @copyright Copyright &copy; Greg Molnar
 * @license http://opensource.org/licenses/bsd-license.php
 */

/**
 * This behaviour will let you add attachments to your model  easily
 * you will need to add the following database fields to your model:
 * filename string
 * In your model behaviours:
 *
 *    'image' => array(
 *               'class' => 'ext.AttachmentBehavior.AttachmentBehavior',
 *               'attribute' => 'filename',
 *               //'fallback_image' => 'images/sample_image.gif',
 *               'path' => "uploads/:model/:id.:ext",
 *               'processors' => array(
 *                   array(
 *                       'class' => 'ImagickProcessor',
 *                       'method' => 'resize',
 *                       'params' => array(
 *                           'width' => 310,
 *                           'height' => 150,
 *                           'keepratio' => true
 *                       )
 *                   )
 *               ),
 *               'styles' => array(
 *                   'thumb' => '!100x60',
 *               )
 *           ),
 *
 * @property string $path
 * @private string $filename
 * @property Message $owner
 * @private integer $filesize
 * @private string $parsedPath
 * */
class AttachmentBehavior extends CActiveRecordBehavior {

    /**
     * @property string folder to save the attachment
     */
    public $folder = 'uploads';

    /**
     * @property string path to save the attachment
     */
    public $path = ':folder/:id.:ext';

    /**
     * @property name of attribute which holds the attachment
     */
    public $attribute = 'files';

    public $scenario;

    /**
     * @property array of processors
     */
    public $processors = array();

    /**
     * @property array of styles needs to create
     * example:
     * array(
     *   'small' => '150x75',
     *  'medium' => '!250x70'
     * )
     */
    public $styles = array();

    /**
     * @property string $fallback_image placeholder image src.
     */
    public $fallback_image;

    private $file_extension, $filename;

    /**
     * getter method for the attachment.
     * if you call it like a property ($model->Attachment) it will return the base size.
     * if you have the styles specified you can get them like this:
     * $model->getAttachment('small')
     * @param string $style style to return
     */
    public function getAttachment($style = '')
    {

        $class=get_class($this->owner);
        $class_id=$this->owner->id;
        $f= AttachmentFile::model()->find('classname=:class_name and class_id=:class_id',array(':class_name'=>$class,'class_id'=>$class_id));

        if($style == ''&&$f){
            return $f->filename;
        }







            elseif($this->fallback_image != ''){
                return $this->fallback_image;
            }elseif(!$f){
            return '';
        }



        else{
            if(isset($this->styles[$style])){
                $im = preg_replace('/\.(.*)$/','-'.$style.'\\0',$this->Owner->{$this->attribute});
                if(file_exists($im))
                    return  $im;
                elseif(isset($this->fallback_image))
                    return $this->fallback_image;
            }
        }

    }

    /**
     * check if we have an attachment
     */
    public function hasAttachment()
    {
        $class=get_class($this->owner);
        $class_id=$this->owner->id;
        return AttachmentFile::model()->exists('classname=:class_name and class_id=:class_id',array(':class_name'=>$class,'class_id'=>$class_id));
    }


    /**
     * @param bool $is_obj
     * @return CFile[] || array
     */
    public function listAttachments($is_obj=false)
    {

            $class=get_class($this->owner);
            $class_id=$this->owner->id;
            $result=array();
            foreach(
                AttachmentFile::model()->findAll(
                    'classname=:class_name and class_id=:class_id and category=:category',
                    array(':class_name'=>$class,':class_id'=>$class_id,':category'=>$this->attribute)
                ) as $file_model){
                $file=app()->file->set($file_model->filename);
                if($file->getExists()&&$file->getIsFile()){

                    $result[$file_model->id]=$is_obj?$file:$file->getRealPath();
                }

            }
            return $result;

    }



    /**
     * deletes the attachment
     */
    public function deleteAttachment($id)
    {
        $class=get_class($this->owner);
        $class_id=$this->owner->id;

        return AttachmentFile::model()->deleteByPk($id);
    }


    public function afterDelete($event)
    {
        $class=get_class($this->owner);
        $class_id=$this->owner->id;

        foreach(
            AttachmentFile::model()->findAll(
                'classname=:class_name and class_id=:class_id',
                array(':class_name'=>$class,'class_id'=>$class_id)
            ) as $file_model){
            $file=app()->file->set($file_model->filename);
            if($file->getExists()&&$file->getIsFile()){
                $file->delete();
            }
            $file_model->delete();

        }
        return true;
    }


    public function afterSave($event)
    {
        Yii::import('application.modules.message.models.*');
        Yii::import('ext.attach.*');
        $class=get_class($this->owner);
        $class_id=$event->sender->id;
        $files = CUploadedFile::getInstances($event->sender,$this->attribute);
        if(count($files)==0&&CUploadedFile::getInstance($event->sender,$this->attribute)){
            $files=array(CUploadedFile::getInstance($event->sender,$this->attribute));
        }
        $attr=$this->attribute;
        $model=$event->sender;
        if (isset($files) && count($files) > 0) {

            // go through each uploaded image
            foreach ($files as $file) {
               // echo $file->name.'<br />';
                $id=$model->id;

                $name=$file->name;
                if(!is_dir(Yii::getPathOfAlias('webroot')."/attachments/$class/$id/$attr/"))
                mkdir(Yii::getPathOfAlias('webroot')."/attachments/$class/$id/$attr/",0777,true);
                if ($file->saveAs(Yii::getPathOfAlias('webroot')."/attachments/$class/$class_id/$attr/".$file->name)) {
                    // add it to the main model now
                    $file_add = new AttachmentFile();
                    $file_add->filename = Yii::getPathOfAlias('webroot')."/attachments/$class/$class_id/$attr/".$file->name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
                    $file_add->class_id = $class_id; // this links your picture model to the main model (like your user, or profile model)
                    $file_add->classname=$class;
                    $file_add->category=$this->attribute;

                    $file_add->save(); // DONE
                }
                else{}
                        // handle the errors here, if you want
                }

            // save the rest of your information from the form

        }

        return true;
    }




    public function UnsafeAttribute($name, $value)
    {
        var_dump(true);exit;
        if($name != $this->attribute)
            parent::onUnsafeAttribute($name, $value);
    }
}
