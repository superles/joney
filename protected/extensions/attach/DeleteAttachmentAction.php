<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.10.14
 * Time: 5:51
 * @property Controller $owner
 * @property AttachmentBehavior $file
 */
Yii::import('ext.attach.*');
class DeleteAttachmentAction extends CAction{

    public function run($id){
        $file=AttachmentFile::model()->findByPk($id);
        if(app()->request->isAjaxRequest&&$file&&is_file($file->filename)){
            @unlink($file->filename);
            $file->delete();
            echo json_encode(array('status'=>$id));
        }else{
            echo json_encode(array('status'=>0));
        }

    }

}