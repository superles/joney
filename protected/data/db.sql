CREATE TABLE ActionLog
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    username VARCHAR(255),
    event TINYINT,
    model_name VARCHAR(50) DEFAULT '',
    model_title LONGTEXT,
    datetime DATETIME
);
CREATE TABLE AuthAssignment
(
    itemname VARCHAR(64) NOT NULL,
    userid VARCHAR(64) NOT NULL,
    bizrule LONGTEXT,
    data LONGTEXT,
    PRIMARY KEY (itemname, userid)
);
CREATE TABLE AuthItem
(
    name VARCHAR(64) PRIMARY KEY NOT NULL,
    type INT NOT NULL,
    description LONGTEXT,
    bizrule LONGTEXT,
    data LONGTEXT
);
CREATE TABLE AuthItemChild
(
    parent VARCHAR(64) NOT NULL,
    child VARCHAR(64) NOT NULL,
    PRIMARY KEY (parent, child)
);
CREATE TABLE Comments
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT DEFAULT 0,
    class_name VARCHAR(100) DEFAULT '',
    object_pk INT,
    status TINYINT,
    email VARCHAR(255) DEFAULT '',
    name VARCHAR(50) DEFAULT '',
    text LONGTEXT,
    created DATETIME,
    updated DATETIME,
    ip_address VARCHAR(255) DEFAULT ''
);
CREATE TABLE Discount
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) DEFAULT '',
    active TINYINT,
    sum VARCHAR(10) DEFAULT '',
    start_date DATETIME,
    end_date DATETIME,
    roles VARCHAR(255)
);
CREATE TABLE DiscountCategory
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    discount_id INT,
    category_id INT
);
CREATE TABLE DiscountManufacturer
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    discount_id INT,
    manufacturer_id INT
);
CREATE TABLE Feedback
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT DEFAULT 0,
    class_name VARCHAR(100) DEFAULT '',
    object_pk INT,
    status TINYINT,
    email VARCHAR(255) DEFAULT '',
    name VARCHAR(50) DEFAULT '',
    text LONGTEXT,
    created DATETIME,
    updated DATETIME,
    ip_address VARCHAR(255) DEFAULT '',
    is_negative INT
);
CREATE TABLE Files
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    class_id INT NOT NULL,
    classname VARCHAR(255) NOT NULL,
    filename VARCHAR(255) NOT NULL,
    category VARCHAR(255),
    description LONGTEXT,
    title VARCHAR(255)
);
CREATE TABLE `Like`
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT DEFAULT 0,
    class_name VARCHAR(100) DEFAULT '',
    object_pk INT,
    status TINYINT,
    email VARCHAR(255) DEFAULT '',
    name VARCHAR(50) DEFAULT '',
    text LONGTEXT,
    created DATETIME,
    updated DATETIME,
    ip_address VARCHAR(255) DEFAULT ''
);
CREATE TABLE Map
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    html LONGTEXT,
    category VARCHAR(255),
    lat_lang VARCHAR(255) NOT NULL
);
CREATE TABLE Message
(
    id INT DEFAULT 0 NOT NULL,
    language VARCHAR(16) DEFAULT '' NOT NULL,
    translation LONGTEXT,
    PRIMARY KEY (id, language)
);
CREATE TABLE `Order`
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT,
    secret_key VARCHAR(10) DEFAULT '',
    delivery_id INT,
    delivery_price REAL,
    total_price REAL,
    status_id INT,
    paid TINYINT,
    user_name VARCHAR(100),
    user_email VARCHAR(100),
    user_address VARCHAR(255),
    user_phone VARCHAR(30),
    user_comment VARCHAR(500),
    ip_address VARCHAR(50),
    created DATETIME,
    updated DATETIME,
    discount VARCHAR(255),
    admin_comment LONGTEXT
);
CREATE TABLE OrderHistory
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    order_id INT,
    user_id INT,
    username VARCHAR(255),
    handler VARCHAR(255),
    data_before LONGTEXT,
    data_after LONGTEXT,
    created DATETIME
);
CREATE TABLE OrderProduct
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    order_id INT NOT NULL,
    product_id INT NOT NULL,
    configurable_id INT,
    name LONGTEXT,
    configurable_name LONGTEXT,
    configurable_data LONGTEXT,
    variants LONGTEXT,
    quantity SMALLINT,
    sku VARCHAR(255),
    price REAL
);
CREATE TABLE OrderStatus
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) DEFAULT '',
    position INT DEFAULT 0
);
CREATE TABLE Page
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT,
    category_id INT,
    url VARCHAR(255) DEFAULT '',
    tags LONGTEXT,
    created DATETIME,
    updated DATETIME,
    publish_date DATETIME,
    status VARCHAR(255) DEFAULT '',
    layout VARCHAR(2555) DEFAULT '',
    view VARCHAR(255) DEFAULT '',
    image_id INT,
    gallery_id INT,
    views_count INT DEFAULT 0,
    vote_id INT
);
CREATE TABLE PageCategory
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    parent_id INT,
    url VARCHAR(255) DEFAULT '',
    full_url LONGTEXT,
    layout VARCHAR(255) DEFAULT '',
    view VARCHAR(255) DEFAULT '',
    created DATETIME,
    updated DATETIME,
    page_size SMALLINT,
    gallery_id INT
);
CREATE TABLE PageCategoryTranslate
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    object_id INT NOT NULL,
    language_id INT NOT NULL,
    name VARCHAR(255),
    description LONGTEXT,
    meta_title VARCHAR(255),
    meta_description VARCHAR(255),
    meta_keywords VARCHAR(255)
);
CREATE TABLE PageTag
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    frequency INT DEFAULT 1
);
CREATE TABLE PageTranslate
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    object_id INT NOT NULL,
    language_id INT NOT NULL,
    title VARCHAR(255) DEFAULT '',
    short_description LONGTEXT,
    full_description LONGTEXT,
    meta_title VARCHAR(255),
    meta_keywords VARCHAR(255),
    meta_description VARCHAR(255),
    about LONGTEXT
);
CREATE TABLE Rights
(
    itemname VARCHAR(64) PRIMARY KEY NOT NULL,
    type INT,
    weight INT
);
CREATE TABLE SourceMessage
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    category VARCHAR(32),
    message LONGTEXT
);
CREATE TABLE StoreAttribute
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) DEFAULT '',
    type TINYINT,
    display_on_front TINYINT DEFAULT 1,
    use_in_filter TINYINT,
    use_in_variants TINYINT,
    use_in_compare TINYINT DEFAULT 0,
    select_many TINYINT,
    position INT DEFAULT 0,
    required TINYINT
);
CREATE TABLE StoreAttributeOption
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    attribute_id INT,
    position TINYINT
);
CREATE TABLE StoreAttributeOptionTranslate
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    language_id INT,
    object_id INT,
    value VARCHAR(255) DEFAULT ''
);
CREATE TABLE StoreAttributeTranslate
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    object_id INT,
    language_id INT,
    title VARCHAR(255) DEFAULT ''
);
CREATE TABLE StoreCategory
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    lft INT UNSIGNED,
    rgt INT UNSIGNED,
    level SMALLINT UNSIGNED,
    url VARCHAR(255) DEFAULT '',
    full_path VARCHAR(255) DEFAULT '',
    layout VARCHAR(255) DEFAULT '',
    view VARCHAR(255) DEFAULT '',
    description LONGTEXT
);
CREATE TABLE StoreCategoryTranslate
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    object_id INT,
    language_id INT,
    name VARCHAR(255) DEFAULT '',
    meta_title VARCHAR(255) DEFAULT '',
    meta_keywords VARCHAR(255) DEFAULT '',
    meta_description VARCHAR(255) DEFAULT '',
    description LONGTEXT
);
CREATE TABLE StoreCurrency
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) DEFAULT '',
    iso VARCHAR(10) DEFAULT '',
    symbol VARCHAR(10) DEFAULT '',
    rate REAL,
    main TINYINT,
    `default` TINYINT
);
CREATE TABLE StoreDeliveryMethod
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    price REAL DEFAULT 0.00,
    free_from REAL DEFAULT 0.00,
    position SMALLINT,
    active TINYINT
);
CREATE TABLE StoreDeliveryMethodTranslate
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    object_id INT,
    language_id INT,
    name VARCHAR(255) DEFAULT '',
    description LONGTEXT
);
CREATE TABLE StoreDeliveryPayment
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    delivery_id INT,
    payment_id INT
);
CREATE TABLE StoreManufacturer
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    url VARCHAR(255) DEFAULT '',
    layout VARCHAR(255) DEFAULT '',
    view VARCHAR(255) DEFAULT ''
);
CREATE TABLE StoreManufacturerTranslate
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    object_id INT,
    language_id INT,
    name VARCHAR(255) DEFAULT '',
    description LONGTEXT,
    meta_title VARCHAR(255) DEFAULT '',
    meta_keywords VARCHAR(255) DEFAULT '',
    meta_description VARCHAR(255) DEFAULT ''
);
CREATE TABLE StorePaymentMethod
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    currency_id INT,
    active TINYINT,
    payment_system VARCHAR(100) DEFAULT '',
    position SMALLINT
);
CREATE TABLE StorePaymentMethodTranslate
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    object_id INT,
    language_id INT,
    name VARCHAR(255) DEFAULT '',
    description LONGTEXT
);
CREATE TABLE StoreProduct
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    manufacturer_id INT,
    type_id INT,
    use_configurations TINYINT DEFAULT 0 NOT NULL,
    url VARCHAR(255) NOT NULL,
    price REAL,
    max_price REAL DEFAULT 0.00 NOT NULL,
    is_active TINYINT,
    layout VARCHAR(255),
    view VARCHAR(255),
    sku VARCHAR(255),
    quantity INT DEFAULT 0,
    availability TINYINT DEFAULT 1,
    auto_decrease_quantity TINYINT DEFAULT 1,
    views_count INT,
    created DATETIME,
    updated DATETIME,
    added_to_cart_count INT,
    votes INT,
    rating INT,
    discount VARCHAR(255),
    video LONGTEXT
);
CREATE TABLE StoreProductAttributeEAV
(
    entity INT UNSIGNED NOT NULL,
    attribute VARCHAR(250) DEFAULT '',
    value LONGTEXT
);
CREATE TABLE StoreProductCategoryRef
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    product INT,
    category INT,
    is_main TINYINT
);
CREATE TABLE StoreProductConfigurableAttributes
(
    product_id INT NOT NULL,
    attribute_id INT NOT NULL
);
CREATE TABLE StoreProductConfigurations
(
    product_id INT NOT NULL,
    configurable_id INT NOT NULL
);
CREATE TABLE StoreProductImage
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    product_id INT,
    name VARCHAR(255) DEFAULT '',
    is_main TINYINT DEFAULT 0,
    uploaded_by INT,
    date_uploaded DATETIME,
    title VARCHAR(255)
);
CREATE TABLE StoreProductTranslate
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    object_id INT,
    language_id INT,
    name VARCHAR(255) DEFAULT '',
    short_description LONGTEXT,
    full_description LONGTEXT,
    meta_title VARCHAR(255),
    meta_keywords VARCHAR(255),
    meta_description VARCHAR(255)
);
CREATE TABLE StoreProductType
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) DEFAULT '',
    categories_preset LONGTEXT,
    main_category INT DEFAULT 0
);
CREATE TABLE StoreProductVariant
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    attribute_id INT,
    option_id INT,
    product_id INT,
    price REAL DEFAULT 0.00,
    price_type TINYINT,
    sku VARCHAR(255) DEFAULT ''
);
CREATE TABLE StoreRelatedProduct
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    product_id INT,
    related_id INT
);
CREATE TABLE StoreTypeAttribute
(
    type_id INT NOT NULL,
    attribute_id INT NOT NULL,
    PRIMARY KEY (type_id, attribute_id)
);
CREATE TABLE StoreWishlist
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `key` VARCHAR(10) DEFAULT '',
    user_id INT
);
CREATE TABLE StoreWishlistProducts
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    wishlist_id INT,
    product_id INT,
    user_id INT
);
CREATE TABLE SystemLanguage
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) DEFAULT '',
    code VARCHAR(25) DEFAULT '',
    locale VARCHAR(100) DEFAULT '',
    `default` TINYINT,
    flag_name VARCHAR(255)
);
CREATE TABLE SystemModules
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) DEFAULT '',
    enabled TINYINT
);
CREATE TABLE SystemSettings
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    category VARCHAR(255) DEFAULT '',
    `key` VARCHAR(255) DEFAULT '',
    value LONGTEXT
);
CREATE TABLE accounting1c
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    object_id INT,
    object_type INT,
    external_id VARCHAR(255)
);
CREATE TABLE file
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    extension VARCHAR(255) NOT NULL,
    path VARCHAR(255),
    filename VARCHAR(255) NOT NULL,
    mimeType VARCHAR(255) NOT NULL,
    byteSize INT UNSIGNED NOT NULL,
    createdAt DATETIME NOT NULL,
    hash VARCHAR(255)
);
CREATE TABLE gallery
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    versions_data LONGTEXT NOT NULL,
    name TINYINT DEFAULT 1 NOT NULL,
    description TINYINT DEFAULT 1 NOT NULL
);
CREATE TABLE gallery_photo
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    gallery_id INT NOT NULL,
    rank INT DEFAULT 0 NOT NULL,
    name VARCHAR(512) DEFAULT '' NOT NULL,
    description LONGTEXT,
    file_name VARCHAR(128) DEFAULT '' NOT NULL,
    cat VARCHAR(255),
    master LONGTEXT
);
CREATE TABLE grid_view_filter
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT,
    grid_id VARCHAR(100) DEFAULT '',
    name VARCHAR(100) DEFAULT '',
    data LONGTEXT
);
CREATE TABLE messages
(
    id INT NOT NULL,
    sender_id INT NOT NULL,
    receiver_id INT NOT NULL,
    subject VARCHAR(256) DEFAULT '' NOT NULL,
    body LONGTEXT,
    is_read CHAR(2) DEFAULT '0' NOT NULL,
    deleted_by CHAR(8),
    created_at DATETIME NOT NULL
);
CREATE TABLE notifications
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    product_id INT,
    email VARCHAR(255)
);
CREATE TABLE poll
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    description LONGTEXT,
    status TINYINT DEFAULT 1 NOT NULL
);
CREATE TABLE poll_choice
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    poll_id INT UNSIGNED NOT NULL,
    label VARCHAR(255) DEFAULT '' NOT NULL,
    votes INT UNSIGNED DEFAULT 0 NOT NULL,
    weight INT DEFAULT 0 NOT NULL
);
CREATE TABLE poll_vote
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    choice_id INT UNSIGNED NOT NULL,
    poll_id INT UNSIGNED NOT NULL,
    user_id INT DEFAULT 0 NOT NULL,
    ip_address VARCHAR(16) DEFAULT '' NOT NULL,
    timestamp INT UNSIGNED DEFAULT 0 NOT NULL
);
CREATE TABLE tbl_migration
(
    version VARCHAR(255) PRIMARY KEY NOT NULL,
    apply_time INT
);
CREATE TABLE user
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    username VARCHAR(255) DEFAULT '',
    password VARCHAR(255) DEFAULT '',
    email VARCHAR(255) DEFAULT '',
    created_at DATETIME,
    last_login DATETIME,
    login_ip VARCHAR(255),
    recovery_key VARCHAR(20),
    recovery_password VARCHAR(100),
    discount VARCHAR(255),
    banned TINYINT DEFAULT 0
);
CREATE TABLE user_profile
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT,
    full_name VARCHAR(255) DEFAULT '',
    phone VARCHAR(20),
    delivery_address VARCHAR(255)
);
CREATE INDEX datetime ON ActionLog (datetime);
CREATE INDEX event ON ActionLog (event);
CREATE INDEX model_name ON ActionLog (model_name);
CREATE INDEX username ON ActionLog (username);
CREATE INDEX child ON AuthItemChild (child);
CREATE INDEX class_name_index ON Comments (class_name);
CREATE INDEX user_id ON Comments (user_id);
CREATE INDEX active ON Discount (active);
CREATE INDEX end_date ON Discount (end_date);
CREATE INDEX start_date ON Discount (start_date);
CREATE INDEX category_id ON DiscountCategory (category_id);
CREATE INDEX discount_id ON DiscountCategory (discount_id);
CREATE INDEX discount_id ON DiscountManufacturer (discount_id);
CREATE INDEX manufacturer_id ON DiscountManufacturer (manufacturer_id);
CREATE INDEX class_name_index ON Feedback (class_name);
CREATE INDEX user_id ON Feedback (user_id);
CREATE INDEX class_name_index ON `Like` (class_name);
CREATE INDEX user_id ON `Like` (user_id);
ALTER TABLE Message ADD FOREIGN KEY (id) REFERENCES SourceMessage (id) ON DELETE CASCADE;
CREATE INDEX delivery_id ON `Order` (delivery_id);
CREATE INDEX secret_key ON `Order` (secret_key);
CREATE INDEX status_id ON `Order` (status_id);
CREATE INDEX user_id ON `Order` (user_id);
CREATE INDEX created_index ON OrderHistory (created);
CREATE INDEX order_index ON OrderHistory (order_id);
CREATE INDEX configurable_id ON OrderProduct (configurable_id);
CREATE INDEX order_id ON OrderProduct (order_id);
CREATE INDEX product_id ON OrderProduct (product_id);
CREATE INDEX position ON OrderStatus (position);
CREATE INDEX category_id ON Page (category_id);
CREATE INDEX created ON Page (created);
CREATE INDEX publish_date ON Page (publish_date);
CREATE INDEX status ON Page (status);
CREATE INDEX updated ON Page (updated);
CREATE INDEX url ON Page (url);
CREATE INDEX user_id ON Page (user_id);
CREATE INDEX created ON PageCategory (created);
CREATE INDEX gallery_id_index ON PageCategory (gallery_id);
CREATE INDEX parent_id ON PageCategory (parent_id);
CREATE INDEX updated ON PageCategory (updated);
CREATE INDEX url ON PageCategory (url);
CREATE INDEX language_id ON PageCategoryTranslate (language_id);
CREATE INDEX object_id ON PageCategoryTranslate (object_id);
CREATE INDEX language_id ON PageTranslate (language_id);
CREATE INDEX object_id ON PageTranslate (object_id);
CREATE INDEX display_on_front ON StoreAttribute (display_on_front);
CREATE INDEX name ON StoreAttribute (name);
CREATE INDEX position ON StoreAttribute (position);
CREATE INDEX use_in_compare ON StoreAttribute (use_in_compare);
CREATE INDEX use_in_filter ON StoreAttribute (use_in_filter);
CREATE INDEX use_in_variants ON StoreAttribute (use_in_variants);
CREATE INDEX attribute_id ON StoreAttributeOption (attribute_id);
CREATE INDEX position ON StoreAttributeOption (position);
CREATE INDEX language_id ON StoreAttributeOptionTranslate (language_id);
CREATE INDEX object_id ON StoreAttributeOptionTranslate (object_id);
CREATE INDEX language_id ON StoreAttributeTranslate (language_id);
CREATE INDEX object_id ON StoreAttributeTranslate (object_id);
CREATE INDEX full_path ON StoreCategory (full_path);
CREATE INDEX level ON StoreCategory (level);
CREATE INDEX lft ON StoreCategory (lft);
CREATE INDEX rgt ON StoreCategory (rgt);
CREATE INDEX url ON StoreCategory (url);
CREATE INDEX language_id ON StoreCategoryTranslate (language_id);
CREATE INDEX name ON StoreCategoryTranslate (name);
CREATE INDEX object_id ON StoreCategoryTranslate (object_id);
CREATE INDEX position ON StoreDeliveryMethod (position);
CREATE INDEX language_id ON StoreDeliveryMethodTranslate (language_id);
CREATE INDEX object_id ON StoreDeliveryMethodTranslate (object_id);
CREATE INDEX url ON StoreManufacturer (url);
CREATE INDEX language_id ON StoreManufacturerTranslate (language_id);
CREATE INDEX object_id ON StoreManufacturerTranslate (object_id);
CREATE INDEX currency_id ON StorePaymentMethod (currency_id);
CREATE INDEX language_id ON StorePaymentMethodTranslate (language_id);
CREATE INDEX object_id ON StorePaymentMethodTranslate (object_id);
CREATE INDEX added_to_cart_count ON StoreProduct (added_to_cart_count);
CREATE INDEX created ON StoreProduct (created);
CREATE INDEX is_active ON StoreProduct (is_active);
CREATE INDEX manufacturer_id ON StoreProduct (manufacturer_id);
CREATE INDEX max_price ON StoreProduct (max_price);
CREATE INDEX price ON StoreProduct (price);
CREATE INDEX sku ON StoreProduct (sku);
CREATE INDEX type_id ON StoreProduct (type_id);
CREATE INDEX updated ON StoreProduct (updated);
CREATE INDEX views_count ON StoreProduct (views_count);
CREATE INDEX attribute ON StoreProductAttributeEAV (attribute);
CREATE INDEX ikEntity ON StoreProductAttributeEAV (entity);
CREATE INDEX value ON StoreProductAttributeEAV (value);
CREATE INDEX category ON StoreProductCategoryRef (category);
CREATE INDEX is_main ON StoreProductCategoryRef (is_main);
CREATE INDEX product ON StoreProductCategoryRef (product);
CREATE UNIQUE INDEX product_attribute_index ON StoreProductConfigurableAttributes (product_id, attribute_id);
CREATE UNIQUE INDEX idsunique ON StoreProductConfigurations (product_id, configurable_id);
CREATE INDEX language_id ON StoreProductTranslate (language_id);
CREATE INDEX object_id ON StoreProductTranslate (object_id);
CREATE INDEX attribute_id ON StoreProductVariant (attribute_id);
CREATE INDEX option_id ON StoreProductVariant (option_id);
CREATE INDEX product_id ON StoreProductVariant (product_id);
CREATE INDEX product_id ON StoreRelatedProduct (product_id);
CREATE INDEX `key` ON StoreWishlist (`key`);
CREATE INDEX user_id ON StoreWishlist (user_id);
CREATE INDEX product_id ON StoreWishlistProducts (product_id);
CREATE INDEX user_id ON StoreWishlistProducts (user_id);
CREATE INDEX wishlist_id ON StoreWishlistProducts (wishlist_id);
CREATE INDEX code ON SystemLanguage (code);
CREATE UNIQUE INDEX name ON SystemModules (name);
CREATE INDEX category ON SystemSettings (category);
CREATE INDEX `key` ON SystemSettings (`key`);
CREATE INDEX external_id ON accounting1c (external_id);
CREATE INDEX object_type ON accounting1c (object_type);
ALTER TABLE gallery_photo ADD FOREIGN KEY (gallery_id) REFERENCES gallery (id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX fk_gallery_photo_gallery1 ON gallery_photo (gallery_id);
CREATE INDEX product_id ON notifications (product_id);
ALTER TABLE poll_choice ADD FOREIGN KEY (poll_id) REFERENCES poll (id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX choice_poll ON poll_choice (poll_id);
ALTER TABLE poll_vote ADD FOREIGN KEY (choice_id) REFERENCES poll_choice (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE poll_vote ADD FOREIGN KEY (poll_id) REFERENCES poll (id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX vote_choice ON poll_vote (choice_id);
CREATE INDEX vote_poll ON poll_vote (poll_id);
CREATE INDEX vote_user ON poll_vote (user_id);
CREATE INDEX user_id ON user_profile (user_id);
INSERT INTO cms.user (id, username, password, email, created_at, last_login, login_ip, recovery_key, recovery_password, discount, banned) VALUES (1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'superles@mail.ru', '2014-07-30 01:07:10', '2015-07-09 05:00:01', '192.168.1.254', null, null, 'admin', 0);
INSERT INTO cms.user_profile (id, user_id, full_name, phone, delivery_address) VALUES (1, 1, 'fullname1423262164', '', '');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Admin', 2, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Authenticated', 2, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Guest', 2, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Orders.*', 1, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Statuses.*', 1, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Orders.Index', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Orders.Create', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Orders.Update', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Orders.AddProductList', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Orders.AddProduct', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Orders.RenderOrderedProducts', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Orders.JsonOrderedProducts', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Orders.Delete', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Orders.DeleteProduct', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Statuses.Index', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Statuses.Create', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Statuses.Update', 0, null, null, 'N;');
INSERT INTO cms.AuthItem (name, type, description, bizrule, data) VALUES ('Orders.Statuses.Delete', 0, null, null, 'N;');
INSERT INTO cms.AuthAssignment (itemname, userid, bizrule, data) VALUES ('Admin', '1', null, null);
