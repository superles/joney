<?php

/**
 * Model to handle feedback form
 */
class FeedbackForm extends CFormModel
{

	/**
	 * @var string
	 */
	public $firstname;

	/**
	 * @var string
	 */
	public $lastname;

	/**
	 * @var string
	 */
	public $phone;

/**
	 * @var string
	 */
	public $email;

	/**
	 * @var string
	 */
	public $message;

	/**
	 * @var string
	 */
	public $code;

	/**
	 * @var string
	 */
	public $type;

	/**
	 * @var string
	 */
	public $file;

	/**
	 * Initialize model
	 */
	public function init()
	{
		$user=Yii::app()->user;
		if($user->isGuest===false)
		{
			$this->firstname=$user->username;
			$this->email=$user->email;
		}
	}

	/**
	 * Validation rules
	 * @return array
	 */
	public function rules()
	{
		return array(

			array('firstname, email ,phone, message', 'required','on'=>'feedback'),

			array('file','file','types'=>'doc,docx,jpg,jpeg,png,pdf,exls,xlsx,txt','on'=>'resume'),

			array('file', 'safe'),

			array('type','safe'),
			array('email', 'required','on'=>'subscribe'),
			array('email', 'required','on'=>'resume'),
			array('email', 'email'),
			array('message', 'length', 'max'=>Yii::app()->settings->get('feedback', 'max_message_length')),
			array('code','captcha','allowEmpty'=>!Yii::app()->settings->get('feedback', 'enable_captcha')),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'firstname'=>Yii::t('FeedbackModule.core', 'Имя'),
			'lastname'=>Yii::t('FeedbackModule.core', 'Фамилия'),
			'phone'=>Yii::t('FeedbackModule.core', 'Телефон'),
			'email'=>Yii::t('FeedbackModule.core', 'Email'),
			'file'=>Yii::t('FeedbackModule.core', 'Файл'),
			'message'=>Yii::t('FeedbackModule.core', 'Сообщение'),
		);
	}

	/**
	 * Send email
	 */
	public function sendMessage()
	{
		$mailer           = Yii::app()->mail;
		$mailer->From     = 'noreply@'.Yii::app()->request->serverName;
		$mailer->FromName = Yii::t('FeedbackModule.core', 'Сообщение с сайта. Форма обратной связи');
		$mailer->Subject  = Yii::t('FeedbackModule.core', 'Сообщение от {firstname}, тел: {phone}',
			array(

				'{firstname}'=>CHtml::encode($this->firstname),
				'{phone}'=>CHtml::encode($this->phone)
			));
		$mailer->Body     = CHtml::encode($this->message);
		$mailer->AddAddress(Yii::app()->settings->get('feedback', 'admin_email'));
		$mailer->AddReplyTo($this->email);
		$mailer->Send();

		Yii::app()->user->setFlash('messages', Yii::t('FeedbackModule', 'Спасибо. Ваше сообщение отправлено.'));
	}

	/**
	 * Send email
	 */
	public function sendSubscribeMessage()
	{
		$mailer           = Yii::app()->mail;
		$mailer->From     = 'noreply@'.Yii::app()->request->serverName;
		$mailer->FromName = Yii::t('FeedbackModule.core', 'Сообщение с сайта. Форма подписки');
		$mailer->Subject  = Yii::t('FeedbackModule.core', 'Сообщение от {email}',
			array(
				'{email}'=>CHtml::encode($this->email)
			));
		$mailer->Body     = CHtml::encode($this->email);
		$mailer->AddAddress(Yii::app()->settings->get('feedback', 'admin_email'));
		$mailer->AddReplyTo($this->email);
		$mailer->Send();


	}


	/**
	 * Send email
	 */
	public function sendResumeMessage()
	{
		$mailer           = app()->mail;
		$mailer->From     = 'noreply@'.Yii::app()->request->serverName;
		$mailer->FromName = Yii::t('FeedbackModule.core', 'Сообщение с сайта. Резюме с сайта');
		$mailer->Subject  = Yii::t('FeedbackModule.core', 'Резюме от {email}',
			array(
				'{email}'=>CHtml::encode($this->email)
			));
		$mailer->Body     = CHtml::encode($mailer->Subject);
		$mailer->AddAddress(Yii::app()->settings->get('feedback', 'admin_email'));
		$mailer->AddReplyTo($this->email);
		$file=CUploadedFile::getInstance($this,'file');

		if($file){
			$newPath=Yii::getPathOfAlias('application.runtime')."/".$file->name."_".time().".".$file->extensionName;
			$file->saveAs($newPath);
			$mailer->AddAttachment($newPath);
		}
		$mailer->Send();


	}

	public function beforeValidate(){
		if(!empty($this->type)){
			$this->setScenario($this->type);
		}
		return parent::beforeValidate();

	}

}
