<?php

Yii::import('ext.elrte.SElrteArea');
Yii::import('ext.ckeditor.CKEditorWidget');
Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget');

/**
 * Draw textarea widget
 */
class ImperaviRedactorArea extends ImperaviRedactorWidget
{

	public function setModel($model)
	{
		$this->model=$model;
	}
}

