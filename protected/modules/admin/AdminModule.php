<?php

class AdminModule extends BaseModule
{
	// Set module name for assets publishing
	public $moduleName = 'admin';
	public function init()
	{
		$this->setImport(array(
			'application.modules.admin.components.*',
		));
	}

}
