<?php

/**
 * Admin site feedbacks
 */
class FeedbacksController extends SAdminController
{

	/**
	 * Display all site feedbacks
	 */
	public function actionIndex()
	{
		$model = new Feedback('search');

		if(!empty($_GET['Feedback']))
			$model->attributes = $_GET['Feedback'];

		$dataProvider = $model->search();
		$dataProvider->pagination->pageSize = Yii::app()->settings->get('core', 'productsPerPageAdmin');

		$this->render('index', array(
			'model'=>$model,
			'dataProvider'=>$dataProvider
		));
	}

	/**
	 * Update feedback
	 * @param $id
	 * @throws CHttpException
	 */
	public function actionUpdate($id)
	{
		$model = Feedback::model()->findByPk($id);

		if(!$model)
			throw new CHttpException(404, Yii::t('FeedbacksModule.admin', 'Отзыв не найден'));

		$form = new CForm('feedbacks.views.admin.feedbacks.feedbackForm', $model);

		if (Yii::app()->request->isPostRequest)
		{
			$model->attributes = $_POST['Feedback'];
			if($model->validate())
			{
				$model->save();

				$this->setFlashMessage(Yii::t('FeedbacksModule.admin', 'Изменения успешно сохранены'));

				if (isset($_POST['REDIRECT']))
					$this->smartRedirect($model);
				else
					$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'model' => $model,
			'form'  => $form
		));
	}

	public function actionUpdateStatus()
	{
		$ids    = Yii::app()->request->getPost('ids');
		$status = Yii::app()->request->getPost('status');
		$models = Feedback::model()->findAllByPk($ids);

		if(!array_key_exists($status, Feedback::getStatuses()))
			throw new CHttpException(404, Yii::t('FeedbacksModule.admin', 'Ошибка проверки статуса.'));

		if(!empty($models))
		{
			foreach ($models as $feedback)
			{
				$feedback->status = $status;
				$feedback->save();
			}
		}

		echo Yii::t('FeedbacksModule', 'Статус успешно изменен');
	}

	/**
	 * Delete feedbacks
	 * @param array $id
	 */
	public function actionDelete($id = array())
	{
		if (Yii::app()->request->isPostRequest)
		{
			$model = Feedback::model()->findAllByPk($_REQUEST['id']);

			if (!empty($model))
			{
				foreach($model as $m)
					$m->delete();
			}

			if (!Yii::app()->request->isAjaxRequest)
				$this->redirect('index');
		}
	}

}
