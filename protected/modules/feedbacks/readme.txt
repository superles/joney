Usage:

1. Connect behavior to AR model
    'feedbacks' => array(
        'class'       => 'feedbacks.components.FeedbackBehavior',
        'class_name'  => 'store.models.StoreProduct', // Alias to feedbackable model
        'owner_title' => 'name', // Attribute name to present feedback owner in admin panel
    )
2. Update view where to enable feedbacks
    ...
    $this->renderPartial('feedbacks.views.feedback.create', array(
        'model'=>$model, // Feedbackable model instance
    ));
    ...
3. Add captcha action
    ...
    public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
            ),
        );
    }
    ...