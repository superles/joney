<?php

/**
 * Behavior for feedbackabe models
 */
class FeedbackBehavior extends CActiveRecordBehavior
{

	/**
	 * @var string model primary key attribute
	 */
	public $pk = 'id';

	/**
	 * @var string alias to class. e.g: application.store.models.StoreProduct or pages.models.Page
	 */
	public $class_name;

	/**
	 * @var string attribute name to present feedback owner in admin panel. e.g: name - references to Page->name
	 */
	public $owner_title;

	/**
	 * @return string pk name
	 */
	public function getObjectPkAttribute()
	{
		return $this->pk;
	}

	public function getClassName()
	{
		return $this->class_name;
	}

	public function getOwnerTitle()
	{
		$attr = $this->owner_title;
		return $this->getOwner()->$attr;
	}

	public function attach($owner)
	{
		parent::attach($owner);
	}

	/**
	 * @param CEvent $event
	 * @return mixed
	 */
	public function afterDelete($event)
	{
		Yii::import('application.modules.feedbacks.models.Feedback');

		$pk = $this->getObjectPkAttribute();
		Feedback::model()->deleteAllByAttributes(array(
				'class_name'=>$this->getClassName(),
				'object_pk'=>$this->getOwner()->$pk
		));
		return parent::afterDelete($event);
	}

	/**
	 * @return string approved feedbacks count for object
	 */
	public function getFeedbacksCount()
	{
		Yii::import('application.modules.feedbacks.models.Feedback');

		$pk = $this->getObjectPkAttribute();
		return Feedback::model()
			->approved()
			->countByAttributes(array(
			'class_name'=>$this->getClassName(),
			'object_pk'=>$this->getOwner()->$pk
		));
	}
    public function getMyFeedbacksCount()
    {
        Yii::import('application.modules.feedbacks.models.Feedback');

        $pk = $this->getObjectPkAttribute();
        return Feedback::model()
            ->approved()
            ->countByAttributes(array(
                'user_id'=>user()->id,
                'class_name'=>$this->getClassName(),
                'object_pk'=>$this->getOwner()->$pk
            ));
    }
    public function getFeedbacks()
	{
		Yii::import('application.modules.feedbacks.models.Feedback');

		$pk = $this->getObjectPkAttribute();
		return Feedback::model()
			->approved()
			->findAllByAttributes(array(

			'class_name'=>$this->getClassName(),
			'object_pk'=>$this->getOwner()->$pk
		));
	}
    public function getMyFeedbacks()
	{
		Yii::import('application.modules.feedbacks.models.Feedback');

		$pk = $this->getObjectPkAttribute();
		return Feedback::model()
			->approved()
			->findAllByAttributes(array(
             'user_id'=>user()->id,
			'class_name'=>$this->getClassName(),
			'object_pk'=>$this->getOwner()->$pk
		));
	}
}
