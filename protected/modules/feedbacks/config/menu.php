<?php

/**
 * @var $this SSystemMenu
 */

Yii::import('feedbacks.FeedbacksModule');

/**
 * Admin menu items for pages module
 */
return array(
    'social'=>array(
        'items'=>array(
            array(
                'label'    => Yii::t('FeedbacksModule.core', 'Отзывы'),
                'url'      => array('/feedbacks/admin/index'),
                'position' => 60,
                'itemOptions' => array(
                    'class'       => 'hasRedCircle circle-feedbacks',
                ),
            ),

        ),
    ),
);

