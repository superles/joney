<?php

/**
 * Routes for feedbacks module
 */
return array(
	'/admin/feedbacks'=>'/feedbacks/admin/feedbacks',
	'/admin/feedbacks/<action>'=>'/feedbacks/admin/feedbacks/<action>',
);