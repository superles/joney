<?php

Yii::import('application.modules.feedbacks.FeedbacksModule');

/**
 * Feedbacks module info
 */ 
return array(
	'name'=>Yii::t('FeedbacksModule.core', 'Отзывы'),
	'author'=>'firstrow@gmail.com',
	'version'=>'0.1',
	'description'=>Yii::t('FeedbacksModule.core', 'Позволяет оставлять отзывы к продуктам, страницам.'),
	'url'=>''
);