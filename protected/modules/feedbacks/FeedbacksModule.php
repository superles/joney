<?php

class FeedbacksModule extends BaseModule
{
	/**
	 * @var string
	 */
	public $moduleName = 'feedbacks';

	/**
	 * Init module
	 */
	public function init()
	{
		$this->setImport(array(
			'feedbacks.models.Feedback',
		));
	}

	/**
	 * @param $model
	 * @return Feedback
	 */
	public function processRequest($model)
	{
		$feedback = new Feedback;
		if(Yii::app()->request->isPostRequest)
		{
			$feedback->attributes = Yii::app()->request->getPost('Feedback');

			if(!Yii::app()->user->isGuest)
			{
				$feedback->name = Yii::app()->user->name;
				$feedback->email = Yii::app()->user->email;
			}

			if($feedback->validate())
			{
				$pkAttr = $model->getObjectPkAttribute();
				$feedback->class_name = $model->getClassName();
				$feedback->object_pk = $model->$pkAttr;
				$feedback->user_id = Yii::app()->user->isGuest ? 0 : Yii::app()->user->id;


				$feedback->save();

				$url = Yii::app()->getRequest()->getUrl();

                if($feedback->status==Feedback::STATUS_WAITING)
                {

					$url.='#';
					Yii::app()->user->setFlash('messages', Yii::t('FeedbacksModule.core', 'Ваш комментарий успешно добавлен. Он будет опубликован после проверки администратором.'));
				}elseif($feedback->status==Feedback::STATUS_APPROVED){
                    $url.='#feedback_'.$feedback->id;
                }

				// Refresh page
				Yii::app()->request->redirect($url, true);
			}
		}
		return $feedback;
	}

}