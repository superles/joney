<?php

/**
 * Display feedbacks list
 *
 * @var $model Feedback
 **/

Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/admin/feedbacks.index.js');

$this->pageHeader = Yii::t('FeedbacksModule.core', 'Отзывы');

$this->breadcrumbs = array(
    'Home'=>$this->createUrl('/admin'),
    Yii::t('FeedbacksModule.core', 'Отзывы'),
);

$this->widget('ext.sgridview.SGridView', array(
    'dataProvider' => $dataProvider,
    'id'           => 'feedbacksListGrid',
    'filter'       => $model,
    'customActions'=>array(
        array(
            'label'=>Yii::t('FeedbacksModule.core', 'Подтвержден негативный'),
            'url'=>'#',
            'linkOptions'=>array(
                'onClick'=>'return setFeedbacksStatus(3, this);',
            )
        ),
        array(
            'label'=>Yii::t('FeedbacksModule.core', 'Подтвержден позитивный'),
            'url'=>'#',
            'linkOptions'=>array(
                'onClick'=>'return setFeedbacksStatus(4, this);',
            )
        ),
        array(
            'label'=>Yii::t('FeedbacksModule.core', 'Ждет негативный'),
            'url'=>'#',
            'linkOptions'=>array(
                'onClick'=>'return setFeedbacksStatus(1, this);',
            )
        ),
        array(
            'label'=>Yii::t('FeedbacksModule.core', 'Ждет позитивный'),
            'url'=>'#',
            'linkOptions'=>array(
                'onClick'=>'return setFeedbacksStatus(2, this);',
            )
        ),
        array(
            'label'=>Yii::t('FeedbacksModule.core', 'Спам'),
            'url'=>'#',
            'linkOptions'=>array(
                'onClick'=>'return setFeedbacksStatus(5, this);',
            )
        ),
    ),
    'columns' => array(
        array(
            'class'=>'CCheckBoxColumn',
        ),
        array(
            'class'=>'SGridIdColumn',
            'name'=>'id',
        ),
        array(
            'name'  => 'name',
            'type'  => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->name), array("update", "id"=>$data->id))',
        ),
        array(
            'name'=>'email',
        ),
        array(
            'name'=>'text',
            'value'=>'Feedback::truncate($data, 100)'
        ),
        array(
            'name'=>'status',
            'filter'=>Feedback::getStatuses(),
            'value'=>'$data->statusTitle',
        ),
        array(
            'name'=>'owner_title',
            'filter'=>false
        ),
        'ip_address',
        array(
            'name'=>'created',
        ),
        // Buttons
        array(
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
));