<?php

/**
 * Create/update feedback
 *
 * @var $model Feedback
 */

$this->topButtons = $this->widget('admin.widgets.SAdminTopButtons', array(
	'form'         => $form,
	'deleteAction' => $this->createUrl('/feedbacks/admin/feedbacks/delete', array('id'=>$model->id)),
	'template'     => array('history_back','save','delete')
));

$title = Yii::t('FeedbacksModule.admin', 'Редактирование комментария');

$this->breadcrumbs = array(
	'Home'=>$this->createUrl('/admin'),
	Yii::t('FeedbacksModule.admin', 'Отзывы')=>$this->createUrl('index'),
	CHtml::encode($model->email),
);

$this->pageHeader = $title;

?>

<div class="form wide padding-all">
	<?php echo $form; ?>
</div>