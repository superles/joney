<?php
/**
 * @var $this Controller
 * @var $form CActiveForm
 */

// Load module
$module = Yii::app()->getModule('feedbacks');
// Validate and save feedback on post request
$feedback = $module->processRequest($model);
// Load model feedbacks
$feedbacks = Feedback::getObjectFeedbacks($model);

$currentUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

// Display feedbacks
if(!empty($feedbacks))
{
	foreach($feedbacks as $row)
	{
	?>
		<div class="feedback" id="feedback_<?php echo $row->id; ?>">
			<span class="username"><?php echo CHtml::encode($row->name); ?></span> <span class="created">(<?php echo $row->created; ?>)</span>
			<?php echo CHtml::link('#', Yii::app()->request->getUrl().'#feedback_'.$row->id) ?>
			<div class="message">
				<?php echo nl2br(CHtml::encode($row->text)); ?>
			</div>
			<hr>
		</div>
	<?php
	}
}
?>

<div class="leave_feedback" id="leave_feedback">
	<h3><?php echo Yii::t('FeedbacksModule.core', 'Оставить отзыв') ?></h3>
	<div class="form wide ">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'                     =>'feedback-create-form',
		'action'                 =>$currentUrl.'#feedback-create-form',
		'enableAjaxValidation'   =>false,
		'enableClientValidation' =>true,
	)); ?>

	<?php if(Yii::app()->user->isGuest): ?>
		<div class="row">
			<?php echo $form->labelEx($feedback,'name'); ?>
			<?php echo $form->textField($feedback,'name'); ?>
			<?php echo $form->error($feedback,'name'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($feedback,'email'); ?>
			<?php echo $form->textField($feedback,'email'); ?>
			<?php echo $form->error($feedback,'email'); ?>
		</div>
	<?php endif; ?>

		<div class="row">
			<?php echo $form->labelEx($feedback,'text'); ?>
			<?php echo $form->textArea($feedback,'text', array('rows'=>5)); ?>
			<?php echo $form->error($feedback,'text'); ?>
		</div>

		<?php if(Yii::app()->user->isGuest): ?>
		<div class="row">
			<?php echo CHtml::activeLabelEx($feedback, 'verifyCode')?>
			<?php $this->widget('CCaptcha', array(
				'clickableImage'=>true,
				'showRefreshButton'=>false,
			)) ?>
			<br/>
			<label>&nbsp;</label>
			<?php echo CHtml::activeTextField($feedback, 'verifyCode')?>
			<?php echo $form->error($feedback,'verifyCode'); ?>
		</div>
		<?php endif ?>

		<div class="row buttons">
			<?php echo CHtml::submitButton(Yii::t('FeedbacksModule.core', 'Отправить')); ?>
		</div>

	<?php $this->endWidget(); ?><!-- /form -->
	</div>
</div>