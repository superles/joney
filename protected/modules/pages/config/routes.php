<?php

return array(
	'pages/captcha'=>'pages/pages/captcha',
	'page/search/'  => array('pages/pages/search',array('search'=>'')),
	'page/<url>'  => 'pages/pages/view',

	'page/search/<search>'  => 'pages/pages/search',
	'pages/<url>*'     => 'pages/pages/list',
	'pages/root/<url>*'     => 'pages/pages/list',

);