<?php

Yii::import('application.modules.pages.PagesModule');

/**
 * Admin menu items for pages module
 */
return array(
	'cms'=>array(
		'position'=>5,
		'items'=>array(
			array(
				'label'=>Yii::t('PagesModule.core', 'Страницы'),
				'url'=>array('/admin/pages'),
				'position'=>3
			),
			array(
				'label'=>Yii::t('PagesModule.core', 'Категории'),
				'url'=>array('/admin/pages/category'),
				'position'=>4
			),
			array(
				'label'=>Yii::t('PagesModule.admin', 'Типы материалов'),
				'url'=>Yii::app()->createUrl('pages/admin/pageType'),
				'position'=>6
			),
			array(
				'label'=>Yii::t('PagesModule.admin', 'Атрибуты'),
				'url'=>Yii::app()->createUrl('pages/admin/attribute'),
				'position'=>7
			),
		),
	),
);