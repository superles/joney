<?php

/**
 * Pages controller
 * @package modules.pages
 */
class PagesController extends Controller
{



    /**
     * @return array
     */
    public function actions()
    {
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
            ),
            'downloadAttachment'=>'ext.attach.DownloadAttachmentAction',
        );
    }
    public function filters()
    {
        return !YII_DEBUG?array(
            array(
                'CHttpCacheFilter + view',
                'lastModified'=>Yii::app()->db->createCommand("SELECT MAX(`updated`) FROM {{Page}}")->queryScalar(),
            ),
        ):array();
    }

    public function actionListbytag($tag)
    {
        $this->layout='//layouts/main_site';
        $url = substr(Yii::app()->request->getPathInfo(), 6);



        $criteria = Page::model()
            ->published()

            ->getDbCriteria();

        if(isset($_GET['tag']))
            $criteria->addSearchCondition('tags',$_GET['tag']);

        $count = Page::model()->count($criteria);

        $pagination = new CPagination($count);

        $pagination->applyLimit($criteria);





        $pages = Page::model()->findAll($criteria);

        $view = $this->setDesign(new PageCategory(), 'list_bytags');

        $this->render($view, array(
            'model'=>new PageCategory(),
            'pages'=>$pages,
         //   'popular' => $this->getPopular(4),
            'pagination'=>$pagination
        ));


    }
    public function actionSearch($search=false)
    {

        $url = substr(Yii::app()->request->getPathInfo(), 6);

        if(!$search){
            $this->redirect('/pages/routes');
        }

        $criteria = Page::model()
            ->published()

            ->getDbCriteria();

        $criteria->with=array('translate');
        $criteria->addCondition('translate.title LIKE "%'.$search.'%" OR translate.short_description LIKE "%'.$search.'%" OR translate.short_description LIKE "%'.$search.'%" ');

        $count = Page::model()->count($criteria);

        $pagination = new CPagination($count);

        $pagination->applyLimit($criteria);





        $pages = Page::model()->findAll($criteria);



        $view = $this->setDesign(PageCategory::model()->withUrl('routes'), 'routes');

        $this->render($view, array(
            'model'=>new PageCategory(),
            'pages'=>$pages,
         //   'popular' => $this->getPopular(4),
            'pagination'=>$pagination
        ));


    }
	/**
	 * Filter pages by category
	 */
	public function actionList()
	{
		// Remove "pages/" from beginning

		$url = request()->getParam('url',substr(Yii::app()->request->getPathInfo(), 6));

		$model = PageCategory::model()
			->withFullUrl($url)
			->find();

        if(!$model){
            $model = PageCategory::model()
                ->withUrl($url)
                ->find();
        }

		if (!$model) throw new CHttpException(404, Yii::t('PagesModule.core', 'Категория не найдена.'));

		$criteria = Page::model()
			->published()
			->filterByCategory($model)
			->getDbCriteria();

        if(isset($_GET['tag']))
            $criteria->addSearchCondition('tags',$_GET['tag']);

        $ids=array();

//        $category=PageCategory::model()->findAllByAttributes(array('parent_id'=>$model->id));
//        if($category&&count($category)>0){
//
//
//            foreach($category as $cat){
//                $ids[]=intval($cat->id);
//            }
//        }
//        if(count($ids)>0){
//            array_push($ids,$model->id);
//            $criteria->addInCondition('category_id',$ids);
//        }else{
//            $criteria->compare('category_id',$model->id);
//        }

		$count = Page::model()->count($criteria);

		$pagination = new CPagination($count);
		$pagination->pageSize = ($model->page_size > 0) ? $model->page_size: $model->defaultPageSize;
		$pagination->applyLimit($criteria);

		$pages = Page::model()->findAll($criteria);

		$view = $this->setDesign($model, 'list');

		$this->render($view, array(
			'model'=>$model,
			'pages'=>$pages,
			'pagination'=>$pagination
		));
	}

	/**
	 * Display page by url.
	 * Example url: /page/some-page-url
	 * @param string $url page url
	 */
	public function actionView($url)
	{
		$model = Page::model()
			->published()
			->withUrl($url)
			->find(array(
				'limit'=>1
			));

		if (!$model){
            if(user()->isAdmin()){
                $this->redirect(array('/admin/pages/default/create','url'=>$url));
            }else{
                throw new CHttpException(404, Yii::t('PagesModule.core', 'Страница не найдена.'));
            }

        }

		$view = $this->setDesign($model, 'view');

        $this->model=$model;
		$this->render($view, array(
			'model'=>$model,
		));
	}

    /**
     * @param $limit
     * @return array
     */
    protected function getPopular($limit)
    {
        return StoreProduct::model()
            ->active()
            ->byViews()
            ->findAll(array('limit'=>$limit));
	}

}