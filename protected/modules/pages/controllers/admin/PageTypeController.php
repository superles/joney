<?php

/**
 * Admin product types
 */
class PageTypeController extends SAdminController {

	/**
	 * Display types list
	 */
	public function actionIndex()
	{
		$model = new PageType('search');

		if (!empty($_GET['PageType']))
			$model->attributes = $_GET['PageType'];

		$dataProvider = $model->orderByName()->search();
		$dataProvider->pagination->pageSize = Yii::app()->settings->get('core', 'productsPerPageAdmin');

		$this->render('index', array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Create new product type
	 */
	public function actionCreate()
	{
		$this->actionUpdate(true);
	}
	/**
	 * Update product type
	 * @param bool $new
	 * @throws CHttpException
	 */
	public function actionUpdate($new = false)
	{
		if ($new === true)
			$model = new PageType;
		else
			$model = PageType::model()->findByPk($_GET['id']);

		if (!$model)
			throw new CHttpException(404, Yii::t('PagesModule.admin', 'Тип продукта не найден.'));

		if (Yii::app()->request->isPostRequest)
		{
			$model->attributes = $_POST['PageType'];

			if(isset($_POST['categories']) && !empty($_POST['categories']))
			{
				$model->categories_preset = serialize($_POST['categories']);
				$model->main_category = $_POST['main_category'];
			}
			else
			{
				//return defaults when all checkboxes were checked off
				$model->categories_preset = null;
				$model->main_category = 0;
			}

			if ($model->validate())
			{
				$model->save();
				// Set type attributes
				$model->useAttributes(Yii::app()->request->getPost('attributes', array()));

				$this->setFlashMessage(Yii::t('PagesModule.admin', 'Изменения успешно сохранены'));

				if (isset($_POST['REDIRECT']))
					$this->smartRedirect($model);
				else
					$this->redirect('create');
			}
		}

		// Select available(not used) attributes
		$cr = new CDbCriteria;
		$cr->addNotInCondition('PageAttribute.id', CHtml::listData($model->attributeRelation, 'attribute_id','attribute_id'));
		$allAttributes = PageAttribute::model()->findAll($cr);

		$this->render('update', array(
			'model'=>$model,
			'attributes'=>$allAttributes,
		));
	}

	/**
	 * Delete type
	 * @param array $id
	 */
	public function actionDelete($id = array())
	{
		if (Yii::app()->request->isPostRequest)
		{
			$model = PageType::model()->findAllByPk($_REQUEST['id']);

			if (!empty($model))
			{
				foreach($model as $m)
				{
					if($m->productsCount > 0)
						throw new CHttpException(404, Yii::t('PagesModule.admin', 'Ошибка удаления типа продукта. Он используется в продуктах.'));
					else
						$m->delete();
				}
			}

			if (!Yii::app()->request->isAjaxRequest)
				$this->redirect('index');
		}
	}

}
