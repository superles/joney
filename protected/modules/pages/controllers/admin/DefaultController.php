<?php

class DefaultController extends SAdminController {

	public function actions()
	{
		return array(
			'downloadAttachment'=>'ext.attach.DownloadAttachmentAction',
			'deleteAttachment'=>'ext.attach.DeleteAttachmentAction',
			'editAttachment'=>'ext.attach.EditAttachmentAction',
			'fileUpload'=>'ext.imperavi-redactor-widget.actions.FileUpload',
			'imageList'=>'ext.imperavi-redactor-widget.actions.ImageList',
			'imageUpload'=>'ext.imperavi-redactor-widget.actions.ImageUpload'
		);
	}
	/**
	 * Suggests tags based on the current user input.
	 * This is called via AJAX when the user is entering the tags input.
	 */
	public function actionSuggestTags()
	{
		if(isset($_GET['q']) && ($keyword=trim($_GET['q']))!=='')
		{
			$tags=PageTag::model()->suggestTags($keyword);
			if($tags!==array())
				echo implode("\n",$tags);
		}
	}

	/**
	 * Display pages list.
	 */
	public function actionIndex()
	{
		$model = new Page('search');

		if (!empty($_GET['Page']))
			$model->attributes = $_GET['Page'];
        $criteria=$model->getDbCriteria();
        if(isset($_GET['tag']))
            $criteria->addSearchCondition('tags',$_GET['tag']);
        $model->setDbCriteria($criteria);
		$dataProvider = $model->search();
		$dataProvider->pagination->pageSize = Yii::app()->settings->get('core', 'productsPerPageAdmin');

		$this->render('index', array(
			'model'=>$model,
			'dataProvider'=>$dataProvider
		));
	}

	public function actionCreate()
	{
		$this->actionUpdate(true);
	}

	/**
	 * Create or update new page
	 * @param boolean $new
	 */
	/**
	 * Load attributes relative to type and available for product configurations.
	 * Used on creating new product.
	 */
	public function actionLoadConfigurableOptions()
	{
		// For configurations that  are available only dropdown and radio lists.
		$cr = new CDbCriteria;
		$cr->addInCondition('PageAttribute.type', array(PageAttribute::TYPE_DROPDOWN, PageAttribute::TYPE_RADIO_LIST));
		$type = PageType::model()->with(array('pageAttributes'))->findByPk($_GET['type_id'], $cr);

		$data = array();
		foreach($type->pageAttributes as $attr)
		{
			$data[] = array(
				'id'=>$attr->id,
				'title'=>$attr->title,
			);
		}

		echo json_encode($data);
	}
	public function actionUpdate($new = false)
	{
		if ($new === true)
		{
			$model = new Page;
			$model->publish_date = date('Y-m-d H:i:s');
			if(user()->isAdmin()&&$url=request()->getParam('url')){
				$model->url=$url;
			}
			$gallery=new Gallery;
			$gallery->name='name';
			$gallery->description='description';
			$gallery->versions=array(
				'small' => array(
					   'resize' => array(200, null),
				),
			  'medium' => array(
					   'resize' => array(800, null),
			   )
		    );
			$gallery->save();
			$model->galleryBehavior->setGallery($gallery);
			$model->gallery_id=$gallery->id;

		}
		else
		{
			$model = Page::model()->language($_GET)->findByPk($_GET['id']);


			if(request()->getParam('partial',false)){
				$model->setAttribute('full_description',$_POST['Page']['full_description']);
				if($model->save()){
					$this->redirect(request()->urlReferrer);
				}
			}
		}

		if (!$model)
			throw new CHttpException(404, Yii::t('PagesModule.core', 'Страница не найдена.'));

		if(!$model->type){
			$model->type=PageType::model()->find();
			$model->type_id=$model->type->id;
		}

		$form = new STabbedForm('application.modules.pages.views.admin.default.pageForm', $model);

		$form->additionalTabs = array(
			Yii::t('PagesModule.admin','Дополнительные категории') => $this->renderPartial('_tree', array('model'=>$model), true),
			Yii::t('PagesModule.admin','Характеристики') => $this->renderPartial('_attributes', array('model'=>$model), true),

		);
		if($model->mainCategory)
			$model->main_category_id = $model->mainCategory->id;

		// Or set selected category from type pre-set.
		if($model->type && !Yii::app()->request->isPostRequest && $model->isNewRecord){
			$model->main_category_id = $model->type->main_category;
		}





		if (Yii::app()->request->isPostRequest)
		{
			$model->attributes = $_POST['Page'];



			if ($model->isNewRecord)
				$model->created = date('Y-m-d H:i:s');
			$model->updated = date('Y-m-d H:i:s');

			if ($model->validate())
			{
				$model->save();

				// Process categories
				$mainCategoryId = 1;
				if(isset($_POST['Page']['main_category_id']))
					$mainCategoryId=$_POST['Page']['main_category_id'];

				$model->setCategories(Yii::app()->request->getPost('categories', array()), $mainCategoryId);

				$this->processAttributes($model);



				$model->save();

				$this->setFlashMessage(Yii::t('PagesModule.core', 'Изменения успешно сохранены'));

				if (isset($_POST['REDIRECT']))
					$this->smartRedirect($model);
				else
					$this->redirect(array('index'));
			}
		}

		$this->render('update', array(
			'model'=>$model,
			'form'=>$form,
		));
	}

	/**
	 * Delete page by Pk
	 */
	public function actionDelete()
	{
		if (Yii::app()->request->isPostRequest)
		{
			$model = Page::model()->findAllByPk($_REQUEST['id']);

			if (!empty($model))
			{
				foreach($model as $page)
					$page->delete();
			}

			if (!Yii::app()->request->isAjaxRequest)
				$this->redirect('index');
		}
	}

	/**
	 * Save model attributes
	 * @param Page $model
	 * @return boolean
	 */
	protected function processAttributes(Page $model)
	{
		$attributes = new CMap(Yii::app()->request->getPost('PageAttribute', array()));
		if(empty($attributes))
			return false;

		$deleteModel = Page::model()->findByPk($model->id);
		$deleteModel->deleteEavAttributes(array(), true);

		// Delete empty values
		foreach($attributes as $key=>$val)
		{
			if(is_string($val) && $val === '')
				$attributes->remove($key);
		}

		return $model->setEavAttributes($attributes->toArray(), true);
	}
}