<?php
	// Page create/edit view
/**
 * @var Page $model
 */

	$this->topButtons = $this->widget('application.modules.admin.widgets.SAdminTopButtons', array(
		'form'=>$form,
		'langSwitcher'=>!$model->isNewRecord,
		'deleteAction'=>$this->createUrl('/pages/admin/default/delete', array('id'=>$model->id))
	));

	$title = ($model->isNewRecord) ? Yii::t('PagesModule.admin', 'Создание страницы') :
		Yii::t('PagesModule.admin', 'Редактирование страницы');

	$this->breadcrumbs = array(
		'Home'=>$this->createUrl('/admin'),
		Yii::t('PagesModule.admin', 'Страницы')=>$this->createUrl('index'),
		($model->isNewRecord) ? Yii::t('PagesModule.admin', 'Создание страницы') : CHtml::encode($model->title),
	);

	$this->widget('application.modules.admin.widgets.schosen.SChosen', array(
		'elements'=>array('Page_category_id')
	));

	$this->pageHeader = $title;
$this->widget('application.modules.admin.widgets.schosen.SChosen', array(
	'elements'=>array('Page_main_category_id	')
));
clientScript()->registerCssFile($this->module->getAssetsUrl().'/css/bs.css');
clientScript()->registerScriptFile($this->module->getAssetsUrl().'/js/tests.js',CClientScript::POS_END);

js('/bower_components/underscore/underscore-min.js',CClientScript::POS_END);
?>
<?php clientScript()->registerCssFile("//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css")?>
<?php clientScript()->registerScriptFile("//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js",CClientScript::POS_END)?>
<?php

clientScript()->registerScript('',<<<JS
  $('.select2').select2({
   placeholder: "Выберите связанные обьекты",
  });


                    $('.edit').each(function(i,el){

                        var editor = CKEDITOR.replace(el);

                        editor.mode='source';

                        editor.on( 'blur', function( e ) {

                                        editor.updateElement();

                        });
                        editor.on( 'change', function( e ) {

                                        editor.updateElement();

                        } );

                    });



JS
	,CClientScript::POS_READY
);?>
<!-- Use padding-all class with SidebarAdminTabs -->
<div class="form wide padding-all">
	<?php echo $form->asTabs(); ?>
</div>

