<?php
/**
 * @var Page $model
 */
/*** Create/update page form ***/
Yii::import('zii.widgets.jui.CJuiDatePicker');
clientScript()->registerScriptFile(app()->theme->baseUrl.'/assets/js/holder.js',CClientScript::POS_END);
$image=$this->model->file;
$path='';
if($image){
	$path=baseUrl($image->resolveUrl());
}
$list=$this->model->getList();
/**
 * @var FileBehavior $image
 */
$model=$this->model;
$specs_files=app()->controller->renderPartial('_files',array('category'=>'specs','files'=>$model->spec->listAttachments()),true,false);
//$man_files=app()->controller->renderPartial('_files',array('category'=>'manuals','files'=>$model->manual->listAttachments()),true,false);
//$driver_files=app()->controller->renderPartial('_files',array('category'=>'drivers','files'=>$model->driver->listAttachments()),true,false);




$specfield=app()->controller->widget('CMultiFileUpload', array(
    'model'=>$model,
    'attribute'=>'specs',

    'options'=>array(
        // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
        // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
        // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
        // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
        // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
        // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
    ),
    'denied'=>'File is not allowed',
    'max'=>10, // max 10 files


),true);

/**
 * @var File $image
 */
$path='';
if($image){
$path=baseUrl($image->resolveUrl());
}
return array(
	'id'=>'PageUpdateForm',
	'showErrorSummary'=>true,
    'attributes' => array(
        'enctype' => 'multipart/form-data',
        'id'=>'PageUpdateForm',

    ),
	'elements'=>array(
		'content'=>array(
			'type'=>'form',
			'title'=>Yii::t('PagesModule.core', 'Содержимое'),
			'elements'=>array(
				'title'=>array(
					'type'=>'text',
				),
				'url'=>array(
					'type'=>'text',
				),
                'views_count'=>array(
                    'type'=>'text',
                ),
				'type_id'=>array(
					'type'=>'dropdownlist',
					'items'=>PageType::keyValueList(),
					'empty'=>'---',
                ),
				'main_category_id'=>array(
					'type'=>'dropdownlist',
					'items'=>CHtml::listData(PageCategory::model()->findAll(),'id','nameWithLevel'),
					'empty'=>'---',
				),
//				'category_id'=>array(
//					'type'=>'dropdownlist',
//					'items'=>PageCategory::keyValueList(),
//					'empty'=>'---',
//				),

                'vote_id'=>array(
                    'type'=>'dropdownlist',
                    'items'=>CHtml::listData(Poll::model()->findAll(),'id','title'),
                    'empty'=>'---',
				),

				'about'=>array(
					'type'=>app()->settings->get('core','editor'),
				),

                'short_description'=>array(
					'type'=>app()->settings->get('core','editor'),
				),
				'full_description'=>array(
					'type'=>app()->settings->get('core','editor'),
				),



                '<div class="row"><label>Теги</label><div>'.
                app()->controller->widget('CAutoComplete', array(
                    'model'=>$this->model,
                    'attribute'=>'tags',
                    'url'=>array('suggestTags'),
                    'multiple'=>true,
                    'htmlOptions'=>array('class'=>'span5'),
                ),true).'</div></div>',


				),
			),
		'seo'=>array(
			'type'=>'form',
			'title'=>Yii::t('PagesModule.core', 'Мета данные'),
			'elements'=>array(
				'meta_title'=>array(
					'type'=>'text',
				),
				'meta_keywords'=>array(
					'type'=>'textarea',
				),
				'meta_description'=>array(
					'type'=>'textarea',
				),
			),
		),
		'additional'=>array(
			'type'=>'form',
			'title'=>Yii::t('PagesModule.core', 'Дополнительно'),
			'elements'=>array(
				'status'=>array(
					'type'=>'dropdownlist',
					'items'=>Page::statuses()
				),
				'publish_date'=>array(
					'type'=>'CJuiDatePicker',
					'options'=>array(
						'dateFormat'=>'yy-mm-dd '.date('H:i:s'),
					),
				),
				'layout'=>array(
					'type'=>'text',
				),
				'view'=>array(
					'type'=>'text',
				),

			),
		),
		'relation'=>array(
			'type'=>'form',
			'title'=>Yii::t('PagesModule.core', 'Связанные обьекты'),
			'elements'=>array(


				"<label>Связанные обьекты</label> $list"
			),
		),
        'image'=>array(
            'type'=>'form',
            'title'=>Yii::t('PagesModule.core', 'Image'),
            'elements'=>array(

				'gallery_id'=>array(
					'type'=>'hidden',

				),
            '<div class="row"><label></label><div >'.(
            ($this->model->galleryBehavior->getGallery() === null) ?'<p>Перед добавлять фотографии, необходимо сохранить</p>'
                :
                app()->controller->widget('GalleryManager', array(
                    'gallery' => $this->model->galleryBehavior->getGallery(),

                    'controllerRoute' => '/gallery', //route to gallery controller
                ),true)).'</div></div>'
			),
		),
        'specs'=>array(
            'type'=>'form',
            'title'=>Yii::t('PagesModule.core', 'Файлы'),
            'elements'=>array(

                !$model->isNewRecord?'
    <div class="row filefield" style="padding: 10px 0px !important;
margin-bottom: 10px;
width: 100%;
float: left;">
                        <label for="filefield">Загрузить Файлы</label>
                        <div  class="col-xs-6">'.$specfield.'</div>

                </div>':'',
                !$model->isNewRecord?'<div class="row exists_files" style="padding: 10px 0px !important;
margin-bottom: 10px;
width: 100%;
float: left;">
                        <label for="exists_files">Загруженные файлы</label>
                        <div class="col-xs-6">'.$specs_files.'</div>

                </div>':'',

            ),
        ),

	),
);

