<?php
/**
 * @var Page $model
 */
clientScript()->scriptMap=array(
    'jquery'=>false
)
?>

<?php /*clientScript()->registerScript('',<<<JS

         $(function() {
        var dialog, form,



            title = $( "#title" ),
            description = $( "#description" ),

            allFields = $( [] ).add( title ).add( description ).add( password ),
            tips = $( ".validateTips" );

        function updateTips( t ) {
            tips
                .text( t )
                .addClass( "ui-state-highlight" );
            setTimeout(function() {
                tips.removeClass( "ui-state-highlight", 1500 );
            }, 500 );
        }



        function editFile(url) {
            var valid = true;
            allFields.removeClass( "ui-state-error" );
            $.ajax({
            url:url
            data:form.serialize(),

            })

            return valid;
        }

        dialog = $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 300,
            width: 350,
            modal: true,
            buttons: {
                "Сохранить": editFile,
                Cancel: function() {
                    dialog.dialog( "close" );
                }
            },
            close: function() {
                form[ 0 ].reset();
                allFields.removeClass( "ui-state-error" );
            }
        });

        form = dialog.find( "form" ).on( "submit", function( event ) {
            event.preventDefault();
            editFile(this.dataset.url);
        });

        $( "#edit-file" ).button().on( "click", function() {
            dialog.dialog( "open" );
        });
    });
JS
    ,CClientScript::POS_END
) */?>

<?php $dialog=CJavaScript::encode(<<<HTML
<div class="dialog-form" title="Редактировать файл" id="files_$category" >

     <form>
            <fieldset>
                <label for="title">Заголовок</label><br/>
                <input style="padding: 3px;width: 100%" type="text" name="title" id="title" value="" class="text ui-widget-content ui-corner-all"/><br/>
                <br/>
                <label for="description">Описание</label><br/>
                <textarea style="padding: 3px;width: 100%" rows="5" name="description" id="description" class="text ui-widget-content ui-corner-all"></textarea><br/>


                <!-- Allow form submission with keyboard without duplicating the dialog button -->
                <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
            </fieldset>
      </form>

</div>
HTML
);
?>



<div id="files">




    <?php

    Yii::import('ext.attach.*');


    ?>
    <table class="table table-striped table-hover">
    	<thead>
    		<tr>
    			<th width="300">Имя файла</th><th width="300">Редактировать</th><th width="300">Удалить</th>
    		</tr>
    	</thead>
    	<tbody>


    <?php
    /**
     * @var AttachmentFile $file
     */
    foreach($files as $id=>$file):?>

        <?php $file_model=app()->file->set($file);         ?>

            <?php
            $mime=explode('/',$file_model->getMimeTypeByExtension());
            ?>
            <tr  class="file" id="<?php echo $id ?>">
                <td>

            <?php echo TbHtml::link('<span class="glyphicon glyphicon-download-alt"> </span> '.$file_model->getFilename(),app()->controller->createUrl('downloadAttachment',array('id'=>$id)))?>
                </td>  <td>
                    <?php
                    $url=app()->controller->createUrl('editAttachment',array('id'=>$id));
                    echo TbHtml::ajaxButton('<span class="glyphicon glyphicon-edit"> </span> ',$url,
                        array(
                            'dataType'=>'json',

                            'success'=><<<JS
    function(data){

 var dialog = $($dialog).dialog({
            autoOpen: false,
            height: 300,
            width: 350,
            modal: true,
            open: function( event, ui ) {
                var form = $(this).find('form');

                 var title = form.find("#title" );
                var description = form.find("#description" );

                title.val(data.title);
                description.val(data.description);

                 form.on( "submit", function( event ) {
                    event.preventDefault();
                });
            },
            buttons: {
                "Сохранить": function(){
                    var that=this;
                  var form = $(this).find('form');
             var edit=function(form){


              var data=form.serialize();
                 console.log(data);
                   console.log(form);
                console.log(this);
                $.ajax({
                url:'$url',
                data:form.serialize(),
                type:'POST',
                success:function(data){
                    console.log(data);
                    dialog.dialog( "close" );
                }
                });
            };



              edit(form);



                },
                Cancel: function() {
                    dialog.dialog( "close" );
                }
            },
            close: function() {
                var form = $(this).find('form');
                form[ 0 ].reset();

            }
        });




        dialog.dialog('open');
}
JS

                        ))?>
                </td>  <td>
            <?php echo TbHtml::ajaxButton('<span class="glyphicon glyphicon-remove"> </span> ',app()->controller->createUrl('deleteAttachment',array('id'=>$id)),array(
                'dataType'=>'json',
                'success'=>"function(data){
    if(data.status!=0){
        $('#'+data.status).remove();

    }
}"
            ))?> </td>
            </tr>

    <?php endforeach?>

    </tbody>

    </table>
</div>