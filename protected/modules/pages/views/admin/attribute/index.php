<?php

/**
 * Display attributes list
 **/

$this->pageHeader = Yii::t('PagesModule.admin', 'Атрибуты');

$this->breadcrumbs = array(
	'Home'=>$this->createUrl('/admin'),
	Yii::t('PagesModule.admin', 'Атрибуты'),
);

$this->topButtons = $this->widget('application.modules.admin.widgets.SAdminTopButtons', array(
	'template'=>array('create'),
	'elements'=>array(
		'create'=>array(
			'link'=>$this->createUrl('create'),
			'title'=>Yii::t('PagesModule.admin', 'Создать атрибут'),
			'options'=>array(
				'icons'=>array('primary'=>'ui-icon-plus')
			)
		),
	),
));

$this->widget('ext.sgridview.SGridView', array(
	'dataProvider'=>$dataProvider,
	'id'=>'productsListGrid',
	'filter'=>$model,
	'columns'=>array(
		array(
			'class'=>'CCheckBoxColumn',
		),
		array(
			'class'=>'SGridIdColumn',
			'name'=>'id'
		),
		array(
			'name'=>'title',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->title), array("/pages/admin/attribute/update", "id"=>$data->id))',
		),
		'name',
		array(
			'name'=>'type',
			'filter'=>PageAttribute::getTypesList(),
			'value'=>'CHtml::encode(PageAttribute::getTypeTitle($data->type))'
		),
		'position',
		// Buttons
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
));