<?php

return array(
	'id'=>'attributeUpdateForm',
	'showErrorSummary'=>true,
	'elements'=>array(
		'content'=>array(
			'type'=>'form',
			'title'=>Yii::t('PagesModule.admin', 'Параметры'),
			'elements'=>array(
				'title'=>array(
					'type'=>'text',
				),
				'name'=>array(
					'type'=>'text',
					'hint'=>Yii::t('PagesModule.admin', 'Укажите уникальный идентификатор')
				),
					'required'=>array(
						'type'=>'checkbox',
					),
				'type'=>array(
					'type'=>'dropdownlist',
					'items'=>PageAttribute::getTypesList()
				),
				'display_on_front'=>array(
					'type'=>'dropdownlist',
					'items'=>array(
						1=>Yii::t('PagesModule.admin', 'Да'),
						0=>Yii::t('PagesModule.admin', 'Нет')
					),
				),
				'use_in_filter'=>array(
					'type'=>'dropdownlist',
					'items'=>array(
						1=>Yii::t('PagesModule.admin', 'Да'),
						0=>Yii::t('PagesModule.admin', 'Нет'),
					),
					'hint'=>Yii::t('PagesModule.admin', 'Использовать свойство для поиска материалов')
				),
				'select_many'=>array(
					'type'=>'dropdownlist',
					'items'=>array(
						0=>Yii::t('PagesModule.admin', 'Нет'),
						1=>Yii::t('PagesModule.admin', 'Да')
					),
					'hint'=>Yii::t('PagesModule.admin', 'Позволяет искать материалы по более чем одному параметру одновременно')
				),
				'use_in_variants'=>array(
					'type'=>'dropdownlist',
					'items'=>array(
						0=>Yii::t('PagesModule.admin', 'Нет'),
						1=>Yii::t('PagesModule.admin', 'Да'),
					),
				),
				'use_in_compare'=>array(
					'type'=>'dropdownlist',
					'items'=>array(
						1=>Yii::t('PagesModule.admin', 'Да'),
						0=>Yii::t('PagesModule.admin', 'Нет'),
					),
				),
				'position'=>array(
					'type'=>'text',
				),
			),
		),
	),
);