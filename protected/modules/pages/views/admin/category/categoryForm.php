<?php

/*** Create/update page form ***/

return array(
	'id'=>'pageUpdateForm',
	'showErrorSummary'=>true,
	'elements'=>array(
		'content'=>array(
			'type'=>'form',
			'title'=>Yii::t('PagesModule.core', 'Параметры'),
			'elements'=>array(
				'name'=>array(
					'type'=>'text',
				),
				'url'=>array(
					'type'=>'text',
				),
				'parent_id'=>array(
					'type'=>'dropdownlist',
					'items'=>PageCategory::keyValueList(),
					'empty'=>'---',
					'options'=>array(
						$this->model->id=>array('disabled'=>true),
					)
				),
				'description'=>array(
					'type'=>app()->settings->get('core','editor'),
				),
                '<div class="row"><label></label><div style="margin-left:180px">'.(
                ($this->model->galleryBehavior->getGallery() === null) ?'<p>Перед добавлять фотографии, необходимо сохранить</p>'
                    :
                    app()->controller->widget('GalleryManager', array(
                        'gallery' => $this->model->galleryBehavior->getGallery(),

                        'controllerRoute' => '/gallery', //route to gallery controller
                    ),true)).'</div></div>'

			),
		),
		'seo'=>array(
			'type'=>'form',
			'title'=>Yii::t('PagesModule.core', 'Мета данные'),
			'elements'=>array(
				'meta_title'=>array(
					'type'=>'text',
				),
				'meta_keywords'=>array(
					'type'=>'textarea',
				),
				'meta_description'=>array(
					'type'=>'textarea',
				),
			),
		),
		'design'=>array(
			'type'=>'form',
			'title'=>Yii::t('PagesModule.core', 'Внешний вид'),
			'elements'=>array(
				'page_size'=>array(
					'type'=>'text',
				),
				'layout'=>array(
					'type'=>'text',
				),
				'view'=>array(
					'type'=>'text',
				),
			),
		),
	),
);

