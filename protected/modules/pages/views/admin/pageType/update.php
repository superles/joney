<?php

/**
 * Create/update product types
 *
 * @var $this SAdminController
 */

$this->topButtons = $this->widget('application.modules.admin.widgets.SAdminTopButtons', array(
	'formId'=>'PageTypeForm',
	//'langSwitcher'=>!$model->isNewRecord,
	'deleteAction'=>$this->createUrl('/pages/admin/pageType/delete', array('id'=>$model->id))
));

$title = ($model->isNewRecord) ? Yii::t('PagesModule.admin', 'Создание нового материала') :
	Yii::t('PagesModule.admin', 'Редактирование типа материала');

$this->breadcrumbs = array(
	'Home'=>$this->createUrl('/admin'),
	Yii::t('PagesModule.admin', 'Типы материалов')=>$this->createUrl('index'),
	($model->isNewRecord) ? Yii::t('PagesModule.admin', 'Создание нового материала') : CHtml::encode($model->name),
);

$this->pageHeader = $title;

// Register scripts
Yii::app()->clientScript->registerScriptFile(
	$this->module->assetsUrl.'/admin/productType.update.js',
	CClientScript::POS_END
);

?>

<div class="form wide padding-all">
	<?php
	echo CHtml::beginForm('', 'post',array(
		'id'=>'PageTypeForm'
	));
	echo CHtml::errorSummary($model);

	echo CHtml::hiddenField('main_category', $model->main_category);

	$this->widget('ext.sidebartabs.SAdminSidebarTabs', array(
		'tabs'=>array(
			Yii::t('PagesModule.admin','Опции')     => $this->renderPartial('_options', array('model'=>$model,'attributes'=>$attributes), true),
			Yii::t('PagesModule.admin','Категории') => $this->renderPartial('_tree', array('model'=>$model), true),
		)
	));
	echo CHtml::endForm(); ?>
</div><!-- end form -->
