<?php

/**
 * View page
 * @var Page $model
 */

// Set meta tags
$this->pageTitle       = ($model->meta_title) ? $model->meta_title : $model->title;
$this->pageKeywords    = $model->meta_keywords;
$this->pageDescription = $model->meta_description;
?>
<div class="h-30"></div>
<div class="service_btn" >
    <h1 style="font-weight: bold"><?php echo $model->title;  ?></h1>
</div>
<p class="date"><?php echo dateFormatter()->formatDateTime($master->getTimestamp()->getTimestamp(),'medium',false) ?></p>

<div class="small_text">
    <i><?php echo $model->full_description?></i>
</div>