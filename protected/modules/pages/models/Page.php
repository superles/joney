<?php


Yii::import('application.modules.pages.models.PageCategoryRef');
/**
 * This is the model class for table "Pages".
 *
 * The followings are the available columns in table 'Pages':
 * @property integer $id
 * @property integer $user_id
 * @property integer $category_id
 * @property string $title
 * @property string $url
 * @property string $short_description
 * @property string $full_description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $created
 * @property string $updated
 * @property int $type_id
 * @property string $publish_date
 * @property string $status
 * @property string $layout
 * @property string $view
 * @property string $tags
 * @property integer $views_count
 * @property integer $vote_id
 * @property integer $image_id
 * @property PageTranslate $translate
 * @property File $file
 * @property GalleryBehavior $galleryBehavior
 * @method Page findByPk
 * @property PageCategory mainCategory
 * @method Page limitSidebar sorted menu
 * @method Page published()
 * @method Page[] findAll findAllByAttributes
 * @property FileBehavior $fileBehavior
 * @method Image loadImage()
 * @mixin GalleryBehavior
 * @mixin AttachmentBehavior
 * @property  AttachmentBehavior $specs
 * @property Map $map
 * @mixin EEavBehavior
 * @mixin STranslateBehavior
 * TODO: Set DB indexes
 */
class Page extends BaseModel
{

	public $_statusLabel;

	/**
	 * Status to allow display page on the front.
	 */
	public $publishStatus = 'published';

	/**
	 * Multilingual attrs
	 */
	public $title;
	public $short_description;
	public $full_description;
	public $meta_title;
	public $meta_description;
	public $meta_keywords;
    public $upload;
    public $about;
	public $rel;
	public $main_category_id;


	private $_oldTags;

	/**
	 * @param bool $is_models
	 * @return array|Page[]
	 */
	public function getRel($is_models=false){
		$childs=$this->childrens?:false;
		if($childs){
			$data=array_map(function($el) use($is_models){
				if($is_models){
					return $el;
				}else{
					return $el->id;
				}

			},$childs);
			return $data;
		}else{
			return array();
		}

	}
	public function getMaps($is_models=false){
		$childs=$this->maps?:false;
		if($childs){
			$data=array_map(function($el) use($is_models){
				if($is_models){
					return $el;
				}else{
					return $el->id;
				}

			},$childs);
			return $data[0];
		}else{
			return array();
		}

	}
	public function setRel($values){
		return array();
	}

	/**
	 * Name of the translations model.
	 */
	public $translateModelName = 'PageTranslate';

	/**
	 * Returns the static model of the specified AR class.
	 * @return Page
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
    public function getCategoryShortUrl(){
        $cat=PageCategory::model()->findByPk($this->category_id);
        return $cat?$cat->url:'';
    }

    public function getAllPks($category=false){
        if($category){
            $cat_val=$category;
        }else{
            $cat_val=$this->category_id;
        }
            $ids=array();
            $criteria=$this->getDbCriteria();
            $category=PageCategory::model()->findAllByAttributes(array('parent_id'=>$cat_val));
            if($category&&count($category)>0){


                foreach($category as $cat){
                    $ids[]=intval($cat->id);
                }
            }
            if(count($ids)>0){
                array_push($ids,$cat_val);
                $criteria->addInCondition('category_id',$ids);
            }else{
                $criteria->compare('category_id',$cat_val);
            }
        $criteria->mergeWith(Page::model()->published()->getDbCriteria());
        $criteria->order='t.id ASC';
        $res=array();
        $this->findAll($criteria);
        if($this->findAll($criteria)&&count($this->findAll($criteria))>0){


            foreach($this->findAll($criteria) as $item){
                $res[]=intval($item->id);
            }

        }
        return $res;


    }

    public function getNext(){
        $all=$this->getAllPks($this->category_id);
        sort($all);
        foreach($all as $id){
            if($id>$this->id){
                return Page::model()->findByPk($id)->getViewUrl();
            }
        }
        return Page::model()->findByPk($all[0])->getViewUrl();
    }
    public function getPrev(){
        $all=$this->getAllPks($this->category_id);
        rsort($all);
        foreach($all as $id){
            if($id<$this->id){
                return Page::model()->findByPk($id)->getViewUrl();
            }
        }
        return Page::model()->findByPk($all[0])->getViewUrl();

    }

	public function tableName()
	{
		return 'Page';
	}

	public function defaultScope()
	{
		return array(
			'order'=>'publish_date DESC',
		);
	}

    public function getMenu(){
        $menu=array();
        $pages=Page::model()->findAll($this->getDbCriteria());
        if($pages){
        foreach ($pages as $page){
            if($page->category->url=='projects'){
                if(user()->isAdmin()||(!user()->isGuest&&user()->getModel()&&user()->getModel()->profile&&user()->getModel()->profile->related_project&&in_array($page->id,user()->getModel()->profile->related_project))) {
                    $menu[] = array('label' => "<span class=\"glyphicon glyphicon-chevron-right\"></span> {$page->title}", 'url' => $page->getViewUrl());
                }
            }else{
                $menu[]=array('label' => "<span class=\"glyphicon glyphicon-chevron-right\"></span> {$page->title}", 'url' => $page->getViewUrl());
            }

        }
        }
        return $menu;
    }

	public function scopes()
	{
		return array(
			'published'=>array(
				'condition'=>'t.publish_date <= :date AND t.status = :status',
				'params'=>array(
					':date'=>date('Y-m-d H:i:s'),
					':status'=>$this->publishStatus
				),
			),
			'menu'=>array(
				'order'=>'publish_date ASC',
			),
			'sorted'=>array(
				'order'=>'publish_date DESC',
			),

			'limitSidebar'=>array(
				'limit'=>3,
				'order'=>'publish_date DESC',
			),

		);
	}

	/**
     * @return array a list of links that point to the post list filtered by every tag of this post
     */
    public function getTagLinks()
    {
        $links=array();
        foreach(PageTag::string2array($this->tags) as $tag)
            $links[]=CHtml::link(CHtml::encode($tag), array('/blog/post/index', 'tag'=>$tag));
        return $links;
    }

    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags($attribute,$params)
    {
        $this->tags=PageTag::array2string(array_unique(PageTag::string2array($this->tags)));
    }

    /**
     * Adds a new comment to this post.
     * This method will set status and post_id of the comment accordingly.
     * @param Comment the comment to be added
     * @return boolean whether the comment is saved successfully
     */
    public function addComment($comment)
    {
        if(Yii::app()->params['commentNeedApproval'])
            $comment->status=Comment::STATUS_PENDING;
        else
            $comment->status=Comment::STATUS_APPROVED;
        $comment->post_id=$this->id;
        return $comment->save();
    }

    /**
     * This is invoked when a record is populated with data from a find() call.
     */
    protected function afterFind()
    {
        parent::afterFind();
        $this->_oldTags=$this->tags;
        if(empty($this->views_count)){
            $this->views_count=1;
            $this->save();
        }else{
            $this->saveCounters(array(
                'views_count'=>1
            ));
        }
    }

	/**
	 * Find page by url.
	 * Scope.
	 * @param string Page url
	 * @return Page
	 */
	public function withUrl($url)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'url=:url',
			'params'=>array(':url'=>$url)
		));

		return $this;
	}

	/**
	 * Filter pages by category.
	 * Scope.
	 * @param PageCategory|int $category
	 * @return Page
	 */
	public function filterByCategory($category,$is_main=true)
	{
		$c=$this->getDbCriteria();
		$c->addInCondition('t.id',array_map(function(PageCategoryRef $el){ return $el->page; },PageCategoryRef::model()->findAllByAttributes(array('is_main'=>$is_main,'category'=>($category instanceof PageCategory)?$category->id:$category))));
		$this->setDbCriteria($c);
		return $this;
	}
    public function filterByUser($user)
	{
		if($user instanceof User)
			$user=$user->id;

		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'user_id=:user',
			'params'=>array(':user'=>$user)
		));

		return $this;
	}
	/**
	 * Filter pages by category.
	 * Scope.
	 * @param PageCategory|int $category
	 * @return Page
	 */
	public function menuByCategory($category)
	{
		if($category instanceof PageCategory)
			$category=$category->id;

		$this->menu()->getDbCriteria()->mergeWith(array(
			'condition'=>'category_id=:category',
			'params'=>array(':category'=>$category)
		));
		$data=array();
		foreach($this->findAll() as $page){
			$data[]=array(
				'label'=>$page->title,
				'url'=>$page->getViewUrl()
			);
		}

		return $data;
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('short_description, full_description, category_id', 'type', 'type'=>'string'),
			array('status', 'in', 'range'=>array_keys(self::statuses())),
			array('title, status, publish_date', 'required'),
			array('url', 'LocalUrlValidator'),
			array('publish_date', 'date', 'format'=>'yyyy-MM-dd HH:mm:ss'),
            array('tags', 'match', 'pattern'=>'/^[А-Яа-я\w\s,]+$/u', 'message'=>'Tags can only contain word characters.'),
            array('tags', 'normalizeTags'),
			array('rel','safe'),
            array('upload', 'file', 'allowEmpty' => true, 'safe'=>true, 'types' => 'jpg, jpeg, gif, png'),
            array('image_id,gallery_id,views_count,vote_id,about,type_id,main_category_id', 'safe'),
			array('title, url, meta_title, meta_description, meta_keywords, publish_date, layout, view', 'length', 'max'=>255),
			// The following rule is used by search().
			array('id, type_id, user_id, title, url, short_description, full_description, meta_title, meta_description, meta_keywords, created, updated, publish_date,tags,gallery_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'vote'=>array(self::BELONGS_TO, 'Poll', 'vote_id'),
			'translate'=>array(self::HAS_ONE, $this->translateModelName, 'object_id'),
			'author'=>array(self::BELONGS_TO, 'User', 'user_id'),
			'category'=>array(self::BELONGS_TO, 'PageCategory', 'category_id'),
            'file' => array(self::BELONGS_TO, 'File', 'image_id'),
            'files' => array(self::MANY_MANY, 'File', 'PageFile(page_id, file_id)'),
			'childrens'=>array(self::MANY_MANY, 'StoreProduct',
				'PageRelation(page_id, relation_id)'),

			'map'=>array(self::BELONGS_TO, 'Map','map_id'),
			'type'=>array(self::BELONGS_TO, 'PageType','type_id'),



			'maps'=>array(self::MANY_MANY, 'Map',
				'PageMap(map_id,page_id)'),
			'parents'=>array(self::MANY_MANY, 'StoreProduct',
				'PageRelation(relation_id,page_id)'),
			'categorization'  => array(self::HAS_MANY, 'PageCategoryRef', 'page'),
			'categories'      => array(self::HAS_MANY, 'PageCategory',array('category'=>'id'), 'through'=>'categorization'),
			'mainCategory'    => array(self::HAS_ONE, 'PageCategory', array('category'=>'id'), 'through'=>'categorization', 'condition'=>'categorization.is_main = 1','scopes'=>'applyTranslateCriteria'),
		);
	}

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return array(

			'eavAttr' => array(
				'class'     => 'ext.behaviors.eav.EEavBehavior',
				'tableName' => 'PageAttributeEAV',
			),

			'STranslateBehavior'=>array(
				'class'=>'ext.behaviors.STranslateBehavior',
				'translateAttributes'=>array(
					'title',
					'short_description',
					'full_description',
					'meta_title',
					'meta_description',
					'meta_keywords',
                    'about'
				),
			),
            'fileBehavior' => array(
                'class' => 'ext.yii-filemanager.behaviors.FileBehavior',
                'name' => $this->url,
                'path' => 'pages',
            ),

            'galleryBehavior' => array(
                'class' => 'GalleryBehavior',
                'idAttribute' => 'gallery_id',
                'versions' => array(
                    'pager' => array(
                        'adaptive' => array(100, 100),
                    ),
                    'main' => array(
                        'adaptive' => array(270,220),
                    )
                ),
                'name' => true,
                'description' => true,

            ),
            'spec' => array(
                'class' => 'ext.attach.AttachmentBehavior',
                # Should be a DB field to store path/filename
                'attribute'=>'specs',
                # Default image to return if no image path is found in the DB
                //'fallback_image' => 'images/sample_image.gif',
                'path' => "attachments/:model/:id.:ext",
//                'processors' => array(
//                    array(
//                        # Currently GD Image Processor and Imagick Supported
//                        'class' => 'ImagickProcessor',
//                        'method' => 'resize',
//                        'params' => array(
//                            'width' => 310,
//                            'height' => 150,
//                            'keepratio' => true
//                        )
//                    )
//                ),
//                'styles' => array(
//                    # name => size
//                    # use ! if you would like 'keepratio' => false
//                    'thumb' => '!100x60',
//                )
            ),

		);
	}

	public function getRandom()
	{
		$pages=Page::model()->filterByCategory(3)->findAll();
		$data=array();
		foreach($pages as $page){
			if(!$page->getIsPaid()){
				$data[]=$page;
			}

		}
		if((count($data)-1)>0){
			return $data[rand(0,count($data)-1)]->getRel()[0];
		}else{
			false;
		}

	}

	public function getIsPaid()
	{
		if(user()->isGuest)
			return false;
		$product=$this->getRel(true)[0];
		$ids=array_map(function(Order $el){ return $el->id; },Order::model()->findAllByAttributes(array('user_id'=>user()->id)));
		$criteria=new CDbCriteria();
		$criteria->compare('t.order_id',$ids);
		$criteria->compare('t.product_id',$product->id);
		if($oproduct=OrderProduct::model()->find($criteria)){
			$order=$oproduct->order;
			if($order->paid==1){
			return true;
			}
		}
		return false;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => Yii::t('PagesModule.core', 'Автор'),
			'category_id' => Yii::t('PagesModule.core', 'Категория'),
			'title' => Yii::t('PagesModule.core', 'Заглавие'),
			'url' => Yii::t('PagesModule.core', 'URL'),
			'short_description' => Yii::t('PagesModule.core', 'Краткое описание'),
			'full_description' => Yii::t('PagesModule.core', 'Содержание'),
			'meta_title' => Yii::t('PagesModule.core', 'Meta Title'),
			'meta_description' => Yii::t('PagesModule.core', 'Meta Description'),
			'meta_keywords' => Yii::t('PagesModule.core', 'Meta Keywords'),
			'created' => Yii::t('PagesModule.core', 'Дата создания'),
			'updated' => Yii::t('PagesModule.core', 'Дата обновления'),
			'publish_date' => Yii::t('PagesModule.core', 'Дата публикации'),
			'status' => Yii::t('PagesModule.core', 'Статус'),
			'layout' => Yii::t('PagesModule.core', 'Макет'),
			'view' => Yii::t('PagesModule.core', 'Шаблон'),
            'tags' => Yii::t('blog','Теги'),
            'upload' => Yii::t('PagesModule.core', 'Image'),
            'about' => 'Доп. описание',
			'type_id' => 'Тип',
			'main_category_id'       => Yii::t('StoreModule.core', 'Категория'),
		);
	}

	/**
	 * @return array
	 */
	public static function statuses()
	{
		return array(
			'published'=>Yii::t('PagesModule.core', 'Опубликован'),
			'waiting'=>Yii::t('PagesModule.core', 'Ждет одобрения'),
			'draft'=>Yii::t('PagesModule.core', 'Черновик'),
			'archive'=>Yii::t('PagesModule.core', 'Архив'),
		);
	}

	/**
	 * @return mixed
	 */
	public function getStatusLabel()
	{
		$statuses = $this->statuses();
		return $statuses[$this->status];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions. Used in admin search.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($params = array(), $additionalCriteria = null)
	{
		$criteria=new CDbCriteria;



		$criteria->with = array(
			'categorization',
			'author','translate',
			'type',
		);

		$criteria->compare('t.id',$this->id);

		$criteria->compare('t.user_id',$this->user_id,true);
		$criteria->compare('t.type_id',$this->type_id,true);
		$criteria->compare('translate.title',$this->title,true);
		$criteria->compare('t.url',$this->url,true);
		$criteria->compare('translate.short_description',$this->short_description,true);
        $criteria->compare('translate.about',$this->about,true);
		$criteria->compare('translate.full_description',$this->full_description,true);
		$criteria->compare('translate.meta_title',$this->meta_title,true);
		$criteria->compare('translate.meta_description',$this->meta_description,true);
		$criteria->compare('translate.meta_keywords',$this->meta_keywords,true);
		$criteria->compare('t.created',$this->created,true);
		$criteria->compare('t.updated',$this->updated,true);
		$criteria->compare('t.publish_date',$this->publish_date,true);
		$criteria->compare('t.status',$this->status);
        $criteria->compare('t.tags',$this->tags);
		$criteria->compare('t.type_id', $this->type_id);
		if (isset($params['category']) && $params['category'])
		{
			$criteria->with=array('categorization'=>array('together'=>true));
			$criteria->compare('categorization.category', $params['category']);
		}


		// Create sorting by translation title
		$sort=new CSort;
		$sort->attributes=array(
			'*',
			'title' => array(
				'asc'   => 'translate.title',
				'desc'  => 'translate.title DESC',
			)
		);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>20,
			)
		));
	}

	/**
	 * @return bool
	 */
	public function beforeSave()
	{
		if(isset($_REQUEST['Page'])&&isset($_REQUEST['Page']['main_category_id'])&&$_REQUEST['Page']['main_category_id']==3){
			if($this->isNewRecord&&!$this->rel){
				$product=new StoreProduct();
				$product->name=$this->title;
				$product->price=floatval($_REQUEST['PageAttribute']['price']);
				$product->save();
				$this->rel=$product->id;


			}else{
				$rel_models=$this->getRel(true);
				if(!empty($rel_models)){
					$rel_models[0]->price=$this->getEavAttribute('price');
					$rel_models[0]->save();
				}
			}
		}
		if($this->rel){
			PageRelation::model()->deleteAll('page_id=:page',array(':page'=>$this->id));
			if(!is_array($this->rel))
				$this->rel=array($this->rel);
			foreach($this->rel as $rel_id){

				$relation=PageRelation::model()->findByAttributes(array('page_id'=>$this->id,'relation_id'=>$rel_id));
				if(!$relation){
					$relation=new PageRelation();
					$relation->page_id=$this->id;
					$relation->relation_id=$rel_id;
					$relation->save();
				}
			}
		}
		if(!$this->created && $this->isNewRecord)
			$this->created = date('Y-m-d H:i:s');
		if(!$this->updated)
			$this->updated = date('Y-m-d H:i:s');

		if ($this->isNewRecord&&!Yii::app()->user->isGuest)
			$this->user_id = Yii::app()->user->id;

		if (empty($this->url))
		{
			// Create slug
			Yii::import('ext.SlugHelper.SlugHelper');
			$this->url = SlugHelper::run($this->title);
		}

		// Check if url available
		if($this->isNewRecord)
		{
			$test = Page::model()
				->withUrl($this->url)
				->count();
		}
		else
		{
			$test = Page::model()
				->withUrl($this->url)
				->count('id!=:id', array(':id'=>$this->id));
		}

		if ($test > 0)
			$this->url .= '-'.date('YmdHis');

        if($file=$this->fileBehavior->getUploadedFile()){
            $model=$this->fileBehavior->saveFile($file);

        }





		return parent::beforeSave();
	}

	/**
     * This is invoked after the record is saved.
     */
    protected function afterSave()
    {
        parent::afterSave();
        PageTag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete()
    {
        parent::afterDelete();


        PageTag::model()->updateFrequency($this->tags, '');
    }

	/**
	 * Get url to view object on front
	 * @return string
	 */
	public function getViewUrl()
	{
		return Yii::app()->createAbsoluteUrl('pages/pages/view', array('url'=>$this->url));
	}

    /**
     * @return DateTime
     */
    public function getTimestamp(){
       $date=DateTime::createFromFormat('Y-m-d H:i:s',$this->publish_date);


       return $date;
    }
    public function getIwi(){

        $file=$this->getImage(true);
        return $file?iwi($file->getPath()):iwi(webroot('/styles/img/noimage.png'));
    }
	public function setCategories(array $categories, $main_category)
	{
		$dontDelete = array();

		if(!PageCategory::model()->countByAttributes(array('id'=>$main_category)))
			$main_category = 1;

		if(!in_array($main_category, $categories))
			array_push($categories, $main_category);

		foreach($categories as $c)
		{
			$count = PageCategoryRef::model()->countByAttributes(array(
				'category'=>$c,
				'page'=>$this->id
			));

			if($count == 0)
			{
				$record = new PageCategoryRef;
				$record->category = (int)$c;
				$record->page = $this->id;
				$record->save(false);
			}

			$dontDelete[] = $c;
		}

		// Clear main category
		PageCategoryRef::model()->updateAll(array(
			'is_main'=>0
		), 'page=:p', array(':p'=>$this->id));

		// Set main category
		PageCategoryRef::model()->updateAll(array(
			'is_main'=>1
		), 'page=:p AND category=:c ', array(':p'=>$this->id,':c'=>$main_category));

		// Delete not used relations
		if(sizeof($dontDelete) > 0)
		{
			$cr = new CDbCriteria;
			$cr->addNotInCondition('category', $dontDelete);

			PageCategoryRef::model()->deleteAllByAttributes(array(
				'page'=>$this->id,
			), $cr);
		}
		else
		{
			// Delete all relations
			PageCategoryRef::model()->deleteAllByAttributes(array(
				'page'=>$this->id,
			));
		}
	}
    /**
     * @param bool $file
     * @return GalleryPhoto||string
     */
	public function applyCategories($categories, $select = 't.*')
	{
		if($categories instanceof PageCategory)
			$categories = array($categories->id);
		else
		{
			if(!is_array($categories))
				$categories = array($categories);
		}

		$criteria = new CDbCriteria;

		if($select)
			$criteria->select = $select;
		$criteria->join = 'LEFT JOIN `PageCategoryRef` `categorization` ON (`categorization`.`page`=`t`.`id`)';
		$criteria->addInCondition('categorization.category', $categories);
		$this->getDbCriteria()->mergeWith($criteria);

		return $this;
	}
    public function getImages(){
        $photos=$this->getGalleryPhotos();
        $url=false;
        if($photos){
            return $photos;
    }

    }
    public function getPicture($class){
        $photos=$this->getGalleryPhotos();
        $url='';
        if($photos){
            $photo=$photos[0];
            $url=$photo->getUrl();
        }
        return CHtml::image($url,'',array('class'=>$class));

    }
    public function getAdaptive($width=200,$height=200,$class=''){
        $photos=$this->getGalleryPhotos();
        $url=false;
        if($photos){
            $photo=$photos[0];
            $url=iwi($photo->getPath())->adaptive($width,$height,true)->cache();
        }
        return CHtml::image($url,'',array('class'=>$class));
    }
    public function getResized($width=false,$height=false,$class=''){
        if($width||$height){
        $photos=$this->getGalleryPhotos();
        $url=false;
        if($photos){
            $photo=$photos[0];
            $url=iwi($photo->getPath())->resize($width?:$height,$height?:$width,$height?Image::HEIGHT:Image::WIDTH)->cache();
        }
        return CHtml::image($url,'',array('class'=>$class));
        }else{
            return '';
        }
    }
    public function getImage($file=false){
        $photos=$this->getPhotos();
        $url=false;
        if($photos){
            $photo=$photos[0];
            $url=$file?$photo:$photo->getUrl();
        }
        return $url;
    }

    public function getIndexImage($file=false){
        $photos=$this->getGalleryPhotos('front');
        $url=false;
        if($photos){
            $photo=$photos[0];
            $url=$file?$photo:$photo->getUrl();
        }
        return $url;
    }
    public function getBannerImage($file=false){
        $photos=$this->getGalleryPhotos('banner');
        $url=false;
        if($photos&&count($photos)>0){
            $photo=$photos[0];
            $url=$file?$photo:$photo->getUrl();
        }
        return $url;
    }
    public function getTopImage($file=false){
        $photos=$this->getGalleryPhotos('top');
        $url=false;
        if($photos&&count($photos)>0){
            $photo=$photos[0];
            $url=$file?$photo:$photo->getUrl();
        }
        return $url;
    }
    public function getMainImage($file=false){
        $photos=$this->getGalleryPhotos('pack');
        $url=false;

        if($photos&&count($photos)>0){
            $photo=$photos[0];
            $url=$file?$photo:$photo->getUrl();
        }
        return $url;
    }
    public function getMainImages($file=false){
        $photos=$this->getGalleryPhotos('pack');
        $url=array();

        if($photos&&count($photos)>0){
            foreach($photos as $photo){
                $url=$file?$photo:$photo->getUrl();
            }


        }
        return $url;
    }
	public function getList(){

		if($this->category&&$this->main_category_id){

			$rel=$this->main_category_id;

			$ids=new CMap();
			$ids->mergeWith(array_map(function($el){return $el['id'];},db()->createCommand()->from(StoreProduct::model()->tableName())->select('id')->queryAll(true)));

			$c=new CDbCriteria();
			$c->addInCondition('category_id',$ids->toArray());
			$models=Page::model()->findAll($c);

		}else{
			$models=Page::model()->findAll();
		}

		return CHtml::dropDownList('Page[rel]',$this->getRel(),CHtml::listData(StoreProduct::model()->findAll(),'id','name'),array('class'=>'select2','prompt'=>'','placeholder'=>'Выберите связанный продукт'));
	}

	public function __get($name)
	{
		if(substr($name,0,4) === 'eav_')
		{
			if($this->getIsNewRecord())
				return null;

			$attribute = substr($name, 4);
			$eavData = $this->getEavAttributes();

			if(isset($eavData[$attribute]))
				$value = $eavData[$attribute];
			else
				return null;

			$attributeModel = PageAttribute::model()->findByAttributes(array('name'=>$attribute));
			return $attributeModel->renderValue($value);
		}
		return parent::__get($name);
	}

	public function editor($attribute='full_description')
	{
		Yii::import('application.modules.admin.components.*');
		$widget=TbHtml::beginForm('/admin/pages/default/update?id='.$this->id);
		$widget.=TbHtml::hiddenField('partial',1);
		$widget.=TbHtml::activeHiddenField($this,'id');

		$widget.=app()->controller->widget(app()->settings->get('core','editor'),array('model'=>$this,'attribute'=>'full_description','id'=>CHtml::activeId($this,$attribute),'inline'=>true,'auto'=>false),true);
		$widget.=TbHtml::endForm();

		return $widget;


	}
	public function editorLink($attribute='full_description',$label='Редактор')
	{
		$link="<li><a href='#' onclick=\"return setupCKEditor('".CHtml::activeId($this,$attribute)."', this, false, true);\">$label</a></li>";;
		return $link;


	}
}
