<?php

/**
 * Class PageCategoryRef
 * @property integer $id
 * @property integer $category
 * @property integer $page
 * @property boolean $is_main
 */
class PageCategoryRef extends BaseModel
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return StoreProductCategoryRef the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PageCategoryRef';
	}

}