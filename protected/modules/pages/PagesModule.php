<?php

class PagesModule extends BaseModule 
{
	public $moduleName = 'pages';

	public function init()
	{
		$this->setImport(array(
			'application.modules.pages.models.*',
			'application.modules.pages.models.PageTypeAttribute',
			'application.modules.pages.models.PageCategoryRef',
            'application.modules.pages.components.*'
		));
	}
}