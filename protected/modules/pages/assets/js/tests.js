function delQuestion(el) {

    $.post('/removeFaq/' + $(el).parents('.test_row').find('#PageFaq_id').val(), function () {
        $(el).parents('.test_row').replaceWith('');
    });
}

    function submitFaq(el,url){





        $.ajax({
            url:url,
            type:'POST',
            data:{
                'PageFaq[question]':$(el).parents('.test_row').find('#PageFaq_question').val(),
                'PageFaq[answer]':$(el).parents('.test_row').find('#PageFaq_answer').val(),
                'PageFaq[category]':$(el).parents('.test_row').find('#PageFaq_category').val(),
                'PageFaq[page_id]':$(el).parents('.test_row').find('#PageFaq_page_id').val(),
                'PageFaq[id]':$(el).parents('.test_row').find('#PageFaq_id').val()
            },
            dataType:'json',

            success:function(data){
                console.log(data);

                var row=$(el.parentElement.outerHTML);

                row.find('#PageFaq_id').val(data.id);
                var index=row.find('#PageFaq_id').index()+1;

                var test= _.template(window.emptyFaq);
                var content=$('#test_content');
                var html=$(test({id:row.find('#PageFaq_id'),type:''}));

                html.find('.edit').each(function(i,el){
                    var editor = CKEDITOR.replace(el);
                    editor.mode='source';
                    editor.on( 'blur', function( e ) {

                        editor.updateElement();

                    });
                    editor.on( 'change', function( e ) {

                        editor.updateElement();

                    });
                })

                var category=content.find('.test_row').last().find('#PageFaq_category').val();

                if(category.length>0){
                    content.append(html)


                }



            }
        });

    }



