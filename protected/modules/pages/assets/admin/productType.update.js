
// On form submit select all options
$("#PageTypeForm").submit(function(){
    $("#attributes option").attr('selected', 'selected');
});

// Connect lists
$("#allAttributes").delegate('option', 'click', function(){
    var clon = $(this).clone();
    $(this).remove();
    $(clon).appendTo($("#attributes"));
});
$("#attributes").delegate('option', 'click', function(){
    var clon = $(this).clone();
    $(this).remove();
    $(clon).appendTo($("#allAttributes"));
});

// Process checked categories
$("#PageTypeForm").submit(function(){
    var checked = $("#PageTypeCategoryTree li.jstree-checked");
    checked.each(function(i, el){
        var cleanId = $(el).attr("id").replace('PageTypeCategoryTreeNode_', '');
        $("#PageTypeForm").append('<input type="hidden" name="categories[]" value="' + cleanId + '" />');
    });
});

// Process main category
$('#PageTypeCategoryTree').delegate("a", "click", function (event) {
    $('#PageTypeCategoryTree').jstree('checkbox').check_node($(this));
    var id = $(this).parent("li").attr('id').replace('PageTypeCategoryTreeNode_', '');
    $('#main_category').val(id);
});

// Check node
;(function($) {
    $.fn.checkNode = function(id) {
        $(this).bind('loaded.jstree', function () {
            $(this).jstree('checkbox').check_node('#PageTypeCategoryTreeNode_' + id);
        });
    };
})(jQuery);
