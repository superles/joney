
// Process checked categories
$("#PageUpdateForm").submit(function(){
	var checked = $("#PageCategoryTree li.jstree-checked");
	checked.each(function(i, el){
		var cleanId = $(el).attr("id").replace('PageCategoryTreeNode_', '');
		$("#PageUpdateForm").append('<input type="hidden" name="categories[]" value="' + cleanId + '" />');
	});
});

$('#PageCategoryTree').delegate("a", "click", function (event) {
	$('#PageCategoryTree').jstree('checkbox').check_node($(this));
	var id = $(this).parent("li").attr('id').replace('PageCategoryTreeNode_', '');
});

// Check node
;(function($) {
	$.fn.checkNode = function(id) {
		$(this).bind('loaded.jstree', function () {
			$(this).jstree('checkbox').check_node('#PageCategoryTreeNode_' + id);
		});
	};
})(jQuery);

// On change `use configurations` select - load available attributes
$('#Page_use_configurations, #Page_type_id').change(function(){
	var attrs_block = $('#availableAttributes');
	var type_id = $('#Page_type_id').val();
	attrs_block.html('');

	if($('#Page_use_configurations').val() == '0') return;

	$.getJSON('/admin/pages/default/loadConfigurableOptions/?type_id='+type_id, function(data){
		var items = [];

		$.each(data, function(key, option) {
			items.push('<li style="clear: both;"><label><input type="checkbox" name="Page[configurable_attributes][]" value="' + option.id + '" name=""> ' + option.title + '</label></li>');
		});

		$('<ul/>', {
			'class': 'rowInput',
			html: items.join('')
		}).appendTo(attrs_block);
	});
});