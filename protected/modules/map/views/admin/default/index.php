<?php
/**
 * @var CActiveDataProvider $dataProvider
 */
$dataProvider->setPagination(array('pageSize'=>500));
	/** Display maps list **/
	$this->pageHeader = Yii::t('MapModule.core', 'Карты');

	$this->breadcrumbs = array(
		'Home'=>$this->createUrl('/admin'),
		Yii::t('MapModule.core', 'Карты'),
	);

	$this->topButtons = $this->widget('application.modules.admin.widgets.SAdminTopButtons', array(
		'template'=>array('create'),
		'elements'=>array(
			'create'=>array(
				'link'=>$this->createUrl('create'),
				'title'=>Yii::t('MapModule.core', 'Создать карту'),
				'options'=>array(
					'icons'=>array('primary'=>'ui-icon-plus')
				)
			),
		),
	));

	$this->widget('ext.sgridview.SGridView', array(
		'dataProvider'=>$dataProvider,
		'id'=>'mapsListGrid',
		'afterAjaxUpdate'=>"function(){registerFilterDatePickers()}",
		'filter'=>$model,
		'columns'=>array(
			array(
				'class'=>'CCheckBoxColumn',
			),
			array(
				'class'=>'SGridIdColumn',
				'name'=>'id',
			),
			'name',
			'lat_lang',
            'html',
			'category',
			array(
				'class'=>'CButtonColumn',
				'template'=>'{update}{delete}',
			),
		),
	));

