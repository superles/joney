<?php
// Page create/edit view
/**
 * @var Map $model
 */

$this->topButtons = $this->widget('application.modules.admin.widgets.SAdminTopButtons', array(
    'form'=>$form,
    'langSwitcher'=>!$model->isNewRecord,
    'deleteAction'=>$this->createUrl('/map/admin/default/delete', array('id'=>$model->id))
));

$title = ($model->isNewRecord) ? Yii::t('MapModule.admin', 'Создание карты') :
    Yii::t('MapModule.admin', 'Редактирование карты');

$this->breadcrumbs = array(
    'Home'=>$this->createUrl('/admin'),
    Yii::t('MapModule.admin', 'Страницы')=>$this->createUrl('index'),
    ($model->isNewRecord) ? Yii::t('MapModule.admin', 'Создание карты') : CHtml::encode($model->name),
);

$this->widget('application.modules.admin.widgets.schosen.SChosen', array(
    'elements'=>array('Page_category_id')
));

$this->pageHeader = $title;
?>


<?php

clientScript()->registerScriptFile('//maps.google.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&sensor=false',CClientScript::POS_BEGIN);

clientScript()->registerScriptFile('//google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js',CClientScript::POS_BEGIN);

?>
<?php clientScript()->registerScript('',<<<JS





var geocoder = new google.maps.Geocoder();

    function initialize() {
       var center = new google.maps.LatLng(62.021528,93.136597);

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow({
        content: ''
    });


     google.maps.event.addListener(map, "rightclick", function(event) {
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();

            // populate yor box/field with lat, lng
            $('#Map_lat_lang').val(lat + "," + lng);
            var marker = add_marker(lat,lng,$('#Map_name').val(),$('#Map_html').val());
            var myLatlng=new google.maps.LatLng(lat, lng);
            geocoder.geocode({'latLng': myLatlng }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                $('#Map_name').val(results[0].formatted_address);
                 $('#autocomplete').val(results[0].formatted_address);
                 map.setCenter(myLatlng);
                }
                }
                });
          });

        if($('#Map_lat_lang').val().length>0){
            var latlang=$('#Map_lat_lang').val().split(',');

            var newCenter = new google.maps.LatLng(latlang[0],latlang[1]);

            add_marker(latlang[0],latlang[1],$('#Map_name').val(),$('#Map_html').val());

            map.setCenter(newCenter);
        }

      }
      function add_marker(lat, lng, title, box_html) {

    var infowindow = new google.maps.InfoWindow({
        content: box_html
    });

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        title: title
    });
    var latLng = marker.getPosition();
    map.setCenter(latLng);
    map.setZoom(14);
  google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
   google.maps.event.addListener(marker, 'dblclick', function () {
        del_marker(marker);
    });

    return marker;
}
        function del_marker(marker){
        marker.setMap(null);
        $('#Map_lat_lang').val('');
      }


      google.maps.event.addDomListener(window, 'load', initialize);









JS
    ,CClientScript::POS_END
) ?>
<?php clientScript()->registerScript('initializeForm',<<<JS
    initializeForm();
JS
    ,CClientScript::POS_READY
) ?>
<?php clientScript()->registerScript('initializeLoad',<<<JS

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',

};
function initializeForm() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    $('#'+component).val('');
     $('#'+component).attr('disabled',false);
  }
    var city='',address=[];
  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {

      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
      if(addressType=='locality'){
        city=val;
      }
      if(addressType=='route'){
        address[0]=val;
      }
      if(addressType=='street_number'){
        address[1]=val;
      }
    }
  }
     document.getElementById('Map_city').value=city;
  document.getElementById('Map_address').value=address.join(' ');
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}

function findByAddress(){
    var url='http://maps.googleapis.com/maps/api/geocode/json?address=';
    var i=0;
    var text='';
     var city='',address=[];
    $.each(componentForm,function(el){
        if(i>0){
            url+='+';
            text+=' ';
        }
        text+=$('#'+el).val();
        url+=$('#'+el).val();
        i++;
         if(el=='locality'){
        city=$('#'+el).val();
      }
        if(el=='route'){
        address[0]=$('#'+el).val();
      }
      if(el=='street_number'){
        address[1]=$('#'+el).val();
      }
    });
    document.getElementById('Map_city').value=city;
  document.getElementById('Map_address').value=address.join(' ');
    $('#Map_name').val(text);
    url+='&sensor=false';
    $.getJSON(url,function(json){

        var lat = json.results[0].geometry.location.lat;
        var lng = json.results[0].geometry.location.lng;

        if(typeof lat != 'undefined'&&typeof lng != 'undefined'){

            $('#Map_lat_lang').val(lat + "," + lng);
            add_marker(lat,lng,$('#Map_name').val(),$('#Map_html').val());

        }else{
            alert('Адрес не найден');
        }


    });



    return false;
}
JS
    ,CClientScript::POS_HEAD
) ?>
<!-- Use padding-all class with SidebarAdminTabs -->
<div class="form wide padding-all">
    <?php echo $form->asTabs(); ?>
</div>

<style>
    #map-container {
        padding: 6px;
        border-width: 1px;
        border-style: solid;
        border-color: #ccc #ccc #999 #ccc;
        -webkit-box-shadow: rgba(64, 64, 64, 0.5) 0 2px 5px;
        -moz-box-shadow: rgba(64, 64, 64, 0.5) 0 2px 5px;
        box-shadow: rgba(64, 64, 64, 0.1) 0 2px 5px;
        width: 100%;
    }

    #map {
        width: 100%;
        height: 600px;
    }

</style>
<style>
    #locationField, #controls {
        position: relative;
        width: 480px;
    }
    #autocomplete {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 99%;
    }
    .label {
        text-align: right;
        font-weight: bold;
        width: 100px;
        color: #303030;
    }
    #address {
        border: 1px solid #000090;
        background-color: #f0f0ff;
        width: 480px;
        padding-right: 2px;
    }
    #address td {
        font-size: 10pt;
    }
    .field {
        width: 99%;
    }
    .slimField {
        width: 80px;
    }
    .wideField {
        width: 200px;
    }
    #locationField {
        height: 20px;
        margin-bottom: 20px;
    }
</style>
<div class="form" style="padding-left: 180px">

    <div id="locationField">
        <input id="autocomplete" placeholder="Enter your address"
               onFocus="geolocate()" type="text"></input>
    </div>

    <table id="address">
        <tr>
            <td class="label">Дом/Улица</td>
            <td class="slimField"><input class="field" id="street_number"
                                         disabled="true"></input></td>
            <td class="wideField" colspan="2"><input class="field" id="route"
                                                     disabled="true"></input></td>
        </tr>
        <tr>
            <td class="label">Город</td>
            <td class="wideField" colspan="3"><input class="field" id="locality"
                                                     disabled="true"></input></td>
        </tr>
        <tr>
            <td class="label">Область</td>
            <td class="wideField"><input class="field"
                                         id="administrative_area_level_1" disabled="true"></input></td>

        </tr>
        <tr>
            <td class="label">Страна</td>
            <td class="wideField" colspan="3"><input class="field"
                                                     id="country" disabled="true"></input></td>
        </tr>
    </table>
    <input type="button" value="Найти" onclick="findByAddress()"/>

</div>


<div id="map-container"><div id="map"></div></div>


