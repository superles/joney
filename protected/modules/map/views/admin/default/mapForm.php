<?php
/**
 * @var Page $model
 */
/*** Create/update page form ***/

return array(
	'id'=>'mapUpdateForm',
	'showErrorSummary'=>true,
    'attributes' => array(
        'enctype' => 'multipart/form-data',
        'id'=>'mapUpdateForm',

    ),
	'elements'=>array(
		'content'=>array(
			'type'=>'form',
			'title'=>Yii::t('MapModule.core', 'Содержимое'),
			'elements'=>array(
				'name'=>array(
					'type'=>'text',
				),
				'category'=>array(
					'type'=>'text',
				),
                'city'=>array(
					'type'=>'text',
				),
                'address'=>array(
					'type'=>'text',
				),
                'html'=>array(
                    'type'=>'SRichTextarea',
                ),

                'lat_lang'=>array(
                    'type'=>'text',
                    'hint'=>'Два варианта выбора места на карте: <br/><br/>
1) Правой клавишей <br/><br/>
2) Поиск по адресу (заполнить все поля) <br/><br/>
Для удаления маркера кликнуть два раза по нему
<br/><br/>
Для проверки всплывающего окна кликнуть один раз'
                ),



			),

        )
    )
);

