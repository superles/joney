<?php

/**
 * @var $this SSystemMenu
 */

Yii::import('map.MapModule');

/**
 * Admin menu items for pages module
 */
return array(
	array(
		'label'    => Yii::t('MapModule.core', 'Карты'),
		'url'      => array('/map/admin/default/index'),
		'position' => 50,

	),
);