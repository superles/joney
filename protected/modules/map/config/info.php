<?php

Yii::import('application.modules.map.MapModule');

/**
 * Comments module info
 */ 
return array(
	'name'=>Yii::t('MapModule.core', 'Map'),
	'author'=>'firstrow@gmail.com',
	'version'=>'0.1',
	'description'=>Yii::t('MapModule.core', 'Позволяет оставлять комментарии к продуктам, страницам.'),
	'url'=>''
);