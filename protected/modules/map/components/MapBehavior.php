<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20.03.15
 * Time: 5:42
 */

class MapBehavior extends CActiveRecordBehavior{

    public $lat;

    public $lang;

    public function getMap(){
        Yii::import('application.modules.map.*');
        Yii::import('application.modules.map.models.*');
        Yii::import('application.modules.map.components.*');
        return LatLng::model()->findByAttributes(
            array(
                'class_name'=>get_class($this->owner),
                'object_id'=>$this->owner->id
            )
                );
    }

    public function beforeSave()
    {


        return true;

    }


}