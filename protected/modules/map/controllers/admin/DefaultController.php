<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20.03.15
 * Time: 5:37
 */

class DefaultController extends SAdminController {

    
    public function actionIndex()
	{
        $model = new Map('search');

        $this->render('index',array(
                'model'=>$model,
            'dataProvider'=>$model->search())
        );
	}


    public function actionCreate()
    {
        $this->actionUpdate(true);
    }

    /**
     * Create or update new map
     * @param boolean $new
     */
    public function actionUpdate($new = false)
    {
        if ($new === true)
        {
            $model = new Map;

        }
        else
        {
            $model = Map::model()->findByPk($_GET['id']);
        }

        if (!$model)
            throw new CHttpException(404, Yii::t('MapModule.core', 'Страница не найдена.'));

        $form = new STabbedForm('application.modules.map.views.admin.default.mapForm', $model);

        if (Yii::app()->request->isPostRequest)
        {
            $model->attributes = $_POST['Map'];




            if ($model->validate())
            {
                $model->save();

                $this->setFlashMessage(Yii::t('MapModule.core', 'Изменения успешно сохранены'));

                if (isset($_POST['REDIRECT']))
                    $this->smartRedirect($model);
                else
                    $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model'=>$model,
            'form'=>$form,
        ));
    }

    /**
     * Delete map by Pk
     */
    public function actionDelete()
    {
        if (Yii::app()->request->isPostRequest)
        {
            $model = Map::model()->findAllByPk($_REQUEST['id']);

            if (!empty($model))
            {
                foreach($model as $map)
                    $map->delete();
            }

            if (!Yii::app()->request->isAjaxRequest)
                $this->redirect('index');
        }
    }
    
}