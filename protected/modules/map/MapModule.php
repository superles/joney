<?php

class MapModule extends BaseModule
{
	/**
	 * @var string
	 */
	public $moduleName = 'map';

	/**
	 * Init module
	 */
	public function init()
	{
		$this->setImport(array(
			'map.models.Map',
		));
	}




}