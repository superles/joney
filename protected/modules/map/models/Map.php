<?php

/**
 * This is the model class for table "Map".
 *
 * The followings are the available columns in table 'Map':
 * @property integer $id
 * @property string $name
 * @property string $html
 * @property string $category
 * @property string $lat_lang
 * @property string $lat
 * @property string $long
 * @property string $city
 */
class Map extends CActiveRecord
{
    public $lat;
    public $long;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Map';
	}

    public function afterFind(){

        $tmp=explode(',',$this->lat_lang);
        $this->setAttribute('lat',$tmp[0]);
        $this->setAttribute('long',$tmp[1]);


    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lat_lang', 'required'),

			array('name, city, category, lat_lang', 'length', 'max'=>255),
			array('html,lat,long,city,address', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, html, address, city, category, lat_lang,lat,long', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public static function getCityList($assoc=true){
        $criteria=self::model()->getDbCriteria();
        $criteria->distinct=true;
        $cities=array_map(function(Map $el) use ($assoc){ return $el->city; },self::model()->findAll($criteria));

        return $assoc?array_combine($cities,$cities):$cities;
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
            'address'=>'Улица',
            'city' => 'Город',
			'html' => 'Html',
			'category' => 'Category',
			'lat_lang' => 'Lat Lang',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
        $criteria->compare('address',$this->address,true);
        $criteria->compare('city',$this->city,true);
		$criteria->compare('html',$this->html,true);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('lat_lang',$this->lat_lang,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Map the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
