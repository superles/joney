<?php

Yii::import('application.modules.Poll.PollModule');

/**
 * Module info
 */ 
return array(
	'name'=>Yii::t('PollModule.core', 'Голосования'),
	'author'=>'firstrow@gmail.com',
	'version'=>'0.1',
	'description'=>Yii::t('PollModule.core', 'Управление Голосованиями.'),
	'url'=>'', # Url to module home page.
);