<?php

Yii::import('poll.PollModule');

/**
 * Admin menu items for pages module
 */
return array(
	'Poll'=>array(
		'label'       => Yii::t('PollModule.admin', 'Голосования'),
		'url'         => array('/poll/admin'),
		'position'    => 2,
		'itemOptions' => array(
			'class'       => 'hasRedCircle circle-Poll',
		),
		'items' => array(
			array(
				'label'       => Yii::t('PollModule.admin', 'Все Голосования'),
				'url'         => array('/admin/poll'),
				'position'    => 1
			),
			array(
				'label'    => Yii::t('PollModule.admin', 'Создать Голосование'),
				'url'      => array('/admin/poll/create'),
				'position' => 2
			),

		),
	),
);