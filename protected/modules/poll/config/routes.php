<?php

return array(
    'admin/poll'=>'poll/admin/poll/admin',
    'admin/poll/<action>'=>'poll/admin/poll/<action>',
    'poll/<action>'=>'poll/poll/<action>',
    'poll/<action>/<id:\d+>'=>'poll/poll/<action>',
    'poll/pollvote/<action>'=>'poll/pollvote/<action>',
    'poll/pollchoice/<action>'=>'poll/pollchoice/<action>',

);