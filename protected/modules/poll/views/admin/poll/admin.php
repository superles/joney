<?php
/**
 * @var PollController $this
 *
 */
$this->breadcrumbs=array(
  'Polls'=>array('index'),
  'Manage',
);

$this->menu=array(
  array('label'=>'List Polls', 'url'=>array('index')),
  array('label'=>'Create Poll', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
  $('.search-form').toggle();
  return false;
});
$('.search-form form').submit(function(){
  $.fn.yiiGridView.update('poll-grid', {
    data: $(this).serialize()
  });
  return false;
});
");
?>

<?php
$this->pageHeader = Yii::t('PollModule.admin', 'Заказы');

$this->breadcrumbs = array(
    'Home'=>$this->createUrl('/admin'),
    Yii::t('PollModule.admin', 'Заказы'),
);

$this->topButtons = $this->widget('admin.widgets.SAdminTopButtons', array(
    'template'=>array('create'),
    'elements'=>array(
        'create'=>array(
            'link'=>$this->createUrl('create'),
            'title'=>Yii::t('PollModule.admin', 'Создать голосование'),
            'options'=>array(
                'icons'=>array('primary'=>'ui-icon-cart')
            )
        ),
    ),
));
?>
<?php
$this->widget('ext.sgridview.SGridView', array(
  'id'=>'poll-grid',
  'dataProvider'=>$model->search(),
  'filter'=>$model,
  'columns'=>array(
    'title',
    'description',
    array(
      'name' => 'status',
      'value' => 'CHtml::encode($data->getStatusLabel($data->status))',
      'filter' => CHtml::activeDropDownList($model, 'status', $model->statusLabels()),
    ),
    array(
      'class'=>'CButtonColumn',
    ),
  ),
)); ?>
