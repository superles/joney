<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'portlet-poll-form',
  'enableAjaxValidation'=>false,
)); ?>

  <?php echo $form->errorSummary($model); ?>

  <div class="col-xs-12" style="margin-bottom: 15px">

      <div class="row">
          <div style="padding-top: 10px">
    <?php $template = '<div class="row-choice clearfix"><div class="form-radio">{input}</div><div class="form-label">{label}</div></div>'; ?>
    <?php echo $form->radioButtonList($userVote,'choice_id',$choices,array(
      'template'=>$template,
      'separator'=>'',
      'name'=>'PortletPollVote_choice_id')); ?>
    <?php echo $form->error($userVote,'choice_id'); ?>
  </div>
  </div></div>
  <div class="col-xs-12">
      <div class="row">
    <?php echo TbHtml::submitButton('Голосовать'); ?>
          </div>
  </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
