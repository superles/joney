<?php

/**
 * Behavior for likeabe models
 */
class LikeBehavior extends CActiveRecordBehavior
{

	/**
	 * @var string model primary key attribute
	 */
	public $pk = 'id';

	/**
	 * @var string alias to class. e.g: application.store.models.StoreProduct or pages.models.Page
	 */
	public $class_name;

	/**
	 * @var string attribute name to present like owner in admin panel. e.g: name - references to Page->name
	 */
	public $owner_title;

	/**
	 * @return string pk name
	 */
	public function getObjectPkAttribute()
	{
		return $this->pk;
	}

	public function getClassName()
	{
		return $this->class_name;
	}

	public function getOwnerTitle()
	{
		$attr = $this->owner_title;
		return $this->getOwner()->$attr;
	}

	public function attach($owner)
	{
		parent::attach($owner);
	}

	/**
	 * @param CEvent $event
	 * @return mixed
	 */
	public function afterDelete($event)
	{
		Yii::import('application.modules.likes.models.Like');

		$pk = $this->getObjectPkAttribute();
		Like::model()->deleteAllByAttributes(array(
				'class_name'=>$this->getClassName(),
				'object_pk'=>$this->getOwner()->$pk
		));
		return parent::afterDelete($event);
	}

	/**
	 * @return string approved likes count for object
	 */
	public function getLikesCount()
	{
		Yii::import('application.modules.likes.models.Like');

		$pk = $this->getObjectPkAttribute();
		return Like::model()
			->approved()
			->countByAttributes(array(
			'class_name'=>$this->getClassName(),
			'object_pk'=>$this->getOwner()->$pk
		));
	}

    public function likeLink($url='/projects/addLike'){

        if(!user()->isGuest){

        $class=Like::model()->exists('object_pk='.$this->owner->id.' and class_name="application.modules.projects.models.Project" and user_id='.user()->id)?'like_disabled':'like_enabled';

        return CHtml::link(Yii::t('ProjectsModule.main','<span class="like_count">{count}</span> <span class="heart"></span>',array(
            '{count}'=>$this->getLikesCount())),"$url/{$this->owner->id}",

            array('class'=>'like '.$class,'onclick'=>'addLike(this); return false;'));


    }
    }

}
