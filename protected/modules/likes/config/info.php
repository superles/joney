<?php

Yii::import('application.modules.likes.LikesModule');

/**
 * Likes module info
 */ 
return array(
	'name'=>Yii::t('LikesModule.core', 'Нравится'),
	'author'=>'firstrow@gmail.com',
	'version'=>'0.1',
	'description'=>Yii::t('LikesModule.core', 'Позволяет оставлять likes к продуктам, страницам.'),
	'url'=>''
);