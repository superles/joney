<?php

/**
 * Routes for likes module
 */
return array(
	'/admin/likes'=>'/likes/admin/likes',
	'/admin/likes/<action>'=>'/likes/admin/likes/<action>',
);