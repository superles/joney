<?php

/**
 * @var $this SSystemMenu
 */

Yii::import('likes.LikesModule');

/**
 * Admin menu items for pages module
 */
return array(
    'social'=>array(
        'items'=>array(
            array(
                'label'    => Yii::t('LikesModule.core', 'Нравится'),
                'url'      => array('/likes/admin/index'),
                'position' => 61,
                'itemOptions' => array(
                    'class'       => 'hasRedCircle circle-likes',
                ),
            ),

        ),
    ),

);