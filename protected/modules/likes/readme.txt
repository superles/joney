Usage:

1. Connect behavior to AR model
    'likes' => array(
        'class'       => 'likes.components.LikeBehavior',
        'class_name'  => 'store.models.StoreProduct', // Alias to likeable model
        'owner_title' => 'name', // Attribute name to present like owner in admin panel
    )
2. Update view where to enable likes
    ...
    $this->renderPartial('likes.views.like.create', array(
        'model'=>$model, // Likeable model instance
    ));
    ...
3. Add captcha action
    ...
    public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
            ),
        );
    }
    ...