<?php

/**
 * Create/update like
 *
 * @var $model Like
 */

$this->topButtons = $this->widget('admin.widgets.SAdminTopButtons', array(
	'form'         => $form,
	'deleteAction' => $this->createUrl('/likes/admin/likes/delete', array('id'=>$model->id)),
	'template'     => array('history_back','save','delete')
));

$title = Yii::t('LikesModule.admin', 'Редактирование комментария');

$this->breadcrumbs = array(
	'Home'=>$this->createUrl('/admin'),
	Yii::t('LikesModule.admin', 'Нравится')=>$this->createUrl('index'),
	CHtml::encode($model->email),
);

$this->pageHeader = $title;

?>

<div class="form wide padding-all">
	<?php echo $form; ?>
</div>