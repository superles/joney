<?php

/**
 * Display likes list
 *
 * @var $model Like
 **/

Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/admin/likes.index.js');

$this->pageHeader = Yii::t('LikesModule.core', 'Нравится');

$this->breadcrumbs = array(
	'Home'=>$this->createUrl('/admin'),
	Yii::t('LikesModule.core', 'Нравится'),
);

$this->widget('ext.sgridview.SGridView', array(
	'dataProvider' => $dataProvider,
	'id'           => 'likesListGrid',
	'filter'       => $model,
	'customActions'=>array(
		array(
			'label'=>Yii::t('LikesModule.core', 'Подтвержден'),
			'url'=>'#',
			'linkOptions'=>array(
				'onClick'=>'return setLikesStatus(1, this);',
			)
		),
		array(
			'label'=>Yii::t('LikesModule.core', 'Ждет одобрения'),
			'url'=>'#',
			'linkOptions'=>array(
				'onClick'=>'return setLikesStatus(0, this);',
			)
		),
		array(
			'label'=>Yii::t('LikesModule.core', 'Спам'),
			'url'=>'#',
			'linkOptions'=>array(
				'onClick'=>'return setLikesStatus(2, this);',
			)
		),
	),
	'columns' => array(
		array(
			'class'=>'CCheckBoxColumn',
		),
		array(
			'class'=>'SGridIdColumn',
			'name'=>'id',
		),
		array(
			'name'  => 'name',
			'type'  => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->name), array("update", "id"=>$data->id))',
		),
		array(
			'name'=>'email',
		),
		array(
			'name'=>'text',
			'value'=>'Like::truncate($data, 100)'
		),
		array(
			'name'=>'status',
			'filter'=>Like::getStatuses(),
			'value'=>'$data->statusTitle',
		),
		array(
			'name'=>'owner_title',
			'filter'=>false
		),
		'ip_address',
		array(
			'name'=>'created',
		),
		// Buttons
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
));