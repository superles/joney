<?php
/**
 * @var $this Controller
 * @var $form CActiveForm
 */

// Load module
$module = Yii::app()->getModule('likes');
// Validate and save like on post request
$like = $module->processRequest($model);
// Load model likes
$likes = Like::getObjectLikes($model);

$currentUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

// Display likes
if(!empty($likes))
{
	foreach($likes as $row)
	{
	?>
		<div class="like" id="like_<?php echo $row->id; ?>">
			<span class="username"><?php echo CHtml::encode($row->name); ?></span> <span class="created">(<?php echo $row->created; ?>)</span>
			<?php echo CHtml::link('#', Yii::app()->request->getUrl().'#like_'.$row->id) ?>
			<div class="message">
				<?php echo nl2br(CHtml::encode($row->text)); ?>
			</div>
			<hr>
		</div>
	<?php
	}
}
?>

<div class="leave_like" id="leave_like">
	<h3><?php echo Yii::t('LikesModule.core', 'Оставить отзыв') ?></h3>
	<div class="form wide ">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'                     =>'like-create-form',
		'action'                 =>$currentUrl.'#like-create-form',
		'enableAjaxValidation'   =>false,
		'enableClientValidation' =>true,
	)); ?>

	<?php if(Yii::app()->user->isGuest): ?>
		<div class="row">
			<?php echo $form->labelEx($like,'name'); ?>
			<?php echo $form->textField($like,'name'); ?>
			<?php echo $form->error($like,'name'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($like,'email'); ?>
			<?php echo $form->textField($like,'email'); ?>
			<?php echo $form->error($like,'email'); ?>
		</div>
	<?php endif; ?>

		<div class="row">
			<?php echo $form->labelEx($like,'text'); ?>
			<?php echo $form->textArea($like,'text', array('rows'=>5)); ?>
			<?php echo $form->error($like,'text'); ?>
		</div>

		<?php if(Yii::app()->user->isGuest): ?>
		<div class="row">
			<?php echo CHtml::activeLabelEx($like, 'verifyCode')?>
			<?php $this->widget('CCaptcha', array(
				'clickableImage'=>true,
				'showRefreshButton'=>false,
			)) ?>
			<br/>
			<label>&nbsp;</label>
			<?php echo CHtml::activeTextField($like, 'verifyCode')?>
			<?php echo $form->error($like,'verifyCode'); ?>
		</div>
		<?php endif ?>

		<div class="row buttons">
			<?php echo CHtml::submitButton(Yii::t('LikesModule.core', 'Отправить')); ?>
		</div>

	<?php $this->endWidget(); ?><!-- /form -->
	</div>
</div>