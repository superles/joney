<?php

class LikesModule extends BaseModule
{
	/**
	 * @var string
	 */
	public $moduleName = 'likes';

	/**
	 * Init module
	 */
	public function init()
	{
		$this->setImport(array(
			'likes.models.Like',
		));
	}

	/**
	 * @param $model
	 * @return Like
	 */
	public function processRequest($model)
	{
		$like = new Like;
		if(Yii::app()->request->isAjaxRequest)
		{
			$like->attributes = Yii::app()->request->getPost('Like');

			if(!Yii::app()->user->isGuest)
			{
				$like->name = Yii::app()->user->name;
				$like->email = Yii::app()->user->email;
			}

			if($like->validate())
			{
				$pkAttr = $model->getObjectPkAttribute();
				$like->class_name = $model->getClassName();
				$like->object_pk = $model->$pkAttr;
				$like->user_id = Yii::app()->user->isGuest ? 0 : Yii::app()->user->id;
                if(!Like::model()->exists('object_pk='.$like->object_pk.' and class_name="'.$like->class_name.'" and user_id='.user()->id)){
                    if($like->isNewRecord){

                        if(Yii::app()->params['LikeNeedApproval']){
                            $like->status=Like::STATUS_WAITING;
                        }else{
                            $like->status=Like::STATUS_APPROVED;
                        }

                    }

                    $like->save();
                }else{
                    $like=Like::model()->find('object_pk='.$like->object_pk.' and class_name="'.$like->class_name.'" and user_id='.user()->id);
                    return $like->delete();
                }


				$url = Yii::app()->getRequest()->getUrl();

				if($like->status==Like::STATUS_WAITING)
				{
					$url.='#';
					Yii::app()->user->setFlash('messages', Yii::t('LikesModule.core', 'Ваш комментарий успешно добавлен. Он будет опубликован после проверки администратором.'));
				}
				elseif($like->status==Like::STATUS_APPROVED)
					$url.='#like_'.$like->id;

				// Refresh page

			}
		}
		return $like;
	}

}