<?php

/**
 * Admin site likes
 */
class LikesController extends SAdminController
{

	/**
	 * Display all site likes
	 */
	public function actionIndex()
	{
		$model = new Like('search');

		if(!empty($_GET['Like']))
			$model->attributes = $_GET['Like'];

		$dataProvider = $model->search();
		$dataProvider->pagination->pageSize = Yii::app()->settings->get('core', 'productsPerPageAdmin');

		$this->render('index', array(
			'model'=>$model,
			'dataProvider'=>$dataProvider
		));
	}

	/**
	 * Update like
	 * @param $id
	 * @throws CHttpException
	 */
	public function actionUpdate($id)
	{
		$model = Like::model()->findByPk($id);

		if(!$model)
			throw new CHttpException(404, Yii::t('LikesModule.admin', 'Комментарий не найден'));

		$form = new CForm('likes.views.admin.likes.likeForm', $model);

		if (Yii::app()->request->isPostRequest)
		{
			$model->attributes = $_POST['Like'];
			if($model->validate())
			{
				$model->save();

				$this->setFlashMessage(Yii::t('LikesModule.admin', 'Изменения успешно сохранены'));

				if (isset($_POST['REDIRECT']))
					$this->smartRedirect($model);
				else
					$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'model' => $model,
			'form'  => $form
		));
	}

	public function actionUpdateStatus()
	{
		$ids    = Yii::app()->request->getPost('ids');
		$status = Yii::app()->request->getPost('status');
		$models = Like::model()->findAllByPk($ids);

		if(!array_key_exists($status, Like::getStatuses()))
			throw new CHttpException(404, Yii::t('LikesModule.admin', 'Ошибка проверки статуса.'));

		if(!empty($models))
		{
			foreach ($models as $like)
			{
				$like->status = $status;
				$like->save();
			}
		}

		echo Yii::t('LikesModule', 'Статус успешно изменен');
	}

	/**
	 * Delete likes
	 * @param array $id
	 */
	public function actionDelete($id = array())
	{
		if (Yii::app()->request->isPostRequest)
		{
			$model = Like::model()->findAllByPk($_REQUEST['id']);

			if (!empty($model))
			{
				foreach($model as $m)
					$m->delete();
			}

			if (!Yii::app()->request->isAjaxRequest)
				$this->redirect('index');
		}
	}

}
