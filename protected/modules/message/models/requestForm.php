<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04.11.14
 * Time: 13:32
 */
return array(
    'id' => 'partnershipForm',
    'showErrorSummary' => true,
    'class' => 'form form-horizontal',
    'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
    'enctype' => 'multipart/form-data',
    'style' => 'padding-top:20px',
    'action'=>'/admin/girls/default/create',
    'elements' => array(
        'request' => array(
            'type' => 'form',

            'title' => '',
            'class' => 'user',
            'elements' => array(
                'id'=>array('type'=>'hidden'),
                'login'=>array('type'=>'hidden'),
                'email'=>array('type'=>'hidden'),
                'name'=>array('type'=>'hidden'),
                'phone'=>array('type'=>'hidden'),
                'address'=>array('type'=>'hidden'),
                'country'=>array('type'=>'hidden'),
                'city'=>array('type'=>'hidden'),
                'date_registration'=>array('type'=>'hidden'),
                'director_name'=>array('type'=>'hidden'),
                'num_girls'=>array('type'=>'hidden'),
                'num_webcams'=>array('type'=>'hidden'),
                'num_comps'=>array('type'=>'hidden'),
                'num_employees'=>array('type'=>'hidden'),
                'girls_links'=>array('type'=>'hidden'),
                'partner_links'=>array('type'=>'hidden'),
            ),
        ),
    )
);
