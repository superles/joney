<?php

Yii::import('application.modules.message.MessageModule');

/**
 * Message module info
 */ 
return array(
	'name'=>Yii::t('MessageModule.core', 'Сообщения'),
	'author'=>'admin@u3soft.com',
	'version'=>'0.1',
	'description'=>Yii::t('MessageModule.core', ''),
	'url'=>''
);