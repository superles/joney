<?php

/**
 * @var $this SSystemMenu
 */

Yii::import('message.MessageModule');

/**
 * Admin menu items for pages module
 */
return array(
	array(
		'label'    => Yii::t('MessageModule.core', 'Сообщения'),
		'url'      => array('/message/admin/index'),
		'position' => 15,
		'itemOptions' => array(
			'class'       => 'message',
		),
	),

);