<?php

/**
 * Routes for message module
 */
return array(

    '/message'=>'/message/default',
    '/message/inbox'=>'/message/inbox/inbox',

    '/message/sent'=>'/message/sent/sent',
    '/message/compose'=>'/message/compose/compose',
    '/message/suggest'=>'/message/suggest/user',
    '/message/view'=>'/message/view/view',
    '/message/view/<id:\d+>'=>'/message/view/view',
    '/message/delete'=>'/message/delete/delete',
    '/message/suggest/aclist'=>'/message/suggest/aclist',
    '/photomanager/connector' => '/message/photoManager/index',
    '/admin/message/ajax/<id:\d+>'=>'/message/admin/default/ajaxform',
    '/admin/message/viewRequest'=>'/message/admin/default/viewRequest',
    '/admin/message'=>'/message/admin/default',
    '/admin/message/<action>'=>'/message/admin/default/<action>',
    '/admin/message/partnership/<action>'=>'/message/admin/partnership/<action>',

    '/message/last'=>'/message/view/last',

);