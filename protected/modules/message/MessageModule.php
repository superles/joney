<?php

class MessageModule extends BaseModule
{
	public $defaultController = 'inbox';
    public $moduleName = 'message';
	public $userModel = 'User';
	public $userModelRelation = null;
	public $getNameMethod='getName';
	public $getSuggestMethod='getSuggest';

	public $senderRelation;
	public $receiverRelation;

	public $dateFormat = 'Y-m-d H:i:s';

	public $inboxUrl = array("/message/inbox");

	public $viewPath = '/message/default';

	public function init()
	{
		if (!class_exists($this->userModel)) {
		    throw new Exception(MessageModule::t("Class {userModel} not defined", array('{userModel}' => $this->userModel)));
		}

		foreach (array('getNameMethod', 'getSuggestMethod') as $methodName) {
			if (!$this->$methodName) {
				throw new Exception(MessageModule::t("Property MessageModule::{methodName} not defined", array('{methodName}' => $methodName)));
			}

			if (!method_exists($this->userModel, $this->$methodName)) {
				throw new Exception(MessageModule::t("Method {userModel}::{methodName} not defined", array('{userModel}' => $this->userModel, '{methodName}' => $this->$methodName)));
			}
		}

		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'message.models.*',
			'message.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{



		if (Yii::app()->user->isGuest) {
            $controller->redirect($controller->createUrl('/'));
		} else if (parent::beforeControllerAction($controller, $action)) {
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		} else {
			return false;
		}
	}

	public static function t($str='',$params=array(),$dic='message') {
		return Yii::t("MessageModule.".$dic, $str, $params);
	}

	public function getCountUnreadedMessages($userId=false) {
		return Message::model()->getCountUnreaded($userId?:user()->id);
	}

    public static function getInboxMessages($userId) {
        $c=new CDbCriteria();
        $c->compare('receiver_id',$userId);
        $c->order='created_at DESC';
        $data=new CActiveDataProvider('Message',array(
            'criteria'=>$c

        ));
        return $data;
    }

    /**
     * @param bool $userId
     * @param bool $last
     * @return Message|Message[]
     */
    public static function getUnreadMessages($userId=false,$last=true) {
        if(!$userId){
            $userId=user()->id;
        }
        $c = new CDbCriteria();
        $c->addCondition('receiver_id = :receiverId');
        $c->addCondition('deleted_by <> :deleted_by_receiver OR deleted_by IS NULL');
        $c->addCondition('is_read = "0"');
        $c->params = array(
            'receiverId' => $userId,
            'deleted_by_receiver' => Message::DELETED_BY_RECEIVER,
        );

        $c->order='created_at DESC';

        return $last?Message::model()->find($c):Message::model()->findAll($c);
    }
    public static function getSentMessages($userId) {
        $c=new CDbCriteria();
        $c->compare('sender_id',$userId);
        $c->order='created_at DESC';
        $data=new CActiveDataProvider('Message',array(
            'criteria'=>$c

        ));
        return $data;
    }

    public function getBadge($user_id=false){
        $class='default';
        if(!$user_id){
            $count=$this->getCountUnreadedMessages(user()->id);
        }else{
            $count=$this->getCountUnreadedMessages($user_id);
        }

        if($count!='0'){
            $class='info';
        }
        return '<li ><a href="/message/inbox" class="fs12">Входящие <span id="mail_badge" class="mail_badge pull-right badge-'.$class.' badge">'.$count.'
                    </span></a></li>';
    }
    public function getMiniBadge($user_id=false){
        $class='info';
        if(!$user_id){
            $count=$this->getCountUnreadedMessages(user()->id);
        }else{
            $count=$this->getCountUnreadedMessages($user_id);
        }

        if($count!='0'){
            $class='info';
        }
        return '<span id="mail_badge" class="mail_badge pull-right ml10 mt1 mini badge-'.$class.' badge">'.$count.'
                    </span>';
    }

}
