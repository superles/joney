<?php

/**
 * Admin site chat
 */
class DefaultController extends SAdminController
{
    public function filters(){
        return CMap::mergeArray(parent::filters(),array(
            "ajaxOnly + ajaxform"
        ));
    }

    public function actionAjaxform($id)
    {
        //$id=app()->request->getParam('id',false);
        $message=Message::model()->findByPk($id);
        if($message&&$message->type==10){
            if(!$id){
                $form=new PartnershipForm();
            }else{
                $form=new PartnershipForm();
                $form->attributes=CJSON::decode($message->subdata);

            }
            echo $this->renderPartial('_partnership',array('model'=>$form),true,true);
        }
    }

    public function actionRequests()
    {
        $model = new Messages('search');
        $model->unsetAttributes();
        if(!empty($_GET['Message']))
            $model->attributes = $_GET['Message'];
        $c= $model->with(array('sender','receiver'))->search()->criteria;
        /**
         * @var CDbCriteria $c
         */

        $c->order='created_at DESC';
        $dataProvider = new CActiveDataProvider('Message',array('criteria'=>$c));

        $d=$dataProvider->getData();

        $dataProvider->pagination->pageSize = Yii::app()->settings->get('core', 'productsPerPageAdmin');



        $this->render('requests', array(
            'model'=>$model,
            'dataProvider'=>$dataProvider
        ));
    }

	/**
	 * Display all site chat
	 */
	public function actionIndex()
	{
		$model = new Messages('search');
        $model->unsetAttributes();
		if(!empty($_GET['Message']))
			$model->attributes = $_GET['Message'];
        $c= $model->with(array('sender','receiver'))->search()->criteria;
        /**
         * @var CDbCriteria $c
         */

        $c->order='created_at DESC';
		$dataProvider = new CActiveDataProvider('Message',array('criteria'=>$c));

		$dataProvider->pagination->pageSize = Yii::app()->settings->get('core', 'productsPerPageAdmin');

		$this->render('index', array(
			'model'=>$model,
			'dataProvider'=>$dataProvider
		));
	}

	/**
	 * Update comment
	 * @param $id
	 * @throws CHttpException
	 */
	public function actionUpdate($id)
	{
		$model = Comment::model()->findByPk($id);

		if(!$model)
			throw new CHttpException(404, Yii::t('MessageModule.admin', 'Комментарий не найден'));

		$form = new CForm('chat.views.admin.chat.commentForm', $model);

		if (Yii::app()->request->isPostRequest)
		{
			$model->attributes = $_POST['Comment'];
			if($model->validate())
			{
				$model->save();

				$this->setFlashMessage(Yii::t('MessageModule.admin', 'Изменения успешно сохранены'));

				if (isset($_POST['REDIRECT']))
					$this->smartRedirect($model);
				else
					$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'model' => $model,
			'form'  => $form
		));
	}
    public function actionCreate()
	{
		$model = new Messages();

		if(!$model)
			throw new CHttpException(404, Yii::t('MessageModule.admin', 'Комментарий не найден'));



		if (Yii::app()->request->isPostRequest)
		{
			$model->attributes = $_POST['Message'];
			if($model->validate())
			{
				$model->save();

				$this->setFlashMessage(Yii::t('MessageModule.admin', 'Изменения успешно сохранены'));

				if (isset($_POST['REDIRECT']))
					$this->smartRedirect($model);
				else
					$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'model' => $model,

		));
	}

    public function actionView($id) {
        $messageId = $id;
        $viewedMessage = Message::model()->findByPk($messageId);

        if (!$viewedMessage) {
            throw new CHttpException(404, MessageModule::t('Message not found'));
        }

        $userId = Yii::app()->user->getId();

        if ($viewedMessage->sender_id != $userId && $viewedMessage->receiver_id != $userId) {
            if(user()->category=='agency'){
                $ids=array();
                foreach(user()->getModel()->girls as $m){
                    $ids[]=$m->id;
                }
                if(!in_array($viewedMessage->sender_id,$ids)&&!in_array($viewedMessage->receiver_id,$ids)){
                    throw new CHttpException(403, MessageModule::t('You can not view this message'));
                }

            }elseif(!user()->isAdmin()){
                throw new CHttpException(403, MessageModule::t('You can not view this message'));
            }

        }
        if (($viewedMessage->sender_id == $userId && $viewedMessage->deleted_by == Message::DELETED_BY_SENDER)
            || $viewedMessage->receiver_id == $userId && $viewedMessage->deleted_by == Message::DELETED_BY_RECEIVER) {
            throw new CHttpException(404, MessageModule::t('Message not found'));
        }
        $message = new Messages();

        $isIncomeMessage = $viewedMessage->receiver_id == $userId;
        if ($isIncomeMessage) {
            $message->subject = preg_match('/^Re:/',$viewedMessage->subject) ? $viewedMessage->subject : 'Re: ' . $viewedMessage->subject;
            $message->receiver_id = $viewedMessage->sender_id;
        } else {
            $message->receiver_id = $viewedMessage->receiver_id;
        }

        if (Yii::app()->request->getPost('Message')) {
            $message->attributes = Yii::app()->request->getPost('Message');
            $message->sender_id = $userId;
            if ($message->save()) {
                Yii::app()->user->setFlash('success', MessageModule::t('Message has been sent'));
                if ($isIncomeMessage) {
                    $this->redirect($this->createUrl('inbox/'));
                } else {
                    $this->redirect($this->createUrl('sent/'));
                }
            }
        }

        if ($isIncomeMessage) {
            $viewedMessage->markAsRead();
        }

        $this->render(Yii::app()->getModule('message')->viewPath . '/view', array('viewedMessage' => $viewedMessage, 'message' => $message));
    }

    public function actionViewRequest() {

        $messageId = app()->request->getParam('id',false);

        if($messageId){


        $viewedMessage = Message::model()->findByPk($messageId);

            $form = new SAdminForm('application.modules.message.models.requestForm');
        $request=new PartnershipForm();

            $request->attributes=CJSON::decode($viewedMessage->subdata);
            $request->id=$viewedMessage->id;
            $form['request']->model=$request;

        if (!$viewedMessage) {
            throw new CHttpException(404, MessageModule::t('Message not found'));
        }

        $userId = Yii::app()->user->getId();

        if ($viewedMessage->sender_id != $userId && $viewedMessage->receiver_id != $userId) {
            if(user()->category=='agency'){
                $ids=array();
                foreach(user()->getModel()->girls as $m){
                    $ids[]=$m->id;
                }
                if(!in_array($viewedMessage->sender_id,$ids)&&!in_array($viewedMessage->receiver_id,$ids)){
                    throw new CHttpException(403, MessageModule::t('You can not view this message'));
                }

            }elseif(!user()->isAdmin()){
                throw new CHttpException(403, MessageModule::t('You can not view this message'));
            }

        }
        if (($viewedMessage->sender_id == $userId && $viewedMessage->deleted_by == Message::DELETED_BY_SENDER)
            || $viewedMessage->receiver_id == $userId && $viewedMessage->deleted_by == Message::DELETED_BY_RECEIVER) {
            throw new CHttpException(404, MessageModule::t('Message not found'));
        }
        $message = new Messages();

        $isIncomeMessage = $viewedMessage->receiver_id == $userId;
        if ($isIncomeMessage) {
            $message->subject = preg_match('/^Re:/',$viewedMessage->subject) ? $viewedMessage->subject : 'Re: ' . $viewedMessage->subject;
            $message->receiver_id = $viewedMessage->sender_id;
        } else {
            $message->receiver_id = $viewedMessage->receiver_id;
        }

        if (Yii::app()->request->getPost('Message')) {
            $message->attributes = Yii::app()->request->getPost('Message');
            $message->sender_id = $userId;
            if ($message->save()) {
                Yii::app()->user->setFlash('success', MessageModule::t('Message has been sent'));
                if ($isIncomeMessage) {
                    $this->redirect($this->createUrl('inbox/'));
                } else {
                    $this->redirect($this->createUrl('sent/'));
                }
            }
        }

        if ($isIncomeMessage) {
            $viewedMessage->markAsRead();
        }
        }
        $this->render(Yii::app()->getModule('message')->viewPath . '/view_request', array('adminform'=>$form,'viewedMessage' => $viewedMessage, 'message' => $message));
    }


	public function actionUpdateStatus()
	{
		$ids    = Yii::app()->request->getPost('ids');
		$status = Yii::app()->request->getPost('status');
		$models = Comment::model()->findAllByPk($ids);

		if(!array_key_exists($status, Comment::getStatuses()))
			throw new CHttpException(404, Yii::t('ChatModule.admin', 'Ошибка проверки статуса.'));

		if(!empty($models))
		{
			foreach ($models as $comment)
			{
				$comment->status = $status;
				$comment->save();
			}
		}

		echo Yii::t('ChatModule', 'Статус успешно изменен');
	}

	/**
	 * Delete chat
	 * @param array $id
	 */
	public function actionDelete($id = array())
	{
		if (Yii::app()->request->isPostRequest)
		{
			$model = Message::model()->findAllByPk($_REQUEST['id']);

			if (!empty($model))
			{
				foreach($model as $m)
					$m->delete();
			}

			if (!Yii::app()->request->isAjaxRequest)
				$this->redirect('index');
		}
	}

}
