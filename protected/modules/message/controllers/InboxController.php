<?php

class InboxController extends Controller
{
	public $defaultAction = 'inbox';
    public $layout='//layouts/main_profile';
	public function actionInbox() {
		$messagesAdapter = Message::getAdapterForInbox(Yii::app()->user->getId());
        $sort=new CSort();
        $sort->attributes=array(
            'title'=>array(
                'asc'=>'title',
                'desc'=>'title DESC',
                'label'=>'Наименование'
            ),
            'is_read'=>array(
                'asc'=>'is_read',
                'desc'=>'is_read DESC',
                'label'=>'Прочтен'
            )
        );
        $messagesAdapter->setSort($sort);

        $pager = new CPagination($messagesAdapter->totalItemCount);
		$pager->pageSize = 10;
		$messagesAdapter->setPagination($pager);

		$this->render(Yii::app()->getModule('message')->viewPath . '/inbox', array(
			'messagesAdapter' => $messagesAdapter
		));
	}
}
