<?php

class ComposeController extends Controller
{
    public $layout='//layouts/main_msg';
    public $defaultAction = 'compose';

    public function beforeAction($action){
        if(parent::beforeAction($action)){

            return !user()->isGuest;
        }else{
            return false;
        }

    }
    public function actionCompose($id = null)
    {

        $this->layout='//layouts/main_profile';
        $message = new Messages();
        if (Yii::app()->request->getPost('Message')) {
            $receiverName = Yii::app()->request->getPost('receiver');
            $message->attributes = Yii::app()->request->getPost('Message');
            $message->sender_id = Yii::app()->user->getId();

            if (is_array($message->receiver_id)) {
                user()->sendMessage($message->receiver_id,false,$message->subject,$message->body);
                $this->redirect($this->createUrl('inbox/'));
            } elseif (request()->getPost('all', false)) {

                $users = User::model()->findAll();
                $ids = array_map(function ($m) {
                    return $m->id;
                }, $users);

                user()->sendMessage($ids,false,$message->subject,$message->body);

                $this->redirect($this->createUrl('inbox/'));


            } else {

            }

        }

        if ($id) {
            $receiver = call_user_func(array(call_user_func(array(Yii::app()->getModule('message')->userModel, 'model')), 'findByPk'), $id);
            if ($receiver) {
                $receiverName = call_user_func(array($receiver, Yii::app()->getModule('message')->getNameMethod));
                $message->receiver_id = $receiver->id;
            }
        }

        $this->render(Yii::app()->getModule('message')->viewPath . '/compose', array('model' => $message, 'receiverName' => isset($receiverName) ? $receiverName : null));
    }
}