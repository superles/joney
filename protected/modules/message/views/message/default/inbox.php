<?php
/**
 * @var Controller $this
 */
$this->pageTitle=Yii::app()->name . ' - '.MessageModule::t("Сообщения:Входящие"); ?>
<?php
	$this->breadcrumbs=array(
		MessageModule::t("Сообщения"),
		MessageModule::t("Входящие"),
	);

?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation') ?>

<h2><?php echo MessageModule::t('Входящие'); ?></h2>

<?php echo TbHtml::linkButton('Создать',array('url'=>$this->createUrl('/message/compose'),'color'=>'danger')) ?>
    <?php $m=new Messages();
    $m->setDbCriteria(MessageModule::getInboxMessages(user()->id)->criteria);
    ?>
    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider' => MessageModule::getInboxMessages(user()->id),
        'type'=>array(TbHtml::GRID_TYPE_STRIPED,TbHtml::GRID_TYPE_HOVER),
        //'filter' => $m,
        //'template' => "{items}",
        'htmlOptions'=>array('class'=>'table-responsive','style'=>''),
        'rowCssClassExpression'=>'($row%2==0?"odd msg":"even msg").(($data->is_read==1)?" read":" unread")',
        'columns' => array(
            array(
                'name' => 'id',
                'header' => '#',
                'htmlOptions' => array('color' =>'width: 60px'),
            ),

            array(

                'header' => 'От',
                'name'=>'sender.username'

            ),
            array(

                'header' => 'Кому',
                'name'=>'receiver.username'

            ),
            array(
                'name' => 'subject',
                'header' => 'Тема',
            ),
//            array(
//                'name' => 'is_read',
//                'header' => 'Прочтен',
//                'type'=>'raw',
//                'value'=>'$data->is_read?"Да":"Нет"',
//                'filter'=>array('0'=>'нет','1'=>'да')
//
//            ),
            array(
                'name' => 'body',
                'header' => 'Текст',
                'type'=>'raw',
                 'value'=>function(Message $data){
                     return strTruncate(strip_tags_smart($data->body),40,' ','...');
                 },
                'htmlOptions' => array('style'=>'width: 100px'),

            ),
            array(
                'name' => 'created_at',
                'header' => 'Создано',
            ),
            array(
                'name' => 'obj_id',
                'type'=>'raw',
                'header' => 'Событие',
                'value'=>function(Message $data,$index,$column){
                    if(empty($data->obj_class)){
                        return 'нет';
                    }
                    $obj=new $data->obj_class;

                    $obj=$obj->with('translate')->findByPk($data->obj_id);

                    if($obj){
                        return CHtml::link($obj->title,$obj->getViewUrl(),array('class'=>'text-info'));
                    }else{
                        return 'нет';
                    }
                },
                'htmlOptions' => array('style'=>'width: 100px'),
            ),

            array(

                'header' => 'Файлы',
                'type'=>'raw',
                'value'=>'$data->hasAttachment()?"<span class=\"glyphicon glyphicon-paperclip\"></span>":""'
            ),
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'template'=>'{view}',
                'buttons' => array(
                    'view' => array(
                        'url' => '$this->grid->controller->createUrl("/message/view/".$data->id)'

                        ))

            ),
        ),
    )); ?>

</div>