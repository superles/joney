<div class="reply col-md-12" style="margin-top: 15px">
<?php
$adminAssetsUrl = Yii::app()->getModule('admin')->assetsUrl;
clientScript()->registerPackage('jquery.ui')->registerCssFile($adminAssetsUrl.'/vendors/jquery_ui/css/custom-theme/jquery-ui-1.8.14.custom.css');
$this->widget('application.extensions.fancybox.EFancyBox', array(


));

clientScript()->registerScript('fancy_msg',"


	$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});
$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',


					helpers : {
						media : {},
						buttons : {}
					}
				});
    ",CClientScript::POS_READY);

?>

<?php $this->pageTitle=Yii::app()->name . ' - ' . MessageModule::t("Создать сообщение"); ?>
<?php $isIncomeMessage = $viewedMessage->receiver_id == Yii::app()->user->getId() ?>

<?php
	$this->breadcrumbs = array(
        'Home'=>$this->createUrl('index'),
        Yii::t('MessageModule.admin', 'Messages')=>$this->createUrl('index'),
		CHtml::encode($viewedMessage->subject),
	);
?>

<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation') ?>

<?php $form = $this->beginWidget('TbActiveForm', array(
	'id'=>'message-delete-form',
	'enableAjaxValidation'=>false,
	'action' => $this->createUrl('delete/', array('id' => $viewedMessage->id))
)); ?>

<?php $this->endWidget(); ?>

<?php if ($isIncomeMessage): ?>
	<h4 class="message-from">От: <?php echo $viewedMessage->getSenderName() ?></h4>
<?php else: ?>
	<h4 class="message-to padding-bottom-10  padding-top-10 ">Кому: <?php echo $viewedMessage->getReceiverName() ?></h4>
<?php endif; ?>




<div class="panel panel-default">
	  <div class="panel-heading">
          <div class="row">
          <div class="col-xs-6">	<h3 class="panel-title">Тема:  <?php echo CHtml::encode($viewedMessage->subject) ?> </h3></div>
          <div class="col-xs-6 text-right">	<span class="date h12 margin-right-20"><?php echo date(Yii::app()->getModule('message')->dateFormat, strtotime($viewedMessage->created_at)) ?></span><button class="btn btn-default btn-xs "><?php echo MessageModule::t("Удалить") ?></button></div></div>
	  </div>
	  <div class="panel-body">
          <?php echo ($viewedMessage->body) ?>
	  </div>
</div>


<h3><?php echo MessageModule::t('Ответить') ?></h3>

<div class="form margin-top-20">
	<?php $form = $this->beginWidget('TbActiveForm', array(
		'id'=>'message-form',
		'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data')
	));
    /**
     * @var TbActiveForm $form
     */
    ?>

	<?php echo $form->errorSummary($message); ?>


		<?php echo $form->hiddenField($message,'receiver_id'); ?>




		<?php echo $form->textFieldControlGroup($message,'subject'); ?>
    <div class="form-group">
        <?php echo $form->label($message,'body'); ?>
        <span class="required"> *</span>
        <?php
        //echo $form->textArea($model,'body')
        Yii::import('ext.elrte.SElrteArea');
        Yii::import('admin.components.SRichTextarea');
        $editor=new SRichTextarea();

        $editor->auto=true;
        $editor->name='Message[body]';
        $editor->value=$message->body;
        $editor->init();
        $editor->run();
        ?>

    </div>

    <div class="form-group">
        <?php
        $this->widget('CMultiFileUpload', array(
            'model'=>$message,
            'attribute'=>'files',

            'options'=>array(
                // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
                // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
                // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
                // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
                // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
                // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
            ),
            'denied'=>'File is not allowed',
            'max'=>10, // max 10 files


        ));
        ?>
    </div>


	<div class="form-group buttons">
		<?php echo TbHtml::submitButton(MessageModule::t("Ответить")); ?>
	</div>

	<?php $this->endWidget(); ?>
    <div id="files">
        <?php css($this->module->assetsUrl.'/css/admin.css');?>
        <?php js('/style/vendors/bootstrap/js/bootstrap.min.js',CClientScript::POS_END)?>
        <?php
        Yii::import('application.modules.message.models.*');
        Yii::import('ext.attach.*');

        $c=new CDbCriteria();
        $c->compare('classname',get_class($viewedMessage));
        $c->compare('class_id',$viewedMessage->id);

        $files=new CActiveDataProvider('AttachmentFile',array(
            'criteria'=>$c
        ));
         ?>
        <?php
        /**
         * @var AttachmentFile $file
         */
        foreach($files->getData() as $file):?>
        <?php $file_model=app()->file->set($file->filename);         ?>
        <div class="form-group file">
         <?php
            $mime=explode('/',$file_model->getMimeTypeByExtension());
         ?>
           <?php if($mime[0]=='image'):?>

               <div class="col-xs-6 col-md-3">
                   <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo $file->getUrl()?>"><img class="img-thumbnail pull-left" src="<?php echo $file->getUrl()?>" title="" /></a>

               </div>

            <?php else:?>
        <?php echo TbHtml::linkButton('<span class="glyphicon glyphicon-download-alt"> </span> '.app()->file->set($file->filename)->getFilename(),array('url'=>$file->getUrl()))?>
                <?php endif;?>
        </div>
        <?php endforeach?>
    </div>
</div>
</div>