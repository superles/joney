<?php $this->pageTitle=Yii::app()->name . ' - '.MessageModule::t("Messages:sent"); ?>
<?php
	$this->breadcrumbs=array(
        MessageModule::t("Сообщения"),
        MessageModule::t("Входящие"),
	);
?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation') ?>

<h2><?php echo MessageModule::t('Отправленные'); ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => MessageModule::getSentMessages(user()->id),
    //'filter' => new Messages(),
    'template' => "{items}",
    'columns' => array(
        array(
            'name' => 'id',
            'header' => '#',
            'htmlOptions' => array('color' =>'width: 60px'),
        ),
        array(

            'header' => 'Кому',
            'name'=>'receiver.profile.full_name'

        ),
        array(
            'name' => 'subject',
            'header' => 'Тема',
        ),
        array(
            'name' => 'created_at',
            'header' => 'Создано',
        ),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}',
            'buttons' => array(
                'view' => array(
                    'url' => '$this->grid->controller->createUrl("/message/view/".$data->id)'

                ))

        ),
    ),
)); ?>  </div>