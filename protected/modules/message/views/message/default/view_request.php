<?php
/**
 * @var Controller $this
 * @var SAdminForm $form
 */
?>
<?php

$this->topButtons = $this->widget('application.modules.admin.widgets.SAdminTopButtons', array(
    'form'=>$adminform,
    'deleteAction'=>$this->createUrl('delete/', array('id' => $viewedMessage->id)),

));

$title = Yii::t('MessageModule.core', 'Запрос на партнерство');
    Yii::t('MessageModule.core', 'Запрос на партнерство');

$this->breadcrumbs = array(
    'Home'=>$this->createUrl('/admin'),
    Yii::t('MessageModule.core', 'Запросы')=>$this->createUrl('index'),
    Yii::t('MessageModule.core', 'Запрос на партнерство')
);
?>
<div class="reply col-md-12" style="margin-top: 15px">
    <?php

    $this->widget('application.extensions.fancybox.EFancyBox', array(


    ));

    clientScript()->registerScript('slider',"


	$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});
$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',


					helpers : {
						media : {},
						buttons : {}
					}
				});
    ",CClientScript::POS_READY);

    ?>

    <?php css($this->module->assetsUrl.'/css/admin.css');?>
    <?php //js('/style/vendors/bootstrap/js/bootstrap.min.js',CClientScript::POS_END)?>
<?php $form = $this->beginWidget('TbActiveForm', array(
	'id'=>'message-delete-form',
	'enableAjaxValidation'=>false,
	'action' => $this->createUrl('delete/', array('id' => $viewedMessage->id))
)); ?>
	<button class="btn danger"><?php echo MessageModule::t("Удалить") ?></button>

<?php $this->endWidget(); ?>

<h2>Запрос на партнерство</h2>

<div id="partnership">
    <?php

    $f=new PartnershipForm();
    $f->attributes=CJSON::decode($viewedMessage->subdata);

    echo $this->widget('TbDetailView',
        array(
            'data'=>$f,
            'type' => array('condensed', 'striped', 'bordered'),
            'htmlOptions' => array('style'=>'text-align: left'),
            'attributes'=>array(
                'login',
                'email',
                'name',
                'phone',
                'address',
                'country',
                'city',
                'date_registration',
                'director_name',
                'num_girls',
                'num_webcams',
                'num_comps',
                'num_employees',
                'girls_links',
                'partner_links'
            ),

        ),true);
    ?>

    <?php echo $adminform; ?>

</div>

    <div id="files">
        <?php css($this->module->assetsUrl.'/css/admin.css');?>
        <?php //js('/style/vendors/bootstrap/js/bootstrap.min.js',CClientScript::POS_END)?>
        <?php
        Yii::import('application.modules.message.models.*');
        Yii::import('ext.attach.*');

        $c=new CDbCriteria();
        $c->compare('classname',get_class($viewedMessage));
        $c->compare('class_id',$viewedMessage->id);

        $files=new CActiveDataProvider('AttachmentFile',array(
            'criteria'=>$c
        ));
        ?>
        <?php
        /**
         * @var AttachmentFile $file
         */
        foreach($files->getData() as $file):?>
            <?php $file_model=app()->file->set($file->filename);         ?>
            <div class="form-group file">
                <?php
                $mime=explode('/',$file_model->getMimeTypeByExtension());
                ?>
                <?php if($mime[0]=='image'):?>

                    <div class="col-xs-6 col-md-3">
                        <a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo $file->getUrl()?>"><img class="img-thumbnail pull-left" src="<?php echo $file->getUrl()?>" title="" /></a>

                    </div>

                <?php else:?>
                    <?php echo TbHtml::linkButton('<span class="glyphicon glyphicon-download-alt"> </span> '.app()->file->set($file->filename)->getFilename(),array('url'=>$file->getUrl()))?>
                <?php endif;?>
            </div>
        <?php endforeach?>
    </div>
</div>