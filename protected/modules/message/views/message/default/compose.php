<?php
/**
 * @var ComposeController $this
 * @var Message $model
 */
$this->pageTitle=Yii::app()->name . ' - '.MessageModule::t("Создать сообщение"); ?>
<?php
	$this->breadcrumbs=array(
		MessageModule::t("Сообщения"),
		MessageModule::t("Создать"),
	);
$adminAssetsUrl = Yii::app()->getModule('admin')->assetsUrl;
$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
    //'jquery.js'=>false,
    'bootstrap.min.js'=>false
);

clientScript()->registerPackage('jquery.ui')->registerCssFile($adminAssetsUrl.'/vendors/jquery_ui/css/custom-theme/jquery-ui-1.8.14.custom.css');

$model->receiver_id=request()->getParam('id');
?>

<style>
    .dd-option{
        text-align: left;
    }
    ul.dd-options{
        overflow-y: scroll !important;
        height: 300px;
    }
    .dd-option-image, .dd-selected-image {
        vertical-align: middle;
        float: left;
        margin-right: 5px;
        max-width: 30px;
    }
    .dd-option label{

        line-height: 25px !important;
    }

</style>


<?php clientScript()->registerCssFile("//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css")?>
<?php clientScript()->registerScriptFile("//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js",CClientScript::POS_END)?>

<?php
clientScript()->registerScript('select2',<<<JS
  $('.select2').select2({
   placeholder: "Выберите получателя",
   minimumResultsForSearch:1
  });
JS
    ,CClientScript::POS_READY
);
$this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation'); ?>

<h2><?php echo MessageModule::t('Создать сообщение'); ?></h2>

<div class="form">
	<?php $form = $this->beginWidget('TbActiveForm', array(
		'id'=>'message-form',
		'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data')
	));

    /**
     * @var TbActiveForm $form
     */
    ?>

	<p class="note">
        <?php echo MessageModule::t('Поля с <span class="required">*</span> обязательны'); ?>
    </p>

	<?php echo $form->errorSummary($model); ?>










        <?php //clientScript()->registerScriptFile('/style/js/jquery.ddslick.js',CClientScript::POS_END) ?>





            <?php echo TbHtml::activedropDownListControlGroup($model, 'receiver_id',CHtml::listData(User::model()->with('profile')->findAll(),'id','profile.full_name'),array('prompt'=>'','class'=>'select2','multiple'=>'multiple')); ?>




	<?php echo $form->textFieldControlGroup($model,'subject'); ?>
    <div class="form-group">
        <?php echo $form->label($model,'body'); ?>
        <span class="required"> *</span>
        <?php
        //echo $form->textArea($model,'body')
        //Yii::import('ext.ckeditor.CKEditorWidget');
        Yii::import('ext.elrte.SElrteArea');
        Yii::import('admin.components.SRichTextarea');
        $editor=new SRichTextarea();
        $editor->auto=true;
        $editor->theme='compant';
        $editor->helper='message_helper.js';


        $editor->name='Message[body]';
        $editor->value=$model->body;
        $editor->init();
        $editor->run();
        ?>

    </div>
    <div class="form-group">
    <?php
    $this->widget('CMultiFileUpload', array(
        'model'=>$model,
        'attribute'=>'files',

        'options'=>array(
            // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
            // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
            // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
            // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
            // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
            // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
        ),
        'denied'=>'File is not allowed',
        'max'=>10, // max 10 files


    ));
    ?>
    </div>

	<div class="form-group buttons">
		<?php echo TbHtml::submitButton(MessageModule::t("Отправить")); ?>
	</div>

	<?php $this->endWidget(); ?>



<?php
$basePath=Yii::getPathOfAlias('application.modules.message.assets');
$baseUrl=Yii::app()->getAssetManager()->publish($basePath);
$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');

$cs->registerCssFile($baseUrl.'/css/styles.css');

?>


</div>