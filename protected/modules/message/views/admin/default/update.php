<?php

/**
 * Create/update comment
 *
 * @var $model Comment
 */

$this->topButtons = $this->widget('admin.widgets.SAdminTopButtons', array(

	'deleteAction' => $this->createUrl('/chat/admin/chat/delete', array('id'=>$model->id)),
	'template'     => array('history_back','save','delete')
));

$title = Yii::t('ChatModule.admin', 'Редактирование комментария');

$this->breadcrumbs = array(
	'Home'=>$this->createUrl('index'),
	Yii::t('ChatModule.admin', 'Комментарии')=>$this->createUrl('index'),

);

$this->pageHeader = $title;

?>

<?php clientScript()->registerCssFile("//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css")?>
<?php clientScript()->registerScriptFile("//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js",CClientScript::POS_END)?>

<?php

clientScript()->registerScript('',<<<JS
 $('.select2').select2({
   placeholder: "Выберите получателя",
   minimumResultsForSearch:1
  });
JS
    ,CClientScript::POS_READY
);
 $model->sender_id=user()->id;
?>
<div class="form wide padding-all">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'layout' => TbHtml::FORM_LAYOUT_VERTICAL,
        'action'=>'/message/compose'
    ));
    /**
     * @var TbActiveForm $form
     **/
    ?>
    <?php echo $form->dropDownListControlGroup($model, 'receiver_id',CHtml::listData(User::model()->with('profile')->findAll(),'id','profile.full_name'),array('prompt'=>'','class'=>'select2','multiple'=>'multiple')); ?>
    <?php echo TbHtml::dropDownListControlGroup('project', '',CHtml::listData(Project::model()->findAll(),'id','title'),array('label'=>'Участникам проекта','prompt'=>'')); ?>
    <div class="row">
    <label for=""></label>
    <?php echo TbHtml::checkBoxControlGroup('all', '',array('label'=>'Всем')); ?>
    <hr/>
    </div>
    <div class="row">
    <label for="">Сообщение</label>
    <?php echo $form->textArea($model, 'body', array('1', '2', '3')); ?>
        </div>
    <?php echo $form->hiddenField($model,'sender_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'subject'); ?>

    <?php echo TbHtml::formActions(array(
        TbHtml::submitButton('Submit', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
        TbHtml::resetButton('Reset'),
    )); ?>

    <?php $this->endWidget(); ?>
</div>