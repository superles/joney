<?php

/**
 * Display Chat list
 *
 * @var $model Message
 **/

Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/admin/message.index.js');

$this->pageHeader = Yii::t('MessageModule.core', 'Запросы');

$this->breadcrumbs = array(
    'Home'=>$this->createUrl('/admin'),
    Yii::t('MessageModule.core', 'Запросы'),
);

$this->topButtons = $this->widget('application.modules.admin.widgets.SAdminTopButtons', array(
    'template'=>array('create'),
    'elements'=>array(
        'create'=>array(
            'link'=>$this->createUrl('create'),
            'title'=>Yii::t('MessageModule.core', 'Создать запрос'),
            'options'=>array(
                'icons'=>array('primary'=>'ui-icon-plus')
            )
        ),
    ),
));

$this->pageHeader = Yii::t('MessageModule.core', 'Запросы');

$this->widget('ext.sgridview.SGridView', array(
    'dataProvider' => $dataProvider,
    'id'           => 'requestListGrid',
    'filter'       => $model->with('sender.profile'),

    'columns' => array(
        array(
            'class'=>'CCheckBoxColumn',
        ),
        array(
            'class'=>'SGridIdColumn',
            'name'=>'id',
        ),

        array(

            'header' => 'From',
            'value'=>'User::model()->findByPk($data->sender_id)?User::model()->findByPk($data->sender_id)->username:$data->sender_id',
            'type'=>'raw',
            'filter'=>TbHtml::activeDropDownList($model,"sender_id",CHtml::listData(User::model()->findAll(),"id","username"),array('empty'=>''))


        ),
        array(
            'header' => 'To',
            'value'=>'User::model()->findByPk($data->receiver_id)?User::model()->findByPk($data->receiver_id)->username:$data->receiver_id',
            'type'=>'raw',
            'filter'=>TbHtml::activeDropDownList($model,"receiver_id",CHtml::listData(User::model()->findAll(),"id","username"),array('empty'=>''))



        ),

        array(
            'name' => 'subject',
            'header' => 'Subject',
        ),
        array(
            'name' => 'created_at',
            'header' => 'Date',
            'htmlOptions'=>array('class'=>'date-column'),
        ),
//        array(
//            'name' => 'is_read',
//            'header' => 'Is Read?',
//            'type'=>'raw',
//            'value'=>'empty($data->is_read)?"<span class=\"label label-success\">read</span>":"<span class=\"label label-primary\">not read</span>"',
//            'htmlOptions'=>array('class'=>'label-column'),
//        ),
        array(
            'class'=>'TbButtonColumn',
            'template'=>'{view}',
            'buttons' => array(
                'view' => array(
                    'url' => '$this->grid->controller->createUrl("/admin/message/viewRequest?id=".$data->id)'

                ))

        ),
    ),
)); ?>

<style>
    .label-success {
        background-color: #5cb85c;
    }
    .label-primary {
        background-color: #428bca;
    }
    .label {
        display: inline;
        padding: .2em .6em .3em;
        font-size: 75%;
        font-weight: 700;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25em;
    }
    .label-column{
        width: 50px;
    }
    .date-column{
        width: 200px;
    }
</style>