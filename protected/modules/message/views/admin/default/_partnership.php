<?php
/**
 * @var $this Controller
 */
?>
<div id="partnership">
<?php
echo $this->widget('TbDetailView',
    array(
'data'=>$model,
    'type' => array('condensed', 'striped', 'bordered'),
        'htmlOptions' => array('style'=>'text-align: left'),
'attributes'=>array(
'login',
'email',
'name',
'phone',
'address',
'country',
'city',
'date_registration',
'director_name',
'num_girls',
'num_webcams',
'num_comps',
'num_employees',
'girls_links',
'partner_links'
),

),true);
?>
</div>