<?php

class SystemSettingsForm extends CFormModel
{

	/**
	 * @var string
	 */
	public $core_siteName;

	/**
	 * @var integer products limit to display on front site
	 */
	public $core_productsPerPage;

	/**
	 * @var integer
	 */
	public $core_productsPerPageAdmin;

	/**
	 * @var string site theme name
	 */
	public $core_theme;


    /**
	 * @var string site theme name
	 */
	public $core_description;

    /**
	 * @var string site theme name
	 */
	public $core_slogan;

	/**
	 * @var
	 * seo
	 */
	public $seo_robots;

	public $seo_head;

	public $seo_body;
	public $seo_body_bottom;

	/*
	 * counters
	 */

	public $count_google;
	/*
	 * counters
	 *
	 */

	public $count_yandex;
	/*
	 * Social link page.
	 */
	public $social_facebook_link;
	/*
	 * Social link page.
	 */
	public $social_vk_link;
	public $social_twitter_link;
	public $social_instagram_link;
	public $social_pininterest_link;
	public $social_google_link;
	/*
	 * Contacts.
	 */
	public $contacts_phone;

	/**
	 * Editor settings
	 */
	public $core_editor;
	public $core_editorTheme;
	public $core_editorHeight;
	public $core_editorAutoload;

	/**
	 * Image settings
	 */
	public $images_path;
	public $images_thumbPath;
	public $images_url;
	public $images_thumbUrl;
	public $images_maxFileSize;
	public $images_maximum_image_size;
	public $images_watermark_image;
	public $images_watermark_active;
	public $images_watermark_position_vertical;
	public $images_watermark_position_horizontal;
	public $images_watermark_opacity;

	public function getCategories(){
			return	array('core', 'images','contacts','social','count','seo');
	}

	public function init()
	{
		$categories = $this->getCategories();


		foreach(array_keys($this->getAttributes()) as $attr){
			app()->db->createCommand()->from('SystemSettings');
		}

		foreach ($categories as $c)
		{
			$settings=Yii::app()->settings->get($c);

			if($settings)
			{
				foreach ($settings as $key=>$val)
				{
					$attr = $c.'_'.$key;
					if(property_exists($this, $attr))
						$this->$attr = $val;
				}
			}
		}

	}


	
	/**
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('core_siteName,core_editor, core_productsPerPage, core_productsPerPageAdmin, core_theme, core_editorTheme, core_editorHeight, core_editorAutoload', 'required'),
			array('seo_robots,seo_head,seo_body,seo_body_bottom,count_google,count_yandex,social_facebook_link,social_twitter_link,social_instagram_link,social_pininterest_link,social_google_link,contacts_phone,social_vk_link,core_description,core_slogan','safe'),
			array('images_path, images_thumbPath, images_url, images_thumbUrl, images_maxFileSize, images_maximum_image_size', 'required'),
			array('images_watermark_image', 'validateWatermarkFile'),
			array('images_watermark_active', 'boolean'),

			array('images_watermark_position_vertical', 'in', 'range'=>array_keys($this->getImageVerticalPositions())),
			array('images_watermark_position_horizontal', 'in', 'range'=>array_keys($this->getImageHorizontalPositions())),
			array('images_watermark_opacity', 'numerical', 'min'=>0, 'max'=>100),
		);
	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'core_siteName'             => Yii::t('CoreModule.admin', 'Название сайта'),
			'core_productsPerPage'      => Yii::t('CoreModule.admin', 'Количество товаров на сайте'),
			'core_productsPerPageAdmin' => Yii::t('CoreModule.admin', 'Количество товаров в панели управления'),
			'core_theme'                => Yii::t('CoreModule.admin', 'Тема'),
			// Editor
			'core_editorTheme'          => Yii::t('CoreModule.admin', 'Тема'),
			'core_editorHeight'         => Yii::t('CoreModule.admin', 'Высота'),
			'core_editorAutoload'       => Yii::t('CoreModule.admin', 'Автоматическая активация'),
			// Images
			'images_path'               => Yii::t('CoreModule.admin', 'Путь сохранения'),
			'images_thumbPath'          => Yii::t('CoreModule.admin', 'Путь к превью'),
			'images_url'                => Yii::t('CoreModule.admin', 'Ссылка к изображениям'),
			'images_thumbUrl'           => Yii::t('CoreModule.admin', 'Ссылка к превью'),
			'images_maxFileSize'        => Yii::t('CoreModule.admin', 'Максимальный размер файла'),
			'images_maximum_image_size' => Yii::t('CoreModule.admin', 'Максимальный размер изображения'),
			'images_watermark_active'   => Yii::t('CoreModule.admin', 'Активен'),
			'images_watermark_image'    => Yii::t('CoreModule.admin', 'Изображение'),
			'images_watermark_position_vertical'   => Yii::t('CoreModule.admin', 'Позиция по вертикали'),
			'images_watermark_position_horizontal' => Yii::t('CoreModule.admin', 'Позиция по горизонтали'),
			'images_watermark_opacity'             => Yii::t('CoreModule.admin', 'Прозрачность'),
			'seo_robots'=>Yii::t('CoreModule.admin','robots.txt'),
			'seo_head'=>Yii::t('CoreModule.admin',' Код до head  &lt;body&gt;'),
			'seo_body'=>Yii::t('CoreModule.admin',' Код  после body'),
			'seo_body_bottom'=>Yii::t('CoreModule.admin',' Код  перед &lt;/body&gt;'),
			'count_google'=>Yii::t('CoreModule.admin','Google analytics'),
			'count_yandex'=>Yii::t('CoreModule.admin','Metrika '),
			'social_facebook_link'=>Yii::t('CoreModule.admin','facebook'),
			'social_twitter_link'=>Yii::t('CoreModule.admin','tw'),
			'social_instagram_link'=>Yii::t('CoreModule.admin','in'),
			'social_pininterest_link'=>Yii::t('CoreModule.admin','pin'),
			'social_google_link'=>Yii::t('CoreModule.admin','google'),
			'contacts_phone'=>Yii::t('CoreModule.admin','Телефон на сайте'),
            'social_vk_link'=>'ВК',
            'core_description'=>'Описание',
            'core_slogan'=>'Слоган'
		);
	}

	public function getImageVerticalPositions()
	{
		return array(
			'top'    => 'top',
			'center' => 'center',
			'bottom' => 'bottom'
		);
	}

	public function getImageHorizontalPositions()
	{
		return array(
			'left'    => 'left',
			'center'  => 'center',
			'right'   => 'right',
		);
	}

	/**
	 * Saves attributes into database
	 */
	public function save()
	{
		foreach($this->getCategories() as $name){
			Yii::app()->settings->set($name, $this->getDataByPrefix($name));
		}


		$this->saveWatermark();
	}

	/**
	 * Saves watermark
	 */
	public function saveWatermark()
	{
		$watermark = CUploadedFile::getInstance($this,'images_watermark_image');
		if($watermark)
			$watermark->saveAs(Yii::getPathOfAlias('webroot') . '/uploads/watermark.png');
	}

	/**
	 * @param $prefix
	 * @return array
	 */
	public function getDataByPrefix($prefix)
	{
		$prefix.='_';
		$result = array();

		foreach ($this->attributes as $key=>$val)
		{
			if(substr($key,0,strlen($prefix))===$prefix)
			{
				$k=substr($key,strlen($prefix));
				$result[$k]=$val;
			}
		}

		return $result;
	}

	public function renderWatermarkImageTag()
	{
		if(file_exists(Yii::getPathOfAlias('webroot') . '/uploads/watermark.png'))
			return CHtml::image('/uploads/watermark.png?' . time());
	}

	/**
	 * Validates uploaded watermark file
	 */
	public function validateWatermarkFile($attr)
	{
		$file = CUploadedFile::getInstance($this,'images_watermark_image');
		if($file)
		{
			$allowedExts = array('jpg', 'gif', 'png');
			if(!in_array($file->getExtensionName(), $allowedExts))
				$this->addError($attr, 'Ошибка. Водяной знак не изображение.');
		}
	}
}
