<?php
class TranslateController extends TranslateBaseController{
	public function actionIndex(){
        if(isset($_POST['Message'])){
            foreach($_POST['Message'] as $id=>$message){
                if(empty($message['translation']))
                    continue;
                $model=new Message();
                $message['id']=$id;
                $model->setAttributes($message);
                $model->save();
            }
            $this->redirect(Yii::app()->getUser()->getReturnUrl());
        }
        if(($referer=Yii::app()->getRequest()->getUrlReferrer()) && $referer!==$this->createUrl('index'))
            Yii::app()->getUser()->setReturnUrl($referer);
        $translator=TranslateModule::translator();
        $key=$translator::ID."-missing";
        if(isset($_POST[$key]))
            $postMissing=$_POST[$key];
        elseif(Yii::app()->getUser()->hasState($key))
            $postMissing=Yii::app()->getUser()->getState($key);
        
        if(count($postMissing)){
            Yii::app()->getUser()->setState($key,$postMissing); 
            $cont=0;
            foreach($postMissing as $id=>$message){
                $models[$cont]=new Message;
                $models[$cont]->setAttributes(array('id'=>$id,'language'=>$message['language']));
                $cont++;
            }
        }else{
            $this->renderText(TranslateModule::t('All messages translated'));
            Yii::app()->end();
        }
        
        $data=array('messages'=>$postMissing,'models'=>$models);
        
        $this->render('index',$data);
	}
    function actionSet(){
        $translator=TranslateModule::translator();
        if(Yii::app()->getRequest()->getIsPostRequest()){
            TranslateModule::translator()->setLanguage($_POST[$translator::ID]);
            $this->redirect(Yii::app()->getRequest()->getUrlReferrer());
        }else
            throw new CHttpException(400);
    }
    function actionGoogletranslate(){
        if(Yii::app()->getRequest()->getIsPostRequest()){
            $translation=TranslateModule::translator()->googleTranslate($_POST['message'],$_POST['language'],$_POST['sourceLanguage']);
            if(is_array($translation))
                echo CJSON::encode($translation);
            else
                echo $translation;
        }else
            throw new CHttpException(400);
    }
    /**
     * Deletes a record
     * @param integer $id the ID of the model to be deleted
     * @param string $language the language of the model to de deleted
     */
    public function actionDelete($id,$language)
    {
        if(Yii::app()->getRequest()->getIsPostRequest()){
            $model=$this->translateLoadModel($id,$language);
            if($model->delete()){
                if(Yii::app()->getRequest()->getIsAjaxRequest()){
                    echo TranslateModule::t('Message deleted successfully');
                    Yii::app()->end();
                }else
                    $this->redirect(Yii::app()->getRequest()->getUrlReferrer());
            }
        }else
            throw new CHttpException(400);

    }
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function translateLoadModel($id,$language)
    {
        $model=Message::model()->findByPk(array('id'=>$id,'language'=>$language));
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}