<?php

class EditController extends TranslateBaseController
{
    public $defaultAction='admin';

    public $layout='application.modules.admin.views.layouts.main';
    /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionCreate($id,$language)
	{
		$model=new Message('create');
        $model->id=$id;
        $model->language=$language;
        
		if(isset($_POST['Message'])){
			$model->attributes=$_POST['Message'];
			if($model->save())
				$this->redirect(array('missing'));
		}

		$this->render('form',array('model'=>$model));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id,$language)
	{
		$model=$this->translateLoadModel($id,$language);

		if(isset($_POST['Message'])){
			$model->attributes=$_POST['Message'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('form',array('model'=>$model));
	}
    /**
	 * Deletes a record
	 * @param integer $id the ID of the model to be deleted
     * @param string $language the language of the model to de deleted
	 */
	public function actionDelete($id,$language)
	{
        if(Yii::app()->getRequest()->getIsPostRequest()){
    		$model=$this->translateLoadModel($id,$language);
            if($model->delete()){
                if(Yii::app()->getRequest()->getIsAjaxRequest()){
                    echo TranslateModule::t('Message deleted successfully');
                    Yii::app()->end();
                }else
                    $this->redirect(Yii::app()->getRequest()->getUrlReferrer());
            }
        }else
            throw new CHttpException(400);
        
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Message('search');
		$model->unsetAttributes();  // clear any default values
		$ids=array();
		if(isset($_POST['Message'])){
			foreach($_POST['Message'] as $category=>$array){
				foreach($array as $obj){
					$msg=key($obj);
					$translation=$obj[$msg];
					$cat=substr(strtolower($category),3);
					$message=MessageSource::model()->findByAttributes(array('message'=>$msg,'category'=>$cat));
					$ids[]=$message->id;
				}
			}
		}
			$model->id=$ids;

		$this->render('admin',array(
			'model'=>$model,
		));
	}
    /**
	 * 
	 */
	public function actionMissing()
	{
		$model=new MessageSource('search');
		$model->unsetAttributes();  // clear any default values
        
		if(isset($_GET['MessageSource']))
			$model->attributes=$_GET['MessageSource'];

        if(!empty($_GET['lang_id'])){
            $lang=app()->languageManager->getById($_GET['lang_id']);
            $model->language=$lang->code;
        }else{
            $model->language='ru';
        }


		$this->render('missing',array(
			'model'=>$model,
		));
	}
    /**
	 * Deletes a record
	 * @param integer $id the ID of the model to be deleted
     * @param string $language the language of the model to de deleted
	 */
	public function actionMissingdelete()
	{
        $id=$_POST['ids'];
        if(Yii::app()->getRequest()->getIsPostRequest()){

            if(MessageSource::model()->deleteByPk($id)){
                if(Yii::app()->getRequest()->getIsAjaxRequest()){
                    echo TranslateModule::t('Message deleted successfully');
                    Yii::app()->end();
                }else
                    $this->redirect(Yii::app()->getRequest()->getUrlReferrer());
            }
        }else
            throw new CHttpException(400);
        
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function translateLoadModel($id,$language)
	{
		$model=Message::model()->findByPk(array('id'=>$id,'language'=>$language));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}