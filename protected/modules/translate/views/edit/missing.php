<h1><?php echo TranslateModule::t('Missing Translations')." - ".TranslateModule::translator()->acceptedLanguages[$model->language]?></h1>
<?php
$this->topButtons = $this->widget('application.modules.admin.widgets.SAdminTopButtons', array(

    'langSwitcher'=>true,
    'template'=>array('history_back')
));
clientScript()->registerScript('del_all',<<<JS

function deleteMissingTranslates(el,lang){
    $.ajax('/translate/missing/delete', {
        type:"post",
        data:{
            YII_CSRF_TOKEN: $(el).attr('data-token'),
            ids: $.fn.yiiGridView.getSelection('missingGrid'),
            language:lang
        },
        success: function(data){
            $.fn.yiiGridView.update('productsListGrid');
            $.jGrowl(data);
        },
        error:function(XHR, textStatus, errorThrown){
            var err='';
            switch(textStatus) {
                case 'timeout':
                    err='The request timed out!';
                    break;
                case 'parsererror':
                    err='Parser error!';
                    break;
                case 'error':
                    if(XHR.status && !/\s*$/.test(XHR.status))
                        err='Error ' + XHR.status;
                    else
                        err='Error';
                    if(XHR.responseText && !/^\s*$/.test(XHR.responseText))
                        err=err + ': ' + XHR.responseText;
                    break;
            }
            alert(err);
        }
    });
    return false;

}
JS
    ,CClientScript::POS_HEAD);

?>
<style>
    .gridViewOptions{
        display: none;
    }
</style>
<?php
$source=MessageSource::model()->findAll();
$dp=$model->search();
$dp->setPagination(array('pageSize'=>50));
$this->widget('ext.sgridview.SGridView', array(
    'id'=>'missingGrid',
    'dataProvider'=>$dp,
    'filter'=>$model,
    'customActions' => array(
        array(
            'label'=>Yii::t('TranslateModule.admin', 'Удалить все'),
            'url'=>'#',
            'linkOptions'=>array(
                'onClick'=>'return deleteMissingTranslates(this,"'.app()->language.'");',
            ),
        ),
    ),
    'columns'=>array(
        array(
            'class'=>'CCheckBoxColumn',
        ),
        array(
            'class'=>'SGridIdColumn',
            'name'=>'id',
        ),
        array(
            'name'=>'message',
            'filter'=>CHtml::listData($source,'message','message'),
        ),
        array(
            'name'=>'category',
            'filter'=>CHtml::listData($source,'category','category'),
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{create}{delete}',
            'deleteButtonUrl'=>'Yii::app()->getController()->createUrl("missingdelete",array("id"=>$data->id))',
            'buttons'=>array(
                'create'=>array(
                    'label'=>TranslateModule::t('Create '),
                    'url'=>'Yii::app()->getController()->createUrl("create",array("id"=>$data->id,"language"=>isset($_GET["lang_id"])?app()->languageManager->getById($_GET["lang_id"])->code:app()->language))'
                )
            ),


        )
	),
)); ?>