<?php app()->bootstrap->register() ?>
<?php $action=$model->getIsNewRecord() ? 'Create' : 'Update';?>
<h1><?php echo TranslateModule::t(($action) . ' Message')." # ".$model->id." - ".TranslateModule::translator()->acceptedLanguages[$model->language]; ?></h1>

<div class="form">

<?php $form=$this->beginWidget('TbActiveForm', array(
	'id'=>'message-form',
	'enableAjaxValidation'=>false,
));
/**
 * @var TbActiveForm $form
 */
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
    
    <?php echo $form->hiddenField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
    <?php echo $form->hiddenField($model,'language',array('size'=>16,'maxlength'=>16)); ?>
    

        <?php echo $form->textFieldControlGroup($model->source,'category',array('disabled'=>'disabled')) ?>


        <?php echo $form->textFieldControlGroup($model->source,'message',array('disabled'=>'disabled')) ?>


    <?php echo $form->textAreaControlGroup($model,'translation') ?>



		<?php echo TbHtml::submitButton(TranslateModule::t($action)); ?>


<?php $this->endWidget(); ?>

</div><!-- form -->