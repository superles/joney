<?php 
    $language=TranslateModule::translator();
    $languageKey=$language::ID;

    
    $google=!empty(TranslateModule::translator()->googleApiKey) ? true : false;

    $this->topButtons = $this->widget('application.modules.admin.widgets.SAdminTopButtons', array(

        'langSwitcher'=>true,
        'template'=>array('history_back')
    ));
$this->topButtons = $this->widget('application.modules.admin.widgets.SAdminTopButtons', array(

    'langSwitcher'=>true,
    'template'=>array('history_back')
));
clientScript()->registerScript('del_all',<<<JS

function deleteMissingTranslates(el,lang){
    $.ajax('/translate/missing/delete', {
        type:"post",
        data:{
            YII_CSRF_TOKEN: $(el).attr('data-token'),
            ids: $.fn.yiiGridView.getSelection('missingGrid'),
            language:lang
        },
        success: function(data){
            $.fn.yiiGridView.update('productsListGrid');
            $.jGrowl(data);
        },
        error:function(XHR, textStatus, errorThrown){
            var err='';
            switch(textStatus) {
                case 'timeout':
                    err='The request timed out!';
                    break;
                case 'parsererror':
                    err='Parser error!';
                    break;
                case 'error':
                    if(XHR.status && !/\s*$/.test(XHR.status))
                        err='Error ' + XHR.status;
                    else
                        err='Error';
                    if(XHR.responseText && !/^\s*$/.test(XHR.responseText))
                        err=err + ': ' + XHR.responseText;
                    break;
            }
            alert(err);
        }
    });
    return false;

}
JS
    ,CClientScript::POS_HEAD);

?>

<h2><?php echo TranslateModule::t('Перевод на {lang}',array('{lang}'=>$language->acceptedLanguages[$language->getLanguage()]));?></h2>
<?php
    if($google){
        echo TbHtml::link(TranslateModule::t('Translate all with google translate'),"#",array('id'=>$languageKey."-google-translateall"));
        echo TbHtml::script(
            "\$('#{$languageKey}-google-translateall').click(function(){
                var messages=[];\$('.{$languageKey}-google-message').each(function(count){
                    messages[count]=$(this).html();
                });".
                TbHtml::ajax(array(
                    'url'=>$this->createUrl('translate/googletranslate'),
                    'type'=>'post',
                    'dataType'=>"json",
                    'data'=>array(
                        'language'=>Yii::app()->getLanguage(),
                        'sourceLanguage'=>Yii::app()->sourceLanguage,
                        'message'=>'js:messages'
                    ),
                    'success'=>"js:function(response){
                        \$('.{$languageKey}-google-translation').each(function(count){
                            $(this).val(response[count]);
                        });
                        \$('.{$languageKey}-google-button,#{$languageKey}-google-translateall').hide();
                    }",
                    'error'=>'js:function(xhr){alert(xhr.statusText);}',
                ))."
                return false;
            });
        ");
        if(Yii::app()->getRequest()->isAjaxRequest){
            echo TbHtml::script("
                $('#".$languageKey.'-pager'." a').click(function(){
                    \$dialog=$('#".$languageKey.'-dialog'."').load($(this).attr('href'));
                    return false;
                });
            ");
        }
    }
?>
<div class="form wide">
    <?php echo TbHtml::beginForm(); ?>
    <table class="table table-condensed">
        <thead>
            <th><?php echo MessageSource::model()->getAttributeLabel('category'); ?></th>
            <th><?php echo MessageSource::model()->getAttributeLabel('message'); ?></th>
            <th><?php echo Message::model()->getAttributeLabel('translation');?></th>
            <?php echo $google ? TbHtml::tag('th') : null;?>
        </thead>
        <tbody>
        <?php
            $this->widget('TbListView', array(
                'dataProvider'=>new CArrayDataProvider($models,array('pagination'=>array('pageSize'=>100))),
                'pager'=>array(
                    'id'=>$languageKey.'-pager',
                    'class'=>'TbPager',
                ),
                'viewData'=>array(
                    'messages'=>$messages,
                    'google'=>$google,
                ),
                'itemView'=>'_form',
            ));
        ?>
        </tbody>
    </table>
    <?php echo TbHtml::submitButton(TranslateModule::t('Translate'));?>
    <?php echo TbHtml::endForm()?>
</div>