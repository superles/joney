<?php

return array(
    'translate/missing/delete'=>'translate/edit/missingdelete',
    'translate/translate/set'=>'translate/translate/set',
    '/admin/translate'=>'/admin/translate/index',
	'/admin/translate/index'=>'/translate/translate/index',
    '/admin/translate/<action>'=>'/translate/edit/<action>'
);