<?php

Yii::import('translate.TranslateModule');

/**
 * Admin menu items for pages module
 */
return array(
	'Translate'=>array(
		'label'       => Yii::t('TranslateModule.admin', 'Переводы'),
		'url'         => array('/admin/translate/admin'),
		'position'    => 3,
		'itemOptions' => array(
			'class'       => 'hasRedCircle circle-Translate',
		),
        'items' => array(
            array(
                'label'       => Yii::t('TranslateModule.admin', 'Все переводы'),
                'url'         => array('/admin/translate/admin'),
                'position'    => 1
            ),
            array(
                'label'    => Yii::t('TranslateModule.admin', 'Необходимые'),
                'url'      => array('/admin/translate/missing'),
                'position' => 2
            ),

        ),

	),
);