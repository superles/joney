<?php

Yii::import('application.modules.orders.TranslateModule');

/**
 * Module info
 */ 
return array(
	'name'=>Yii::t('TranslateModule.core', 'Переводы'),
	'author'=>'firstrow@gmail.com',
	'version'=>'0.1',
	'description'=>Yii::t('TranslateModule.core', 'Переводы.'),
	'url'=>'', # Url to module home page.
);