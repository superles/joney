<?php

class {ClassName} extends CDbMigration
{
	public function up()
	{
	$this->addForeignKey("poll_vote_ibfk_1", "poll_vote", "choice_id", "poll_choice", "id","CASCADE","CASCADE");
$this->addForeignKey("poll_vote_ibfk_2", "poll_vote", "poll_id", "poll", "id");

	}

	public function down()
	{
		echo "{ClassName} does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}