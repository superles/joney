$(document).ready(function() {

    $("#slider").owlCarousel({
        navigation : false, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true
    });

    $("#sliderTours").owlCarousel({
        navigation : false, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true
    });

    $("#sliderReviews").owlCarousel({
        navigation : false, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true
    });

    $(".fancybox").fancybox();

    $("a#inline").fancybox({
        'type':'ajax',
        'autoSize':true,

        'hideOnContentClick': false
    });

});

function submitForm(el){
    var search=$(el).parents('form').find('input').val()
    window.location='/page/search/'+search;
}