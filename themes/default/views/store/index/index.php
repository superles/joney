<?php

/**
 * View page
 * @var Page $model
 * @var IndexController $this
 * @var Page $page
 */

// Set meta tags
$model=$this->model;
$this->pageTitle       = ($model->meta_title) ? $model->meta_title : $model->title;
$this->pageKeywords    = $model->meta_keywords;
$this->pageDescription = $model->meta_description;
$this->breadcrumbs[]=$model->title;
?>
<div class="pos-r">
	<div class="slideBtns text-center">
		<a href="/pages/routes" class="btn-primary dline ml10 mr10 lh60">Все маршруты</a>
		<a href="#" class="btn-success dline ml10 mr10 lh60">Попробуй бесплатно</a>
	</div>

<div id="slider" class="sliderTop">
	<?php foreach(Page::model()->withUrl('home')->find()->getImages() as  $image): ?>
	<?php
		/**
		 * @var GalleryPhoto $image
		 */
 ?>

	<div class="item">

		<img src="<?php echo $image->getUrl() ?>" alt="">
		<div class="bg"></div>
		<div class="slideText text-center">
			<h3><?php echo $image->name ?></h3>
			<?php echo $image->description ?>

		</div>

	</div>
	<?php endforeach ?>

</div><!-- End carousel -->
</div>
<section class="routes text-center pt80 pb120">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-lg-4">
				<p><img class="h-100 mb15" src="/style/img/route1.svg" alt=""></p>
				<p class="mb20 h-115">Только лучшие<br> и проверенные<br> маршруты</p>
				<p><a href="/pages/routes" class="btn-default dline lh40 pl10 pr10">Все маршруты</a></p>
			</div>
			<div class="col-md-4 col-lg-4 cntrBox">
				<p><img class="h-100 mb15" src="/style/img/route2.svg" alt=""></p>
				<p class="mb20 h-115">Путешествовать<br> с готовым маршрутом<br> просто и легко</p>
				<p><a href="#" class="btn-default dline lh40 pl10 pr10">Cкачать бесплатный маршрут</a></p>
			</div>
			<div class="col-md-4 col-lg-4">
				<p><img class="h-100 mb15" src="/style/img/route3.svg" alt=""></p>
				<p class="mb20 h-115">Устал от чтения блогов<br> и поиска в Интернете?<br> Планировать больше<br> не нужно!</p>

				<p><a id="inline" href="/site/robokassa?id=<?php echo Page::model()->getRandom() ?>" class="btn-default dline lh40 pl10 pr10">Купить случайный маршрут за 99 руб.</a></p>


			</div>
		</div>
	</div>
</section><!-- End routes -->

<section class="titleBox pt40 pb30">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-lg-5 logoBig">
				<img class="mr60" src="/style/img/logoBig.svg" alt="">
			</div>
			<div class="col-sm-10 col-sm-push-2 col-md-7 col-md-push-0 col-lg-7 pt10">
				<p class="pl75">делает подготовку к путешествию простой и неутомительной</p>
			</div>
		</div>
	</div>
</section><!-- End titleBox -->

<section class="order one23 text-center pt100 pb80">
	<div class="container">
		<div class="col-md-12 col-xs-6">
		<div class="row">
			<div class="col-xs-11 col-md-4 col-lg-4">
				<p class="lh150 va-m"><img class="dline va-b" src="/style/img/order1.svg" alt=""></p>

			</div>
			<div class="col-xs-11 col-md-4 col-lg-4 cntrBox">
				<p class="lh150 va-m"><img class="dline va-b" src="/style/img/order2.svg" alt=""></p>

			</div>
			<div class="col-xs-11 col-md-4 col-lg-4">
				<p class="lh150 va-m"><img class="dline va-b" src="/style/img/order3.svg" alt=""></p>

			</div>
		</div>
		</div>
		<div class="col-md-12  col-xs-6">
		<div class="row mt40">
		    <div class="col-xs-12 col-md-4 col-lg-4">

				<p class="dline pos-r pl60 text-left"><span class="pos-a l0 fw-700 ralewaybold">1</span>Выбери маршрут</p>
			</div>
			<div class="col-xs-12 col-md-4 col-lg-4 ">
				<p class="dline pos-r pl60 text-left"><span class="pos-a l0 fw-700 ralewaybold">2</span>Оплати его любым удобным для тебя способом</p>
			</div>
			<div class="col-xs-12 col-md-4 col-lg-4">
				<p class="dline pos-r pl60 text-left"><span class="pos-a l0 fw-700 ralewaybold">3</span>Отправляйся в незабываемое путешествие с JONEY</p>
			</div>
		</div>
		</div>
	</div>
</section><!-- End routes -->

<section class="titleBox2 text-center pos-r">
	<div class="bg"></div>
	<div class="pos-a b115 l0 r0">
		<h3>Начни отдыхать еще до отъезда</h3>
		<p>Планированием займется JONEY</p>
	</div>
</section><!-- End titleBox2 -->

<section class="tours pt75 pb45">
	<div class="container">
		<div id="sliderTours" class="sliderTours">
			<?php foreach(array_chunk(Page::model()->filterByCategory(3)->findAll(),3,true) as $row): ?>
				<div class="item row">
					<?php foreach($row as $page): ?>


						<div class="col-xs-12 col-md-4 col-lg-4">
							<div class="tourInfo pos-r">
								<div class="imgBox" style="background-image: url('<?php

								echo $page->getImage();

								?>') "></div>
								<div class="textBox  pt50 pb50">
									<p class="pl15 pr15 mb5">
										<span class="title col-md-6 col-lg-6"><?php echo $page->title ?></span>
										<span class="price col-md-6 col-lg-6 text-right"><?php echo $page->getEavAttribute('price') ?> руб</span>
										<span class="clear dblock"></span>
									</p>
									<p class="pl15 pr15">
										<span class="duration col-md-6 col-lg-6"><?php echo $page->getEavAttribute('duration') ?></span>
										<span class="download col-md-6 col-lg-6 text-right">скачано раз: <span><?php echo strlen($page->getEavAttribute('download'))>0?$page->getEavAttribute('download'):'0'?></span></span>
										<span class="clear dblock"></span>
									</p>
									<p class="pos-a right-20 bottom-45 visible-sm visible-xs">
										<?php if($page->getIsPaid()): ?>
										<a id="inline" href="/site/robokassa?id=<?php echo $page->getRel()[0] ?>" class="btn btn-success dline pl12 pr12 lh40">Скачать</a>
										<?php else: ?>
											<a id="inline" href="/site/robokassa?id=<?php echo $page->getRel()[0] ?>" class="btn btn-primary dline pl12 pr12 lh40">Купить за <?php echo $page->getEavAttribute('price') ?> руб</a>
										<?php endif; ?>
									</p>
								</div>
								<div class="slideInfo pos-a l0 r0 text-left pd35">
									<p><?php echo CHtml::encode($page->short_description) ?></p>
									<p class="text-center pos-a b35 l0 r0">
										<?php if($page->getIsPaid()): ?>
											<a id="inline" href="/site/robokassa?id=<?php echo $page->getRel()[0] ?>" class="btn btn-success dline pl12 pr12 lh40">Скачать</a>
										<?php else: ?>
											<a id="inline" href="/site/robokassa?id=<?php echo $page->getRel()[0] ?>" class="btn btn-primary dline pl12 pr12 lh40">Купить за <?php echo $page->getEavAttribute('price') ?> руб</a>
										<?php endif; ?>

									</p>
								</div>
							</div>
						</div>


					<?php endforeach ?>
				</div>
			<?php endforeach ?>

		</div><!-- End carousel -->
	</div>
</section><!-- End tours -->

<section class="titleBox3 text-center pos-r">
	<div class="bg"></div>
	<div class="pos-a l0 r0">
		<h3>Поделись эмоциями с <img class="va-t" src="/style/img/logo2.svg" alt=""></h3>
		<p>Путешествуй, загружай маршруты, советуй JONEY<br>
			друзьям и зарабатывай на новые путешествия</p>
	</div>
</section><!-- End titleBox3 -->

<section class="manual text-center pt45 pb80 fs18">
	<div class="container pos-r">
		<div class="row pb40">
			<div class="col-md-4 col-lg-4">
				<div class="imgBox lh140 va-b mb15 w140 dline">
					<img src="/style/img/order3.svg" alt="" class="va-b">
				</div>
				<div class="textBox">
					Путешествуй
				</div>
			</div>
			<div class="col-md-4 col-lg-4">
				<div class="imgBox lh140 va-b mb15 w140 dline">
					<img src="/style/img/manual2.svg" alt="" class="va-b">
				</div>
				<div class="textBox">
					Делай отличные<br> фотографии
				</div>
			</div>
			<div class="col-md-4 col-lg-4">
				<div class="imgBox lh140 va-b mb15 w140 dline">
					<img src="/style/img/order1.svg" alt="" class="va-b">
				</div>
				<div class="textBox">
					Стань автором собственного<br>
					уникального маршрута
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-lg-4">
				<div class="imgBox lh140 va-b mb15 w140 dline">
					<img src="/style/img/order2.svg" alt="" class="va-b">
				</div>
				<div class="textBox">
					Получай 30%<br>
					от стоимости за каждое<br>
					скачивание
				</div>
			</div>
			<div class="col-md-4 col-lg-4">
				<div class="imgBox lh140 va-b mb15 w140 dline">
					<img src="/style/img/manual5.svg" alt="" class="va-b">
				</div>
				<div class="textBox">
					JONEY рассмотрит<br>
					маршрут и примет решение<br>
					о его размещении на сайте
				</div>
			</div>
			<div class="col-md-4 col-lg-4">
				<div class="imgBox lh140 va-b mb15 w140 dline">
					<img src="/style/img/manual6.svg" alt="" class="va-b">
				</div>
				<div class="textBox">
					<a class="btn-success dline lh45 pl20 pr20 fs18">Заполни шаблон Joney</a>
				</div>
			</div>
		</div>
		<div class="line pos-a"></div>
	</div>
</section><!-- End manual -->

<section class="reviews pt30">
	<div class="container">
		<div class="">
			<h3 class="fs54 fw-600 text-center lh120 mb40">Не верь нам на слово</h3>
			<div id="sliderReviews" class="sliderReviews">
				<?php foreach(Page::model()->filterByCategory(4)->findAll() as $review): ?>
					<div class="item row">
						<div class="col-xs-3 col-lg-2 imgBox">
							<img src="<?php echo $review->getImage() ?>" alt="">
						</div>
						<div class="col-xs-9 col-lg-10 review">
							<p class="fs18"><?php echo $review->full_description ?></p>
							<p class="name fs18"><?php echo $review->short_description ?></p>
						</div>
					</div>
				<?php endforeach ?>


			</div><!-- End carousel -->
		</div>
	</div>
</section>


<div style="display:none">
	<div id="data">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
</div>