<?php
/**
 * Remind user password view
 */

$this->pageTitle = Yii::t('UsersModule.core','Напомнить пароль');
?>
<div class="container pb30">
<h1 class="has_background"><?php echo Yii::t('UsersModule.core','Напомнить пароль'); ?></h1>

<div class="login_box pl15">
	<div class="form wide">
		<?php
		echo TbHtml::form();
		echo TbHtml::errorSummary($model);
		?>

		<?php echo TbHtml::activeTextFieldControlGroup($model,'email'); ?>

		<div class="row buttons">
			<input type="submit" class="btn btn-default blue_button" value="<?php echo Yii::t('UsersModule.core','Напомнить'); ?>">
		</div>

		<div class="row buttons">
			<?php echo TbHtml::link(Yii::t('UsersModule', 'Регистрация'), array('register/register')) ?><br>
			<?php echo TbHtml::link(Yii::t('UsersModule', 'Авторизация'), array('login/login')) ?><br>
		</div>
		<?php echo TbHtml::endForm(); ?>
	</div>
</div></div>
