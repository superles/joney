<?php

/**
 * @var UserLoginForm $model
 * @var Controller $this
 */

$this->pageTitle = Yii::t('UsersModule.core','Авторизация');
?>
<div class="container pb30">
<h1 class="has_background"><?php echo Yii::t('UsersModule.core','Авторизация'); ?></h1>

<div class="login_box  pl15">
	<div class="form wide">
		<?php
			echo TbHtml::formTb(TbHtml::FORM_LAYOUT_HORIZONTAL,$this->createUrl('/users/login'),'post', array('id'=>'user-login-form'));
			echo TbHtml::errorSummary($model);
		?>

		<?php echo TbHtml::activeTextFieldControlGroup($model,'username'); ?>

		<?php echo TbHtml::activePasswordFieldControlGroup($model,'password'); ?>
		<div class="form-group buttons" style="margin-left: -35px">
		<?php echo TbHtml::activeCheckBoxControlGroup($model,'rememberMe'); ?>
		</div>
			<div class="form-group buttons">
				<input type="submit" class="btn btn-default blue_button" value="<?php echo Yii::t('UsersModule.core','Вход'); ?>">
			</div>

			<div class="row buttons">
				<?php echo TbHtml::link(Yii::t('UsersModule', 'Регистрация'), array('register/register')) ?><br>
				<?php echo TbHtml::link(Yii::t('UsersModule.core', 'Напомнить пароль'), array('/users/remind')) ?>
			</div>
		<?php echo TbHtml::endForm(); ?>
	</div>
</div></div>
