<?php
/**
 * Created by PhpStorm.
 * User: korg
 * Date: 12.08.2015
 * Time: 21:10
 */
css('/style/css/about.css');
?>

<section class="about pos-r">

    <div class="pos-a t0 l0 r0">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 pt30 pb30">
                    <div class="bgBox pd25 lh21">
                        <h3 class="fs22 fw-600 mt0 lh44 h24 mb30"><span class="dline pr10">О нас</span></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>