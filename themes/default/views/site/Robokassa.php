<?php
$payment=new SPaymentSystemManager();
$system=$payment->getSystemClass('robokassa');
/**
 * @var RobokassaPaymentSystem $system
 */
$method=StorePaymentMethod::model()->findByPk(19);
$ids=array_map(function(Order $el){ return $el->id; },Order::model()->findAllByAttributes(array('user_id'=>user()->id)));
$criteria=new CDbCriteria();
$criteria->compare('t.order_id',$ids);
$criteria->compare('t.product_id',$id);

if($oproduct=OrderProduct::model()->find($criteria)){
    $order=$oproduct->order;
}else{

    $order=new Order();
    $product=StoreProduct::model()->findByPk($id);
    $order->addUser();
    $order->save();
    $order->addProduct($product,1,$product->price);
}

if($order->paid==1){
    $oproducts=$order->getOrderedProducts()->getData();
    $oproduct=$oproducts[0];
    $page=PageRelation::model()->findByAttributes(array('relation_id'=>$oproduct->product_id))->page;
    $files=$page->listAttachments(true);
    echo ' <div class="col-xs-12 text-center">';
    echo '<p class="pb30 fw-700 fs20"> Файлы доступные для загрузки: </p>';
    foreach($files as $fid=>$file){
        echo TbHtml::link('<span class="glyphicon glyphicon-download-alt"> </span> '.$file->getFilename(),app()->controller->createUrl('downloadAttachment',array('id'=>$fid)),array('target'=>'_blank'));
    }
    echo '</div>';

}else{
    ?>

    <div class="col-xs-12 text-center">
        Для оплаты с помощью сервиса ROBOKASSA нажмите кнопку "Оплатить"
    </div>


    <div class="col-xs-12 mt30 text-center">
        <?php
        echo $system->renderPaymentForm($method,$order);


        ?></div>
<?php
}
?>





