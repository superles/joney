<?php
/**
 * @var Controller $this
 * @var Page $model
 */
?>


<?php $this->renderPartial('//layouts/_header') ?>



<?php
	// Notifier module form
	Yii::import('application.modules.notifier.NotifierModule');
	NotifierModule::renderDialog();
?>

<?php
	if(($messages = Yii::app()->user->getFlash('messages')))
	{
		echo '<script type="text/javascript">';
		foreach ($messages as $m)
		{
			echo '$.jGrowl("'.CHtml::encode($m).'",{position:"bottom-right"});';
		}
		echo '</script>';
	}
?>

<?php echo $content ?>

<div style="clear:both;"></div>
<?php $this->renderPartial('//layouts/_admin',array()) ?>
<?php $this->renderPartial('//layouts/_footer') ?>
