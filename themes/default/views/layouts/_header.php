<?php
/**
 * @var Controller $this
 * @var Page $model
 */
Yii::import('application.modules.store.components.SCompareProducts');
Yii::import('application.modules.store.models.wishlist.StoreWishlist');

$assetsManager = Yii::app()->clientScript;
$assetsManager->registerCoreScript('jquery');
$assetsManager->registerCoreScript('jquery.ui');

// jGrowl notifications
Yii::import('ext.jgrowl.Jgrowl');
Jgrowl::register();

// Disable jquery-ui default theme
$assetsManager->scriptMap=array(
   // 'jquery-ui.css'=>false,
);
app()->bootstrap->register()
?>

<?php css('/bower_components/bootstrap/dist/css/bootstrap.css')?>
<?php css('/style/css/main.css')?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo CHtml::encode($this->pageTitle) ?></title>
    <meta charset="UTF-8"/>
    <meta name="description" content="<?php echo CHtml::encode($this->pageDescription) ?>">
    <meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords) ?>">


    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.png" type="image/png">
    <?php css("/bower_components/font-awesome/css/font-awesome.min.css")?>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/common.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/menu.js"></script>
    <script type="text/javascript" src="/style/js/jquery.hotkeys.js"></script>
    <?php echo app()->settings->get('count','yandex','') ?>

    <?php echo app()->settings->get('seo','head','') ?>






    <!-- Bootstrap core CSS -->
    <link href="/style/css/bootstrap.css" rel="stylesheet">

    <!-- Owl-carousel stylesheet -->
    <link rel="stylesheet" href="/bower_components/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="/bower_components/owl-carousel/owl.theme.css">

    <!-- Fancybox -->
    <link rel="stylesheet" href="/style/js/fancybox/jquery.fancybox.css">

    <link href="/style/css/main.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<?php clientScript()->registerScript('hotkeys',<<<JS
    shortcut.add('Ctrl+ALT+A',function() {
				window.location='/admin';
			});

JS
    ,CClientScript::POS_READY
) ?>

<?php
if($this->model instanceof Page&&user()->isAdmin()){
clientScript()->registerScript('edit',<<<JS
    shortcut.add('Ctrl+ALT+E',function() {
				window.location='/admin/pages/default/update?id={$this->model->id}';
			});

JS
    ,CClientScript::POS_READY
);
}
?>
<body data-lang="<?php app()->language ?>">



<?php

clientScript()->registerScript('load',<<<JS

    $(window).bind("beforeunload", function(){
        document.body.style.WebkitTransition="opacity .3s ease-in";
        document.body.style.MozTransition="opacity .3s ease-in";
         $("body").css({opacity:0});
        });
        $(document).on("ready", function() {
            $ ( "body" ).css ( { opacity : 1 } );
        });
JS
    ,CClientScript::POS_READY);

?>
<noscript>
    <link href="/style/css/nojs.css" type="text/css" rel="stylesheet" />
</noscript>
<script>
    document.body.style.opacity=0;
    document.body.style.WebkitTransition="opacity .3s ease-in-out";
    document.body.style.MozTransition="opacity .3s ease-in-out";

</script>

<?php echo app()->settings->get('seo','body','') ?>

<header class="topbar">
    <div class="container">
        <div class="row">
            <div class="col-xs-5 col-sm-4 col-md-2 col-lg-3 first">
                <a href="/" class="logo"><img src="/style/img/logo.svg"  alt=""></a>
            </div>
            <div class="col-xs-7 col-sm-8 col-md-4 col-lg-6 text-center middle">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="list-inline pl0 ml0 text-justify dline">
                            <li><a href="/pages/routes">Маршруты</a></li>
                            <li><a href="">Загрузить маршрут</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <div class="col-xs-6 col-sm-4 col-md-8 text-right">
                    <div class="row">
                        <div class="dline va-m">
                            <form action="/page/search">
                            <div class="input-group">
                                <input type="text" style="float: none" <?php if($search = request()->getParam('search',false)) echo 'value="'.$search.'"' ?> name="search" placeholder="Поиск..." class="form-control" aria-label="">
                            <div class="input-group-btn search pt1">


                                <a href="#" class="btn btn-default" onclick="submitForm(this)"><img src="/style/img/search.svg" alt=""></a>

                            </div> </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-8 col-md-4 text-right">
                        <p class="mb0">
                            <a href="/users/register">Регистрация</a>
                        </p>
                </div>
            </div>
        </div>
    </div>
</header><!-- End topbar -->