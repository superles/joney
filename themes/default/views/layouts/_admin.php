<?php
/**
 * @var Controller $this
 * @var Page $model
 */
?>
<?php

if (user()->isAdmin() && app()->translate->hasMessages()) {
    //generates a to the page where you translate the missing translations found in this page
    ?>
    <?php



    $link = '';

    ?>
<!--    --><?php //if($this->model instanceof Page): ?>
<!--        --><?php
//
//        $model = $this->model;
//        $link = $model->editorLink();
//        ?>
<!---->
<!---->
<!--    --><?php //endif; ?>

    <style>
        body {
            padding-top: 50px;
        }
    </style>
    <nav class="navbar navbar-fixed-top navbar-inverse navbar-small">
        <div class="container">
            <ul class="nav  navbar-nav navbar-left">
                <li>
                    <?php echo app()->translate->translateLink('Добавить перевод', 'link'); ?>
                </li>

                <li><?php
                    echo app()->translate->editLink('Редактировать перевод', 'link'); ?>
                </li>

                <?php if ($this->model instanceof Page): ?>
                    <li>
                        <?php

                        echo CHtml::link('Редактировать страницу', array('/admin/pages/default/update', 'id' => $this->model->id));
                        ?>
                    </li>
                <?php endif; ?>


                <?php if ($this->model instanceof Page && $this->model->mainCategory): ?>
                    <li>
                        <?php

                        echo CHtml::link('Редактировать категорию', array('/admin/pages/category/update', 'id' => $this->model->mainCategory->id));
                        ?>
                    </li>
                <?php endif; ?>

                <?php echo $link ?>

            </ul>

            <ul class="nav  navbar-nav navbar-right">

                <li><a class="btn-default btn" href="/admin">Панель</a></li>
                <li><a class="btn-default btn" href="/users/logout">Выход</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle btn btn-default " data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        <?php echo strtoupper(app()->languageManager->getActive()->code) ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu p100-w" role="menu" style="min-width:inherit">
                        <li><a href="/">RU</a></li>
                        <li><a href="/en">EN</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
<?php } ?>
