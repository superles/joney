<?php
/**
 * @var Controller $this
 * @var Page $model
 */
?>


<footer>
    <div class="container">
        <div class="row lh70">
            <div class="col-xs-12 col-md-3 col-sm-2 col-lg-2 blogo">
                <img src="/style/img/logo3.svg" alt="">
            </div>
            <div class="col-xs-12 col-md-9 col-sm-10 col-lg-6 menu">
                <ul class="mb0">
                    <li class="dline mr28"><a href="/site/about">О нас</a></li>
                    <li class="dline mr28"><a href="/site/index">Пользовательское соглашение</a></li>
                    <li class="dline mr28"><a href="#">Стань автором JONEY</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-4 col-sm-6 col-lg-2 fs12">
                JONEY. All right reserved
            </div>
            <div class="col-xs-12 col-md-8 col-sm-6 col-lg-2 text-right social">
                <a href="<?php echo app()->settings->get('social','facebook_link') ?>" class="dline ml10"><img src="/style/img/f.svg" alt=""></a>
                <a href="<?php echo app()->settings->get('social','vk_link') ?>" class="dline ml10"><img src="/style/img/vk.svg" alt=""></a>
                <a href="<?php echo app()->settings->get('social','instagram_link') ?>" class="dline ml10"><img src="/style/img/in.svg" alt=""></a>
            </div>
        </div>
    </div>
</footer>



<!-- Owl-carousel js plugin -->
<script src="/bower_components/owl-carousel/owl.carousel.js"></script>

<!-- Fancybox -->
<script src="/style/js/fancybox/jquery.fancybox.pack.js"></script>

<script src="/style/js/js.js"></script>
<?php echo app()->settings->get('seo','body_bottom','') ?>
</body>
</html>
