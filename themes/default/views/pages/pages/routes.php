<?php

/**
 * View category pages
 * @var PageCategory $model
 * @var Page $page
 */

// Set meta tags
$this->pageTitle = ($model->meta_title) ? $model->meta_title : $model->name;
$this->pageKeywords = $model->meta_keywords;
$this->pageDescription = $model->meta_description;
?>
<div class="agreement bg-white" style="background: white">
    <div class="container">
        <div class="row ml0">
            <div class="col-xs-12">
                <h2 class="fs22 lh50 mb25">
                    <span class="pr20 bg-white">Все маршруты</span>
                    <span class="floatr pl20 hidden-xs bg-white"><img class="va-t" src="/style/img/agreement.svg" alt=""></span>
                </h2>
            </div>
        </div>
        <div class="row mb100 tours ">
            <div class="col-xs-12 mr40">
                <div class="">


                <section class="tours pt20 pb45">
                    <div class="container">
                        <div id="sliderTours" class="sliderTours">
                            <?php foreach(array_chunk($pages,3,true) as $row): ?>
                                <div class="item row">
                                    <?php foreach($row as $page): ?>


                                        <div class="col-xs-12 col-md-4 col-lg-4">
                                            <div class="tourInfo pos-r">
                                                <div class="imgBox" style="background-image: url('<?php

                                                echo $page->getImage();

                                                ?>') "></div>
                                                <div class="textBox  pt50 pb50">
                                                    <p class="pl15 pr15 mb5">
                                                        <span class="title col-md-6 col-lg-6"><?php echo $page->title ?></span>
                                                        <span class="price col-md-6 col-lg-6 text-right"><?php echo $page->getEavAttribute('price') ?> руб</span>
                                                        <span class="clear dblock"></span>
                                                    </p>
                                                    <p class="pl15 pr15">
                                                        <span class="duration col-md-6 col-lg-6"><?php echo $page->getEavAttribute('duration') ?></span>
                                                        <span class="download col-md-6 col-lg-6 text-right">скачано раз: <span><?php echo strlen($page->getEavAttribute('download'))>0?$page->getEavAttribute('download'):'0'?></span></span>
                                                        <span class="clear dblock"></span>
                                                    </p>
                                                    <p class="pos-a right-20 bottom-45 visible-sm  visible-xs">
                                                        <?php if($page->getIsPaid()): ?>
                                                            <a id="inline" href="/site/robokassa?id=<?php echo $page->getRel()[0] ?>" class="btn btn-success dline pl12 pr12 lh40">Скачать</a>
                                                        <?php else: ?>
                                                            <a id="inline" href="/site/robokassa?id=<?php echo $page->getRel()[0] ?>" class="btn btn-primary dline pl12 pr12 lh40">Купить за <?php echo $page->getEavAttribute('price') ?> руб</a>
                                                        <?php endif; ?>
                                                    </p>
                                                </div>
                                                <div class="slideInfo pos-a l0 r0 text-left pd35">
                                                    <p><?php echo CHtml::encode($page->short_description) ?></p>
                                                    <p class="text-center pos-a b35 l0 r0">
                                                        <?php if($page->getIsPaid()): ?>
                                                            <a id="inline" href="/site/robokassa?id=<?php echo $page->getRel()[0] ?>" class="btn btn-success dline pl12 pr12 lh40">Скачать</a>
                                                        <?php else: ?>
                                                            <a id="inline" href="/site/robokassa?id=<?php echo $page->getRel()[0] ?>" class="btn btn-primary dline pl12 pr12 lh40">Купить за <?php echo $page->getEavAttribute('price') ?> руб</a>
                                                        <?php endif; ?>

                                                    </p>
                                                </div>
                                            </div>
                                        </div>


                                    <?php endforeach ?>
                                </div>
                            <?php endforeach ?>

                        </div><!-- End carousel -->
                    </div>
                </section><!-- End tours -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <?php $this->widget('CLinkPager', array(
                'pages' => $pagination
            )) ?>
        </div>
    </div>
</div>

